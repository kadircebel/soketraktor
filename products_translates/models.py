from django.db import models
# from froala_editor.fields import FroalaField
from helpers.views import custom_slugify
from .managers import *
from ckeditor.fields import RichTextField


class ProductsTranslates(models.Model):
    id = models.BigAutoField(primary_key=True)
    products = models.ForeignKey('products.Products',verbose_name="Ürün",db_index=True,on_delete=models.DO_NOTHING)
    languages = models.ForeignKey('languages.Languages',on_delete=models.DO_NOTHING,verbose_name="Dil")
    title = models.CharField(max_length=600, verbose_name="Ürün Adı",help_text="Seçilen dile göre ürün adı giriniz.")
    content_text = RichTextField(verbose_name="", blank=True, null=True)
    slug = models.SlugField(max_length=650, verbose_name="Ürün Url", blank=True, null=True)
    is_stock = models.BooleanField(default=True, verbose_name="Stokta Mı?")
    is_active = models.BooleanField(default=True,verbose_name="Aktif Mi?")

    objects = ProductsTranslatesManager()

    class Meta:
        managed = True
        db_table = "products_translates"
        verbose_name = "Product"
        verbose_name_plural = "Products"

    def __str__(self):
        return self.title

    def save(self, *args,**kwargs):
        self.slug = custom_slugify(self.title)
        super(ProductsTranslates,self).save(*args,**kwargs)