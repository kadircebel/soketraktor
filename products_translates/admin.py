from django.contrib import admin
from .models import *

class ProductsTranslatesAdmin(admin.ModelAdmin):
    list_per_page = 50
    list_display = ["languages","products","title","slug","is_stock","is_active"]
    list_editable = ["is_stock","is_active"]
    fields = [
        "languages",
        "products",
        "title",
        "content_text",
        "slug",
        "is_stock",
        "is_active"
    ]
    readonly_fields = ["slug"]

    class Meta:
        model = ProductsTranslates

admin.site.register(ProductsTranslates,ProductsTranslatesAdmin)
