from django.apps import AppConfig


class ProductsTranslatesConfig(AppConfig):
    name = 'products_translates'
