"use strict";

$(document).ready(function () {
    homepage_slider();
    products_loadmore();
    more_categories();
    all_categories_more();
    readmore_product_description();
    create_customer();
    logout();
    login();
    password_form();
    account_form_save();
    update_password();
    reset_password_with_email();
    add_basket();
    remove_product_in_dashboard();
    card_form();
    payment_worksheet();
    current_payment_form();
    complete_shopping();
    contact_us();

    $(".autocomplete-search").unbind("keydown").bind("keyup", function (e) {
        if ($(this).val().trim().length > 0) {
            if (keycode_control(e.keyCode)) {
                autocomplete_ajax($(this).val().trim());
            }
        } else {
            $(".search-results").html('');
            $(".search-results").hide();
            $(".all-results").hide();
        }
    });

    hide_search_results();

    // Modal Form
    $('.callback').fancybox({
        padding: 0,
        content: $('#modal-form'),
        helpers: {
            overlay: {
                locked: false
            }
        },
        tpl: {
            closeBtn: '<a title="Close" class="fancybox-item fancybox-close modal-form-close" href="javascript:;"></a>'
        }
    });


    // Fancybox Images
    $('.fancy-img').fancybox({
        padding: 0,
        helpers: {
            overlay: {
                locked: false
            },
            thumbs: {
                width: 60,
                height: 60
            }
        },
        tpl: {
            closeBtn: '<a title="Close" class="fancybox-item fancybox-close modal-form-close2" href="javascript:;"></a>'
        }
    });

    // Modal Videos
    // $("#about-gallery").on('click', '.about-video', function () {
    //     $.fancybox({
    //         'padding': 0,
    //         'autoScale': false,
    //         'transitionIn': 'none',
    //         'transitionOut': 'none',
    //         'href': this.href.replace(new RegExp("watch\\?v=", "i"), 'v/'),
    //         'type': 'swf',
    //         'swf': {
    //             'wmode': 'transparent',
    //             'allowfullscreen': 'true'
    //         }
    //     });
    //     return false;
    // });
    // $("#gallery-grid").on('click', ".gallery-video", function () {
    //     $.fancybox({
    //         'padding': 0,
    //         'autoScale': false,
    //         'transitionIn': 'none',
    //         'transitionOut': 'none',
    //         'href': this.href.replace(new RegExp("watch\\?v=", "i"), 'v/'),
    //         'type': 'swf',
    //         'swf': {
    //             'wmode': 'transparent',
    //             'allowfullscreen': 'true'
    //         }
    //     });
    //     return false;
    // });

    // BreadCrumbs
    if ($('#b-crumbs-menu').length > 0) {
        $('.b-crumbs-menu').on('click', '#b-crumbs-menu', function () {
            if ($(this).hasClass('opened')) {
                $(this).removeClass('opened');
                $(this).next('.b-crumbs-menulist').fadeOut(200);
            } else {
                $(this).addClass('opened');
                $(this).next('.b-crumbs-menulist').fadeIn(200);
            }
            return false;
        });
    }

    // Dropdown
    if ($('.dropdown-wrap').length > 0) {
        $('.dropdown-wrap').on('click', '.dropdown-title', function () {
            $('.dropdown-list').slideUp(200);
            if ($(this).hasClass('opened')) {
                $(this).removeClass('opened');
                // $(this).next('.dropdown-list').slideUp(200);
            } else {
                $('.dropdown-wrap .dropdown-title').removeClass('opened');
                $(this).addClass('opened');
                $(this).next('.dropdown-list').slideDown(200);
            }
            return false;
        });
        $('.cont').on('click', '.dropdown-wrap-range', function () {
            return false;
        });
        $('.dropdown-wrap .dropdown-list li').on('click', 'a', function () {
            $(this).closest('.dropdown-wrap').find('.dropdown-title').text($(this).text());
            if ($(this).attr('href') == '#') {
                $('.dropdown-list').slideUp(200);
                $('.dropdown-wrap .dropdown-title').removeClass('opened');
                return false;
            }
        });
    }

    if ($('.dropdown-wrap').length > 0 || $('#b-crumbs-menu').length > 0) {
        $('body').on('click', function () {
            if ($('#b-crumbs-menu').length > 0) {
                $('.b-crumbs-menulist').fadeOut(200);
                $('#b-crumbs-menu').removeClass('opened');
            }
            if ($('.dropdown-wrap').length > 0) {
                $('.dropdown-list').slideUp(200);
                $('.dropdown-wrap .dropdown-title').removeClass('opened');
            }
        });
    }

    // Top Menu Seacrh
    $('.header').on('click', '#header-searchbtn', function () {
        if ($(this).hasClass('opened')) {
            $(this).removeClass('opened');
            $('#header-search').fadeOut();
        } else {
            $(this).addClass('opened');
            $('#header-search').fadeIn();
        }
        return false;
    });

    // Top Menu
    $('.header').on('click', '#header-menutoggle', function () {
        if ($(this).hasClass('opened')) {
            $(this).removeClass('opened');
            $('#top-menu').fadeOut();
        } else {
            $(this).addClass('opened');
            $('#top-menu').fadeIn();
        }
        return false;
    });
    // Top SubMenu
    $('#top-menu .has-child').on('click', '.fa', function () {
        if ($(this).parent().hasClass('opened')) {
            $(this).parent().removeClass('opened');
            $(this).next('ul').slideUp();
        } else {
            $(this).parent().addClass('opened');
            $(this).next('ul').slideDown();
        }
        return false;
    });

    // Section Menu
    if ($('#section-menu-btn').length > 0) {
        $('.section-top').on('click', '#section-menu-btn', function () {
            var button_text = $("#section-menu-btn").text();
            var language = $("#section-menu-btn").attr("data-language");
            var hover_text = "";
            // console.log(button_text);
            if (language === "tr") {
                hover_text = "Kapat";
                if (hover_text === button_text) {
                    button_text = "Katalog";
                }
            } else {
                hover_text = "Close";
                if (hover_text === button_text) {
                    button_text = "Catalog";
                }
            }
            if ($(this).hasClass('opened')) {
                $(this).removeClass('opened').text(button_text);
                $('#section-menu-wrap').fadeOut(200);
                $('.section-menu-overlay').fadeOut(200).remove();
            } else {
                $(this).addClass('opened').width($(this).width()).text(hover_text);
                $('#section-menu-wrap').fadeIn(200);
                $('body').append('<div class="section-menu-overlay"></div>');
                $('.section-menu-overlay').fadeIn(200);

                $('body').on('click', '.section-menu-overlay', function () {
                    $('#section-menu-btn').removeClass('opened').text(button_text);
                    $('#section-menu-wrap').fadeOut(200);
                    $('.section-menu-overlay').fadeOut(200).remove();
                    return false;
                });
            }
            return false;
        });
    }

    // Product Tabs
    $('.prod-tabs li').on('click', 'a', function () {
        if ($(this).parent().hasClass('prod-tabs-addreview') || $(this).parent().hasClass('active') || $(this).attr('data-prodtab') == '')
            return false;
        $('.prod-tabs li').removeClass('active');
        $(this).parent().addClass('active');

        // mobile
        $('.prod-tab-mob').removeClass('active');
        $('.prod-tab-mob[data-prodtab-num=' + $(this).parent().data('prodtab-num') + ']').addClass('active');

        $('.prod-tab-cont .prod-tab').hide();
        $($(this).attr('data-prodtab')).fadeIn();

        return false;
    });

    // Product Tabs (mobile)
    $('.prod-tab-cont').on('click', '.prod-tab-mob', function () {
        if ($(this).hasClass('active') || $(this).attr('data-prodtab') == '')
            return false;
        $('.prod-tab-cont .prod-tab-mob').removeClass('active');
        $(this).addClass('active');

        // main
        $('.prod-tabs li').removeClass('active');
        $('.prod-tabs li[data-prodtab-num=' + $(this).data('prodtab-num') + ']').addClass('active');

        $('.prod-tab-cont .prod-tab').slideUp();
        $($(this).attr('data-prodtab')).slideDown();
        return false;
    });

    $('.prod-tabs').on('click', '.prod-tabs-addreview', function () {
        if ($('.prod-tabs li.active a').attr('data-prodtab') == '#prod-tab-3') {
            $('html, body').animate({
                scrollTop: ($('.prod-tabs-wrap').offset().top - 10)
            }, 700);
        } else {
            $('.prod-tabs li').removeClass('active');
            $('#prod-reviews').addClass('active');
            $('.prod-tab-cont .prod-tab').hide();
            $('#prod-tab-3').fadeIn();
            $('html, body').animate({
                scrollTop: ($('.prod-tabs-wrap').offset().top - 10)
            }, 700);
        }
        $('#prod-addreview-form').fadeIn();
        return false;
    });

    // Show Properties
    $('.prod-cont').on('click', '#prod-showprops', function () {
        if ($('.prod-tabs li.active a').attr('data-prodtab') == '#prod-tab-2') {
            $('html, body').animate({
                scrollTop: ($('.prod-tabs-wrap').offset().top - 10)
            }, 700);
        } else {
            $('.prod-tabs li').removeClass('active');
            $('#prod-props').addClass('active');
            $('.prod-tab-cont .prod-tab').hide();
            $('#prod-tab-2').fadeIn();
            $('html, body').animate({
                scrollTop: ($('.prod-tabs-wrap').offset().top - 10)
            }, 700);
        }
        return false;
    });

    // Show Description
    $('.prod-cont').on('click', '#prod-showdesc', function () {
        if ($('.prod-tabs li.active a').attr('data-prodtab') == '#prod-tab-1') {
            $('html, body').animate({
                scrollTop: ($('.prod-tabs-wrap').offset().top - 10)
            }, 700);
        } else {
            $('.prod-tabs li').removeClass('active');
            $('#prod-desc').addClass('active');
            $('.prod-tab-cont .prod-tab').hide();
            $('#prod-tab-1').fadeIn();
            $('html, body').animate({
                scrollTop: ($('.prod-tabs-wrap').offset().top - 10)
            }, 700);
        }
        return false;
    });

    // Quantity
    product_quantity();
    // $('.qnt-wrap').on('click', 'a', function () {
    //     var qnt = $(this).parent().find('input').val();
    //     if ($(this).hasClass('qnt-plus')) {
    //         qnt++;
    //     } else if ($(this).hasClass('qnt-minus')) {
    //         qnt--;
    //     }
    //     if (qnt > 0)
    //         $(this).parent().find('input').val(qnt);
    //     return false;
    // });

    // Colors
    // $('.prodv-colors ul').on('click', 'li', function () {
    //     if (!$(this).hasClass('active')) {
    //         $('.prodv-colors ul li').removeClass('active');
    //         $(this).addClass('active');
    //     }
    //     return false;
    // });

    // Catalog Rating
    // $('.sectls-rating').on('click', 'i', function () {
    //     if ($(this).parent().attr('data-rating'))
    //         return false;
    //
    //     $(this).parent().attr('data-rating', $(this).attr('title'));
    //     var rating_count = $(this).parent().parent().find('.sectls-rating-count').text();
    //     rating_count++;
    //     $(this).parent().parent().find('.sectls-rating-count').html(rating_count);
    //     return false;
    // });

    // Product Rating
    // $('.prod-rating').on('click', 'i', function () {
    //     if ($(this).parent().attr('data-rating'))
    //         return false;
    //
    //     $(this).parent().attr('data-rating', $(this).attr('title'));
    //     var rating_count = $(this).parent().parent().find('.prod-rating-count').text();
    //     rating_count++;
    //     $(this).parent().parent().find('.prod-rating-count').html(rating_count);
    //     return false;
    // });

    // Post Add Comment Form
    // $('.post-comments').on('click', '#post-comments-add', function () {
    //     $('#post-addcomment-form').slideDown();
    //     return false;
    // });

    // Forms Validation
    var filterEmail = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,6})+$/;
    $('.form-validate').submit(function () {
        var errors = 0;
        $(this).find('[data-required="text"]').each(function () {
            if ($(this).attr('data-required-email') == 'email') {
                if (!filterEmail.test($(this).val())) {
                    $(this).addClass("redborder");
                    errors++;
                } else {
                    $(this).removeClass("redborder");
                }
                return;
            }
            if ($(this).val() == '') {
                $(this).addClass('redborder');
                errors++;
            } else {
                $(this).removeClass('redborder');
            }
        });
        if (errors === 0) {
            var form1 = $(this);
            $.ajax({
                type: "POST",
                url: 'php/email.php',
                data: $(this).serialize(),
                success: function (data) {
                    form1.append('<p class="form-result">Thank you!</p>');
                    $("form").trigger('reset');
                }
            });
        }
        return false;
    });
    $('.form-validate').find('[data-required="text"]').blur(function () {
        if ($(this).attr('data-required-email') == 'email' && ($(this).hasClass("redborder"))) {
            if (filterEmail.test($(this).val()))
                $(this).removeClass("redborder");
            return;
        }
        if ($(this).val() != "" && ($(this).hasClass("redborder")))
            $(this).removeClass("redborder");
    });
    $(".loading-page").fadeOut();
});


// Generate Front Filter Lines
function motor_createLine(x1, y1, x2, y2, after_el) {
    var length = Math.sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
    var angle = Math.atan2(y2 - y1, x2 - x1) * 180 / Math.PI;
    var transform = 'rotate(' + angle + 'deg)';

    var line = jQuery('<div>').insertAfter(after_el).addClass('line').css({
        'position': 'absolute',
        'transform': transform
    }).offset({
        left: x1,
        top: y1
    }).width(length);
    return line;
}

$(window).load(function () {

    // Front Filter
    if ($('#frontsearch-cont').length > 0) {
        for (var i = 1; i <= $('#frontsearch-cont').data('lines-count'); i++) {
            if ($('#frontsearch-cont .frontsearch-res' + i + ' span').length > 0) {
                motor_createLine(
                    ($('#frontsearch-cont .frontsearch-res' + i + ' span').offset().left + 17),
                    ($('#frontsearch-cont .frontsearch-res' + i + ' span').offset().top + 9),
                    ($('#frontsearch-cont .frontsearch-point' + i).offset().left + 6),
                    ($('#frontsearch-cont .frontsearch-point' + i).offset().top + 6),
                    ('#frontsearch-cont .frontsearch-res' + i)
                );
            }
        }
        $(window).resize(function () {
            if ($('#frontsearch-cont .frontsearch-res' + i + ' span').length > 0) {
                $('#frontsearch-cont .line').remove();
                for (var i = 1; i <= $('#frontsearch-cont').data('lines-count'); i++) {
                    motor_createLine(
                        ($('#frontsearch-cont .frontsearch-res' + i + ' span').offset().left + 17),
                        ($('#frontsearch-cont .frontsearch-res' + i + ' span').offset().top + 9),
                        ($('#frontsearch-cont .frontsearch-point' + i).offset().left + 6),
                        ($('#frontsearch-cont .frontsearch-point' + i).offset().top + 6),
                        ('#frontsearch-cont .frontsearch-res' + i)
                    );
                }
            }
        });
    }


    // Front Slider
    if ($('#front-slider').length > 0) {
        $('#front-slider').fractionSlider({
            'fullWidth': true,
            'controls': false,
            'pager': true,
            'responsive': true,
            'increase': false,
            'pauseOnHover': false,
            'dimensions': "1170,392",
        });
    }


    // Product Slider
    if ($('#prod-slider').length > 0) {
        $("#prod-thumbs").flexslider({
            animation: "slide",
            controlNav: false,
            animationLoop: false,
            slideshow: false,
            itemWidth: 97,
            itemMargin: 0,
            minItems: 4,
            maxItems: 4,
            asNavFor: '#prod-slider',
            start: function (slider) {
                $("#prod-thumbs").resize();
            }
        });
        $('#prod-slider').flexslider({
            animation: "fade",
            animationSpeed: 500,
            slideshow: false,
            animationLoop: false,
            smoothHeight: false,
            controlNav: false,
            sync: "#prod-thumbs",
        });
    }

    // Slider "About Us"
    if ($('#aboutus').length > 0) {
        $('#aboutus').flexslider({
            animation: "fade",
            animationSpeed: 500,
            slideshow: false,
            animationLoop: false,
            directionNav: false,
            smoothHeight: true,
            controlNav: true,
        });
    }


    // Blog sliders
    if ($('.blog-slider').length > 0) {
        $('.blog-slider').flexslider({
            animation: "fade",
            animationSpeed: 500,
            slideshow: false,
            animationLoop: false,
            directionNav: false,
            smoothHeight: false,
            controlNav: true,
        });
    }
    if ($('.post-slider').length > 0) {
        $('.post-slider').flexslider({
            animation: "fade",
            animationSpeed: 500,
            slideshow: false,
            animationLoop: false,
            directionNav: false,
            smoothHeight: true,
            controlNav: true,
        });
    }


    // Section Product Title in Hover
    $('.special')
        .on("mouseenter", function () {
            var ttl_height = $(this).find('h3').height();
            var inner_height = $(this).find('h3 span').height();
            $(this).find('h3 span').css('top', (-inner_height + ttl_height));
        })
        .on("mouseleave", function () {
            $(this).find('h3 span').css('top', 0);
        });

    $('.popular')
        .on("mouseenter", function () {
            var ttl_height = $(this).find('h3').height();
            var inner_height = $(this).find('h3 span').height();
            $(this).find('h3 span').css('top', (-inner_height + ttl_height));
        })
        .on("mouseleave", function () {
            $(this).find('h3 span').css('top', 0);
        });

    $('.sectgl')
        .on("mouseenter", function () {
            var ttl_height = $(this).find('h3').height();
            var inner_height = $(this).find('h3 span').height();
            $(this).find('h3 span').css('top', (-inner_height + ttl_height));
        })
        .on("mouseleave", function () {
            $(this).find('h3 span').css('top', 0);
        });


    // Masonry Grids
    if ($('#blog-grid').length > 0) {
        $('#blog-grid').masonry({
            itemSelector: '.blog-grid-i',
        });
    }
    if ($('#gallery-grid').length > 0) {
        $('#gallery-grid').masonry({
            itemSelector: '.gallery-grid-i',
            columnWidth: '.gallery-grid-sizer',
            percentPosition: true
        });
    }
    if ($('#about-gallery').length > 0) {
        $('#about-gallery').masonry({
            itemSelector: '.grid-item',
            columnWidth: '.grid-sizer',
            percentPosition: true
        });
    }


});


function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}

$.ajaxSetup({
    beforeSend: function (xhr, settings) {

        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {

            xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
        }
    }
});


function homepage_slider() {
    $('.homepage-slider').slick({
        arrows: true,
    });
}

function product_translates(languages, title) {
    var title_translates = "";

    if (languages === "tr") {
        title_translates = title;
    } else {
        title_translates = "Models";
    }

    return title_translates;
}

function translate_words(language, title) {
    var title_str = "";
    if (language === "tr") {
        title_str = title;
    } else {
        switch (title) {
            case "Oem No":
                title_str = "Oem Num.";
                break;
            case "Sub No":
                title_str = "Sub Num.";
                break;
            case "Stok":
                title_str = "Stock";
                break;
            case "Fiyat":
                title_str = "Price";
                break;
            case "Adet":
                title_str = "Quantity";
                break;
            case "Sepete Ekle":
                title_str = "Add To Cart";
                break;
        }
    }
    return title_str;
}

function product_models_html(language, title, prd_models) {
    var html_str = '<div class="sectls-rating-wrap"><p>' + product_translates(language, title) + ': </p></div>';
    var i = 0;
    if (prd_models.length > 0) {
        for (i = 0; i < prd_models.length; i++) {
            if (i === prd_models.length - 1) {
                html_str += '<p class="sectls-id models-bullet" itemscope itemtype="https://schema.org/ProductModel">' + prd_models[i].title + '</p>';
                // console.log(value.prd_models[i].title);
            } else {
                html_str += '<p class="sectls-id models-bullet" itemscope itemtype="https://schema.org/ProductModel">' + prd_models[i].title + ',</p> ';
                // console.log(value.prd_models[i].title + ", ");
            }
        }
    }
    return html_str;
}

function product_html(slug, cover_image, title, oem_number, sub_number, models, language, category_name, category_slug, id, product_type, price) {
    var suboem_html = "";
    if (cover_image === null) {
        cover_image = "/static/images/empty.png";
    }

    // console.log(sub_number);
    if (sub_number.length > 0) {
        sub_number.forEach
        (
            item => suboem_html += '<p class="sectls-total prd-value" itemprop="mpn" content="' + item.suboem + '">' + item.suboem + '</p>'
        );
    } else {
        suboem_html = '<p class="sectls-total prd-value"> - </p>';
    }


    var html = '<div class="sectls" itemtype="https://schema.org/Product" itemscope><a href="' + slug + '" class="sectls-img"><img itemprop="image" src="' + cover_image + '" alt="' + title + '"></a><div class="sectls-cont"><div class="sectls-ttl-wrap"><p itemprop="category"><a href="' + category_slug + '">' + category_name + '</a></p><h3 itemprop="name" content="' + title + '"><a href="' + slug + '">' + title + '</a></h3></div><div class="sectls-price-wrap"><p>' + translate_words(language, "Oem No") + '</p><p class="sectls-total prd-value" itemprop="mpn" content="' + oem_number + '">' + oem_number + '</p></div><div class="sectls-price-wrap"><p>' + translate_words(language, "Sub No") + '</p>' + suboem_html + '</div><div class="sectls-total-wrap"><p itemprop="availability" content="https://schema.org/InStock">' + translate_words(language, "Stok") + '</p><p class="sectls-total"><span class="fa fa-check-circle true-stock"></span></p></div><div class="sectls-price-wrap"><p>' + translate_words(language, "Fiyat") + '</p><p class="sectls-price prd-value" itemprop="price" content="' + price + '">' + price + '</p></div><div class="sectls-qnt-wrap"><p>' + translate_words(language, "Adet") + '</p><p class="qnt-wrap sectls-qnt"><a href="#" class="qnt-minus sectls-minus">-</a><input type="text" value="1"><a href="#" class="qnt-plus sectls-plus">+</a></p></div></div><div class="sectls-info"> ' + product_models_html(language, "Modeller", models) + '<p class="sectls-add"><a href="javascript:void(0);" class="add-basket" data-type="' + product_type + '" data-id="' + id + '">' + translate_words(language, "Sepete Ekle") + '</a></p></div></div>';

    return html;
}

function product_quantity() {
    $('.qnt-wrap').on('click', 'a', function () {
        var qnt = $(this).parent().find('input').val();
        var operation_name = "";
        if ($(this).hasClass('qnt-plus')) {
            qnt++;
            operation_name = "add";
        } else if ($(this).hasClass('qnt-minus')) {
            if (qnt > 1) {
                qnt--;
                operation_name = "minus";
            }
        }

        if (qnt > 0) {
            $(this).parent().find('input').val(qnt);
            $(".add-basket").attr("data-quantity", qnt);
        }
        // arttır
        if (location.href.indexOf("/customers/basket/") > 0) {
            var product_type = $(this).parent().find('input').attr("data-type");
            var product_id = $(this).parent().find('input').attr("data-id");
            // console.log("quantity : " + qnt + " | " + "type : " + product_type + " | product id : " + product_id);
            update_basket_in_dashboard(qnt, product_type, product_id, operation_name);
        }

        return false;
    });
}

function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function revise_price(price) {

    if (price != null) {
        if (location.href.indexOf("/tr/") > 0) {
            // price = "₺ " + price.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
            price = "₺ " + numberWithCommas(price);
        } else {
            // price = "$ " + price;
            price = "$ " + numberWithCommas(price);
        }
    } else {
        if (location.href.indexOf("/tr/") > 0) {
            price = "Bilgi Alınız";
        } else {
            price = "Req. Information";
        }
    }


    return price;
}

function products_loadmore() {
    $("#more-prd").on("click", function () {
        var page_type = $(this).attr("data-pagetype");
        var category = $(this).attr("data-category");
        var page = $(this).attr("data-page");


        if (page_type === "list") {
            $.ajax({
                type: 'POST',
                beforeSend: function (xhr, settings) {
                    if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                        // Only send the token to relative URLs i.e. locally.
                        xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
                    }
                    $(".result-loading").html('');
                    $(".result-loading").html('<img src="/static/images/ajax-loader.gif" class="loading-functions" />');
                },
                url: '/more-products/',
                data: {
                    'category': category,
                    'page': page,
                    'language': get_current_language(),
                },
                success: function (data) {
                    $(".result-loading").html('');
                    $.each(data.products, function (index, value) {

                        $(".section-list").append(product_html(value.slug, value.cover, value.title, value.oem_number, value.sub_number, value.prd_models, data.language, value.category[0].name, value.category[0].slug, value.id, value.product_type, revise_price(value.price)));

                    });
                    if (data.has_next) {
                        $("#more-prd").attr("data-page", data.page);
                    } else {
                        $("#more-prd").hide();
                    }
                    product_quantity();
                    add_basket();

                },
                error: function (data) {

                }
            });
        } else if (page_type === "all") {
            $.ajax({
                type: 'POST',
                beforeSend: function (xhr, settings) {
                    if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                        // Only send the token to relative URLs i.e. locally.
                        xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
                    }
                    $(".result-loading").html('');
                    $(".result-loading").html('<img src="/static/images/ajax-loader.gif" class="loading-functions" />');
                },
                url: '/more-products-index/',
                data: {
                    'page': page,
                    'language': get_current_language(),
                },
                success: function (data) {
                    $(".result-loading").html('');
                    $.each(data.products, function (index, value) {

                        $(".section-list").append(product_html(value.slug, value.cover, value.title, value.oem_number, value.sub_number, value.prd_models, data.language, value.category_json.name, value.category_json.slug, value.id, value.product_type, revise_price(value.price)));

                    });
                    if (data.has_next) {
                        $("#more-prd").attr("data-page", data.page);
                    } else {
                        $("#more-prd").hide();
                    }
                    product_quantity();
                    add_basket();
                },
                error: function (data) {

                }
            });
        }


    });

}

function readmore_product_description() {

    if (location.href.indexOf("/products/detail/") > 0) {
        $("#prod-showdesc").on("click", function () {
            if ($(".prod-tabs").find("#prod-desc").hasClass("active") === false) {
                $(".prod-tabs").find("#prod-props").removeClass("active");
                $(".prod-tabs").find("#prod-reviews").removeClass("active");
                $("#prod-tab-2").css("display", "none");
                $("#prod-tab-3").css("display", "none");
                $(".prod-tabs").find("#prod-desc").addClass("active");
                $("#prod-tab-1").css("display", "block");


            }
        });
    }


}

function category_html(cover_image, title, category_url, parent_title, parent_url, languages) {
    var html = "";
    var parts_str = "";
    if (languages === "tr") {
        parts_str = "Parçalar";
    } else {
        parts_str = "Parts";
    }

    html = '<div class="sectgl" itemprop="category" itemscope itemtype="https://schema.org/Service"><a href="' + category_url + '" class="sectgl-link"><p class="sectgl-img"><img src="' + cover_image + '" alt="' + title + '" itemprop="image"></p><h3 itemprop="name"><span style="top: 0;">' + title + '</span></h3></a><p class="sectgl-info"><a href="' + parent_url + '" class="sectgl-categ">' + parent_title + '</a><a href="#" class="sectgl-add">+ ' + parts_str + '</a></p></div>';


    return html;
}


function more_categories() {

    $("#more-category").on("click", function () {
        var page = $(this).attr("data-page");
        var category = $(this).attr("data-category");

        $.ajax({
            type: 'POST',
            beforeSend: function (xhr, settings) {
                if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                    // Only send the token to relative URLs i.e. locally.
                    xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
                }
                $(".result-loading").html('');
                $(".result-loading").html('<img src="/static/images/ajax-loader.gif" class="loading-functions" />');
            },
            url: '/more-categories/',
            data: {
                'category': category,
                'page': page,
                'language': get_current_language(),
            },
            success: function (data) {
                $(".result-loading").html('');
                $.each(data.categories, function (index, value) {

                    $(".category-item-list").append(category_html(value.cover, value.title, value.slug, value.parent.title, value.parent.slug, data.language));

                });
                if (data.has_next) {
                    $("#more-category").attr("data-page", data.page);
                } else {
                    $("#more-category").hide();
                }

            },
            error: function (data) {

            }
        });

    });

}

function all_categories_more() {
    $("#all-category").on("click", function () {
        var page = $(this).attr("data-page");

        $.ajax({
            type: 'POST',
            beforeSend: function (xhr, settings) {
                if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                    // Only send the token to relative URLs i.e. locally.
                    xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
                }
                $(".result-loading").html('');
                $(".result-loading").html('<img src="/static/images/ajax-loader.gif" class="loading-functions" />');
            },
            url: '/all-categories/',
            data: {
                'page': page,
                'language': get_current_language(),
            },
            success: function (data) {
                $(".result-loading").html('');
                $.each(data.categories, function (index, value) {

                    $(".category-item-list").append(category_html(value.cover, value.title, value.slug, value.parent.title, value.parent.slug, data.language));

                });
                if (data.has_next) {
                    $("#all-category").attr("data-page", data.page);
                } else {
                    $("#all-category").hide();
                }

            },
            error: function (data) {

            }
        });

    });
}

function keycode_control(key_code) {

    var key_codes = [16, 35, 36, 32, 34, 33, 20, 17, 18, 225, 17, 45, 35, 145, 19]

    return !key_codes.includes(key_code);

}

function hide_search_results() {
    $(document).on("click", function () {
        if ($("html").has(".search-results")) {

            $(".search-results").html('');
            $(".search-results").hide();
            $(".all-results").hide();
        }
    });

    $(".section-menu-btn").on("click", function () {
        if ($("html").has(".search-results")) {

            $(".search-results").html('');
            $(".search-results").hide();
            $(".all-results").hide();
        }
    });
}

function isEmailValid(emailAdress) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(emailAdress);
}


function create_customer() {
    $(".register-user").find(".result-message").hide();
    if (location.href.indexOf("/register/") > 0) {
        $("#register-customer").on("click", function () {
            var form = $(".register-user");
            var first_name = form.find(".form-name").val().trim();
            var last_name = form.find(".form-lastname").val().trim();
            var email_address = form.find(".form-email").val().trim();
            var password = form.find(".form-password").val().trim();
            var repassword = form.find(".form-repassword").val().trim();

            if (isEmailValid(email_address)) {
                if (password === repassword) {
                    _create_customer_ajax(first_name, last_name, repassword, email_address);
                } else {
                    console.log("pass uyuşmadı");
                }
            } else {
                console.log("email hatalı");
            }

        });
    }

}

function _create_customer_ajax(first_name, last_name, password, email_address) {
    $.ajax({
        type: 'POST',
        beforeSend: function (xhr, settings) {
            if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                // Only send the token to relative URLs i.e. locally.
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
            }

            $(".register-user").find(".result-message").show();

            $(".register-user").find(".result-message").html('<img src="/static/images/ajax-loader.gif" class="loading-functions" />');

        },
        url: '/register/',
        data: {
            'firstname': first_name,
            'lastname': last_name,
            'password': password,
            'email_address': email_address,
        },
        success: function (data) {
            console.log(data);
            $(".register-user").find(".result-message").html('');

            switch (data.situation) {
                case true:
                    $(".register-user").find(".result-message").append('<p class="result-message success-message">' + data.message + '</p>');
                    setTimeout(function () {
                        window.location.href = data.redirect_url;
                    }, 2000);
                    break;

                case false:
                    $(".register-user").find(".result-message").append('<p class="result-message err-message">' + data.message + '</p>');
                    setTimeout(function () {
                        window.location.href = data.redirect_url;
                    }, 2000);
                    break;

                default:
                    break;
            }

        },
        error: function (data) {

        }
    });
}

function logout() {
    $("#logout-btn").on("click", function () {
        $.post('/logout/', function () {
        }).done(function (data) {
            window.location.href = data.redirect_url;
        });

    });
}

function login() {
    if (location.href.indexOf("/login/") > 0) {
        $("#login-customer").on("click", function () {
            var form = $(".login-user");
            var language = $(this).attr("data-language");
            var email_address = form.find(".form-email").val().trim();
            var password = form.find(".form-password").val().trim();
            var err_message = "";
            if (isEmailValid(email_address)) {
                $(".form-result-message").html('');
                $(".form-result-message").hide();
                login_ajax(email_address, password);
            } else {
                if (language === "tr") {
                    err_message = "Email adresiniz hatalı girilmiş görünüyor. Lütfen düzgün girerek tekrardan deneyiniz.";
                } else {
                    err_message = "Your email address is wrong. Please enter correct one.";
                }
                $(".form-result-message").html('<p class="error-message">' + err_message + '</p>');
                $(".form-result-message").show();
            }
        });
    }
}

function login_ajax(email, password) {

    $.ajax({
        type: 'POST',
        beforeSend: function (xhr, settings) {
            if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                // Only send the token to relative URLs i.e. locally.
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
            }

        },
        url: '/login/',
        data: {
            'email': email,
            'password': password,
        },
        success: function (data) {
            // $(document).find(".login-result").find(".loading-page").remove();
            // console.log(data);
            switch (data.situation) {
                case true:
                    $(".form-result-message").html('');
                    $(".form-result-message").hide();
                    window.location.href = data.redirect_url;
                    break;
                case false:
                    $(".form-result-message").html('<p class="error-message text-center">' + data.message + '</p>');
                    $(".form-result-message").show();
                    break;
                default:
                    break;
            }
        },
        error: function (data) {

        }
    });

}

function _reset_password_ajax(page_type, email) {
    if (page_type === "accounts") {
        $.post('/reset-password-account/', {'page_type': page_type, 'email': email}, function () {

        }).done(function (data) {

            if ($(".result-mails").find(".result-message").length > 0) {
                $(".result-mails").find(".result-message").remove();
                $(".result-mails").find(".loading-functions");
            }

            switch (data.situation) {
                case true:
                    $(".result-mails").append('<p class="result-message success-message">' + data.message + '</p>');
                    setTimeout(function () {
                        window.location.href = data.redirect_url;
                    }, 2000);
                    break;
                case false:
                    $(".result-mails").append('<p class="result-message err-message">' + data.message + '</p>');
                    setTimeout(function () {
                        window.location.href = data.redirect_url;
                    }, 2000);

                    break;
            }

        });

    } else {

        $.post('/reset-password-account/', {'page_type': page_type, 'email': email}, function () {
        }).done(function (data) {

            if ($(".reset-password").find(".form-result-message").length > 0) {
                $(".reset-password").find(".form-result-message").remove();
                $(".form-result-message").hide();
            }

            if ($(".reset-password").find(".general-loading").length > 0) {
                $(".reset-password").find(".general-loading").remove();
            }

            switch (data.situation) {
                case true:
                    $(".reset-password").append('<div class="form-result-message"><p class="success-message result-message">' + data.message + '</p></div>');
                    $(".form-result-message").show();

                    setTimeout(function () {
                        window.location.href = data.redirect_url;
                    }, 2000);
                    break;
                case false:
                    $(".reset-password").append('<div class="form-result-message"><p class="error-message result-message">' + data.message + '</p></div>');
                    $(".form-result-message").show();
                    // setTimeout(function () {
                    //     window.location.href = data.redirect_url;
                    // }, 2000);

                    break;
            }

        });
    }
}


function reset_password_with_email() {
    if (location.href.indexOf("/accounts/") > 0) {
        var email = "";
        $(".change-password-email").on("click", function () {
            $(".result-mails").append('<img src="/static/images/ajax-loader.gif" class="loading-functions" />');
            _reset_password_ajax("accounts", email);
        });
    } else if (location.href.indexOf("/reset-password/") > 0) {
        // page_type = remember-password

        $("#reset-password-btn").on("click", function () {
            var email = $(".reset-password").find("form").find(".form-email").val().trim();

            if ($(".reset-password").find(".form-result-message").length > 0) {
                $(".reset-password").find(".form-result-message").remove();
                $(".form-result-message").hide();
            }

            if (isEmailValid(email)) {
                $(".reset-password").append('<img src="/static/images/ajax-loader.gif" class="general-loading" />');

                _reset_password_ajax("reset-pass", email);
            } else {
                if (location.href.indexOf("/tr/") > 0) {
                    $(".reset-password").append('<div class="form-result-message"><p class="error-message text-center">E-posta adresinizi doğru formatta giriniz.</p></div>');
                    $(".form-result-message").show();
                } else {
                    $(".reset-password").append('<div class="form-result-message"><p class="error-message text-center">Please enter your email address right format.</p></div>');
                    $(".form-result-message").show();
                }

            }

        });
        //**********************************************

    }

}

function _update_pass_ajax(current_pass, new_pass, email) {
    // console.log("update fong");
    $.ajax({
        type: 'POST',
        beforeSend: function (xhr, settings) {
            if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                // Only send the token to relative URLs i.e. locally.
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
            }

            if (email.length > 0) {
                if ($(".changing-password").find(".result-message").length > 0) {
                    $(".changing-password").find(".result-message").remove();
                }
            } else {
                if ($("#password-form").find(".result-message").length > 0) {
                    $("#password-form").find(".result-message").remove();
                }
            }

        },
        url: '/update-password/',
        data: {
            'current_pass': current_pass,
            'new_pass': new_pass,
            'email': email,
        },
        success: function (data) {


            switch (data.situation) {
                case true:
                    $(".changing-password").find(".general-loading").remove();
                    if (email.length > 0) {
                        $(".changing-password").append('<p class="result-message success-message">' + data.message + '</p>');
                    } else {
                        $("#password-form").append('<p class="result-message success-message">' + data.message + '</p>');
                    }
                    setTimeout(function () {
                        window.location.href = data.redirect_url;
                    }, 2000);
                    break;
                case false:
                    $(".changing-password").find(".general-loading").remove();
                    if (email.length > 0) {
                        $(".changing-password").append('<p class="result-message err-message">' + data.message + '</p>');
                    } else {
                        $("#password-form").append('<p class="result-message err-message">' + data.message + '</p>');
                    }
                    break;
            }

        },
        error: function (data) {

        }
    });
}

function update_password() {
    if (location.href.indexOf("/accounts/") > 0) {
        $(".change-password-button").on("click", function () {
            var form = $(".newpass-form");
            var current_pass = form.find(".form-current").val().trim();
            var new_pass = form.find(".form-newpass").val().trim();
            var new_repass = form.find(".form-renewpass").val().trim();

            if ($("#password-form").find(".result-message").length > 0) {
                $("#password-form").find(".result-message").remove();
            }

            if (new_pass === new_repass) {
                _update_pass_ajax(current_pass, new_repass, "")
            } else {

                if (location.href.indexOf("/tr/") > 0) {
                    $("#password-form").append('<p class="result-message err-message">Yeni girdiğiniz şifre uyumlu olmadığı görünüyor. Tekrar yeni şifre giriniz.</p>');
                } else {
                    $("#password-form").append('<p class="result-message err-message">It seems that the password you just entered is not compatible. Enter new password again, please.</p>');
                }

            }


        })
    } else if (location.href.indexOf("/reset-password/") > 0) {
        $("#renew-password-btn").on("click", function () {
            // var form = $(".changing-password").find("form");
            var pass = $(".changing-password").find("form").find(".form-password").val().trim();
            var repass = $(".changing-password").find("form").find(".form-repassword").val().trim();
            var email = $(this).attr("data-mail");

            if ($(".changing-password").find(".result-message").length > 0) {
                $(".changing-password").find(".result-message").remove();
            }

            if (pass === repass) {
                $(".changing-password").append('<img src="/static/images/ajax-loader.gif" class="general-loading"/>');

                _update_pass_ajax("", repass, email);
            } else {

                if (location.href.indexOf("/tr/") > 0) {
                    $(".changing-password").append('<p class="result-message err-message">Yeni girdiğiniz şifre uyumlu olmadığı görünüyor. Tekrar yeni şifre giriniz.</p>');
                } else {
                    $(".changing-password").append('<p class="result-message err-message">It seems that the password you just entered is not compatible. Enter new password again, please.</p>');
                }
            }
        });
    }

}


function password_form() {
    if (location.href.indexOf("/accounts/") > 0) {
        $('.change-password').fancybox({
            padding: 0,
            content: $('#password-form'),
            helpers: {
                overlay: {
                    locked: false
                }
            },
            tpl: {
                closeBtn: '<a title="Close" class="fancybox-item fancybox-close modal-form-close" href="javascript:;"></a>'
            }
        });
    }

}

function account_form_save() {
    if (location.href.indexOf("/accounts/") > 0) {
        $("#account-customer").on("click", function () {
            var form = $(".account-form").find("form");
            var firstname = form.find(".form-firstname").val().trim();
            var lastname = form.find(".form-lastname").val().trim();
            var phone_number = form.find(".form-phone").val().trim();
            var is_cargo = false;
            var is_vat = false;
            var address_type = form.find(".address-type").val().trim();
            var address_text = form.find(".form-address").val().trim();
            var post_code = form.find(".form-postcode").val().trim();

            if (form.find('.is-cargo').is(':checked')) {
                is_cargo = true;
            }
            if (form.find('.is-vat').is(':checked')) {
                is_vat = true;
            }


            account_form_ajax(firstname, lastname, phone_number, address_type, is_cargo, is_vat, post_code, address_text);

        });
    }
}

function account_form_ajax(firstname, lastname, phone_number, address_type, is_cargo, is_vat, postcode, address_text) {

    $.ajax({
        type: 'POST',
        beforeSend: function (xhr, settings) {
            if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                // Only send the token to relative URLs i.e. locally.
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
            }

            if ($(".waiting-system").find(".result-message").length > 0) {
                $(".waiting-system").find(".result-message").remove();
            }

            if ($(".waiting-system").find(".form-loading").length === 0) {
                $("#account-customer").before('<div class="form-loading"></div>');
            }


        },
        url: '/account-info/',
        data: {
            'firstname': firstname,
            'lastname': lastname,
            'phone': phone_number,
            'address_type': address_type,
            'is_cargo': is_cargo,
            'is_vat': is_vat,
            'postcode': postcode,
            'address_text': address_text,
        },
        success: function (data) {
            if ($(".waiting-system").find(".form-loading").length > 0) {
                $(".waiting-system").find(".form-loading").remove();
            }


            switch (data.situation) {
                case true:
                    $("#account-customer").before('<p class="result-message success-message">' + data.message + '</p>');
                    break;

                case false:
                    $("#account-customer").before('<p class="result-message err-message">' + data.message + '</p>');
                    if (data.redirect_url.length > 0) {
                        window.location.href = data.redirect_url;
                    }
                    break;

            }


        },
        error: function (data) {

        }
    });
}

function get_product_quantity(quantity) {
    if (quantity === null || quantity === undefined) {
        return 1;
    } else {
        return quantity;
    }
}

function remove_product_in_dashboard() {
    if (location.href.indexOf("/customers/basket/") > 0) {
        $(".remove-basket").on("click", function () {
            var product_id = $(this).attr('data-id');
            var operation = "remove";
            update_basket_in_dashboard("", "", product_id, operation);
        });
    }
}

function update_basket_in_dashboard(quantity, product_type, product_id, operation) {
    $.ajax({
        type: 'POST',
        beforeSend: function (xhr, settings) {
            if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                // Only send the token to relative URLs i.e. locally.
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
            }

        },
        url: '/update-basket/',
        data: {
            'product_id': product_id,
            'product_type': product_type,
            'quantity': quantity,
            'language': get_current_language(),
            'operation': operation,
        },
        success: function (data) {
            if (data.situation) {
                notification_message("basket", data.situation, data.message);

                // $(".basket-header-div").find(".total-cost").text(data.basket_price);
                $(".basket-header-div").find(".total-cost").text(revise_price(data.basket_price));
                $(".basket-header-div").find(".total-count").text(data.basket_count);

                $(".cont-sections").find("li.active").find("span").text(data.basket_count);
                // $(".cart-btn").find(".cart-cost").text(data.basket_price);
                $(".cart-btn").find(".cart-cost").text(revise_price(data.basket_price));
                $(".cart-btn").attr('title', revise_price(data.basket_price));
                $(".cart-btn").find(".cart-count").text(data.basket_count);


                $.each(data.products, function (index, value) {
                    // console.log(value.product_id === product_id);
                    if (value.product_id === product_id) {
                        if (value.total !== null && value.total !== 0) {
                            if (get_current_language() === "tr") {
                                $(".product-" + value.product_id).find("span").text("Toplam : ₺ " + value.total);
                            } else {
                                $(".product-" + value.product_id).find("span").text("Total : $ " + value.total);
                            }
                        }
                    }
                });

                if (operation === "remove") {

                    window.location.href = "/" + get_current_language() + "/customers/basket";
                }

            } else {
                window.location.href = data.redirect_url;
            }


        },
        error: function (data) {

        }
    });
}


function add_basket() {
    $(".add-basket").on("click", function () {
        var product_id = $(this).attr("data-id");
        var product_type = $(this).attr("data-type");
        var quantity = get_product_quantity($(this).attr("data-quantity"));
        _add_basket_ajax(product_id, product_type, quantity);

    });
}


function _add_basket_ajax(product_id, product_type, quantity) {

    $.ajax({
        type: 'POST',
        beforeSend: function (xhr, settings) {
            if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                // Only send the token to relative URLs i.e. locally.
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
            }

        },
        url: '/add-basket/',
        data: {
            'product_id': product_id,
            'product_type': product_type,
            'quantity': quantity,
            'language': get_current_language(),
        },
        success: function (data) {
            console.log(data);
            notification_message("basket", data.situation, data.message);
            update_basket(data.total_price, data.basket_count);
        },
        error: function (data) {

        }
    });
}

function get_current_language() {
    if (location.href.indexOf("/tr/") > 0) {
        return "tr";
    } else {
        return "en";
    }
}


function update_basket(total_price, basket_count) {
    if (location.href.indexOf("/tr/") > 0) {
        $(document).find(".header-personal ul li a.active-basket").html('Sepet ' + '<span>' + basket_count + '</span>')
    } else {
        $(document).find(".header-personal ul li a.active-basket").html('Basket ' + '<span>' + basket_count + '</span>')
    }

    $(document).find(".cart-btn").find(".cart-cost").text(revise_price(total_price));
    $(document).find(".cart-btn").attr("title", revise_price(total_price));
    $(document).find(".cart-btn").find(".cart-count").html(basket_count);


}


function notification_message(message_type, situation, message) {
    var cssClass = "";
    if (situation)
        cssClass = "success-notification";
    else
        cssClass = "error-notification";

    if (message_type === "basket") {
        notification_bar(cssClass, message);
    } else if (message_type === "shopping") {
        notification_bar(cssClass, message);
    }
    else if (message_type === "email") {
        notification_bar(cssClass, message);
    }
}

function notification_bar(cssClass, message) {
    $.notifyBar({
        html: message,
        delay: 1000,
        animationSpeed: 200,
        cssClass: cssClass,
        close: false,
        closeText: '&times;',
        position: 'top',
        onBeforeShow: null,
        onShow: null,
        onBeforeHide: null,
        onHide: null,
    });
}


function card_form() {

    if (location.href.indexOf("/customers/credit-card/") > 0 || location.href.indexOf("/customers/orders/") > 0) {
        var acc = document.getElementsByClassName("accordion");
        var i;

        for (i = 0; i < acc.length; i++) {
            acc[i].addEventListener("click", function () {
                /* Toggle between adding and removing the "active" class,
                to highlight the button that controls the panel */
                this.classList.toggle("active-accordion");

                /* Toggle between hiding and showing the active panel */
                var panel = this.nextElementSibling;
                if (panel.style.display === "block") {
                    panel.style.display = "none";
                } else {
                    panel.style.display = "block";
                }
            });
        }


    }


}

function payment_worksheet() {
    if (location.href.indexOf("/customers/credit-card/") > 0) {

        if (($(".basket-content").find("li").length - 1) < 2) {
            $(".basket-content li").css('width', '100%');
        } else if (($(".basket-content").find("li").length - 1) === 2) {
            $(".basket-content li").css('width', '48%');
        } else {
            $(".basket-content li").css('width', '32%');
        }

    }
}

function current_payment_form() {
    if (location.href.indexOf("/customers/current-payment/") > 0) {
        var currency_prefix = '';
        if (get_current_language() === "tr")
            currency_prefix = ' ₺';
        else
            currency_prefix = ' $';

        $("#cardcost").maskMoney({
            thousands: ',',
            decimal: '.',
            allowZero: false,
            suffix: "",
            affixesStay: true,
            prefix: currency_prefix,
        });
    } else if (location.href.indexOf("/customers/credit-card/") > 0) {
        $('#cardcost').maskMoney('unmasked')[0];
    }

}

function payment_form_type_error_messages(input_type) {
    var message = "";
    if (get_current_language() === "tr") {
        switch (input_type) {
            case "name":
                message = "Kart sahibinin adını giriniz.";
                break;
            case "card_number":
                message = "Kart numarasını giriniz.";
                break;
            case "card_date":
                message = "Kart tarihini giriniz.";
                break;
            case "security_number":
                message = "Kartın arka yüzeyindeki güvenlik kodunu giriniz.";
                break;
            case "card_cost":
                message = "Ödenecek tutar 0 olamaz.";
                break;
            default:
                break;
        }
    } else {
        switch (input_type) {
            case "name":
                message = "Enter the name of the card holder.";
                break;
            case "card_number":
                message = "Enter the card number.";
                break;
            case "card_date":
                message = "Enter the expiry date of the card.";
                break;
            case "security_number":
                message = "Enter the security code on the back of the card.";
                break;
            case "card_cost":
                message = "The amount due cannot be $ 0";
                break;
            default:
                break;
        }
    }

    return message;
}

function check_payment_form(str_value) {
    if (str_value.toString().length > 0) {
        return true;
    } else {
        return false;
    }
}

function complete_shopping() {
    if (location.href.indexOf("/customers/current-payment/") > 0 || location.href.indexOf("/customers/credit-card/") > 0) {
        $("#complete-shopping").on("click", function () {
            // console.log("tık");
            var payment_type = $(this).attr("data-payment-type");
            var card_name = $("#name").val().trim();
            var card_number = $("#cardnumber").val().trim();
            var card_date = $("#expirationdate").val().trim();
            var security_number = $("#securitycode").val().trim();
            var error_message = "";
            var form_situation = false;

            if (payment_type === "credit-card") {
                if (check_payment_form(card_name)) {
                    form_situation = true;
                    if (check_payment_form(card_number) && form_situation === true) {
                        form_situation = true;
                        if (check_payment_form(card_date) && form_situation === true) {
                            form_situation = true;
                            if (check_payment_form(security_number) && form_situation === true) {
                                form_situation = true;
                            } else {
                                error_message = payment_form_type_error_messages("security_number");
                                form_situation = false;
                            }
                        } else {
                            error_message = payment_form_type_error_messages("card_date");
                            form_situation = false;
                        }
                    } else {
                        error_message = payment_form_type_error_messages("card_number");
                        form_situation = false;
                    }
                } else {
                    form_situation = false;
                    error_message = payment_form_type_error_messages("name");
                }

                if (form_situation) {
                    pay_with_creditcard(card_number, card_name, card_date, security_number);
                } else {
                    $(".error-messages").html('');
                    $(".error-messages").html('<p>' + error_message + '</p>');
                }


            } else {
                var card_cost = $("#cardcost").val().trim();
                var currency_type = "";

                if (check_payment_form(card_name)) {
                    form_situation = true;
                    if (check_payment_form(card_number) && form_situation === true) {
                        form_situation = true;
                        if (check_payment_form(card_date) && form_situation === true) {
                            form_situation = true;
                            if (check_payment_form(security_number) && form_situation === true) {
                                form_situation = true;
                                if (get_current_language() === "tr") {
                                    card_cost = card_cost.toString().replace('₺', '');
                                    currency_type = "₺";
                                } else {
                                    card_cost = card_cost.toString().replace('$', '');
                                    currency_type = "$";
                                }

                                if (parseFloat(card_cost) > 0 && form_situation) {
                                    form_situation = true;
                                } else {
                                    error_message = payment_form_type_error_messages("card_cost");
                                    form_situation = false;
                                }

                            } else {
                                error_message = payment_form_type_error_messages("security_number");
                                form_situation = false;
                            }
                        } else {
                            error_message = payment_form_type_error_messages("card_date");
                            form_situation = false;
                        }
                    } else {
                        error_message = payment_form_type_error_messages("card_number");
                        form_situation = false;
                    }
                } else {
                    form_situation = false;
                    error_message = payment_form_type_error_messages("name");
                }

                if (form_situation) {
                    card_cost = card_cost.toString().replace(',', '');
                    card_cost = parseFloat(card_cost).toFixed(2);
                    // console.log(card_cost);
                    pay_with_current(card_number, card_name, card_date, security_number, card_cost, currency_type);
                } else {
                    // console.log("burası");
                    $(".error-messages").html('');
                    $(".error-messages").html('<p>' + error_message + '</p>');
                }


            }
            // return false;
        });
    }

}

function pay_with_current(card_number, card_name, card_date, security_number, cardcost, currency_type) {
    $.ajax({
        type: 'POST',
        beforeSend: function (xhr, settings) {
            if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                // Only send the token to relative URLs i.e. locally.
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
            }

            $(".error-messages").html('<img src="/static/images/ajax-loader.gif" class="loading revize-loading-position" />');

            // $("#complete-shopping").attr("disabled", true);
            // $("#complete-shopping").addClass("disables-payment");

        },
        url: '/current-payment/',
        data: {
            'card_number': card_number,
            'card_name': card_name,
            'card_date': card_date,
            'security_number': security_number,
            'language': get_current_language(),
            'pay_cost': cardcost,
            'currency_type': currency_type,
        },
        success: function (data) {
            $(".error-messages").html('');

            console.log(data);

            notification_message("shopping", data.situation, data.message);
            // update_basket(data.total_price, data.basket_count);
            if (data.situation) {
                setTimeout(function () {
                    window.location.href = data.redirect_url;
                }, 2000);
            } else {
                $("#complete-shopping").attr("disabled", false);
                $("#complete-shopping").removeClass("disables-payment");
            }
        },
        error: function (data) {

        }
    });
}

function pay_with_creditcard(card_number, card_name, card_date, security_number) {
    $.ajax({
        type: 'POST',
        beforeSend: function (xhr, settings) {
            if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                // Only send the token to relative URLs i.e. locally.
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
            }

            $(".error-messages").html('<img src="/static/images/ajax-loader.gif" class="loading revize-loading-position" />');

            $("#complete-shopping").attr("disabled", true);
            $("#complete-shopping").addClass("disables-payment");

        },
        url: '/credit-card/',
        data: {
            'card_number': card_number,
            'card_name': card_name,
            'card_date': card_date,
            'security_number': security_number,
            'language': get_current_language(),
        },
        success: function (data) {
            $(".error-messages").html('');

            // console.log(data);

            notification_message("shopping", data.completed[0].situation, data.completed[0].message);
            // update_basket(data.total_price, data.basket_count);
            if (data.situation) {
                setTimeout(function () {
                    window.location.href = data.redirect_url;
                }, 2000);
            } else {
                $("#complete-shopping").attr("disabled", false);
                $("#complete-shopping").removeClass("disables-payment");
            }
        },
        error: function (data) {

        }
    });
}

function contact_form_validate(form_name, form_mail, form_phone, form_message) {
    var situation = false;

    if (form_name.toString().trim().length > 0) {
        situation = true;
        if (isEmailValid(form_mail) && situation) {
            situation = true;
            if (form_phone.toString().trim().length > 0 && situation) {
                situation = true;
                if (form_message.toString().trim().length > 0 && situation) {
                    situation = true;
                } else {
                    situation = false;
                }
            } else {
                situation = false;
            }
        } else {
            situation = false;
        }
    } else {
        situation = false;
    }

    return situation;
}

function contact_us() {
    if (location.href.indexOf("/iletisim/") > 0 || location.href.indexOf("/contact/") > 0) {
        $("#contact-btn").on("click", function () {
            var form_name = $(".form-name").val();
            var form_mail = $(".form-email").val();
            var form_phone = $(".form-phone").val();
            var form_message = $(".form-message").val();

            if (contact_form_validate(form_name, form_mail, form_phone, form_message)) {
                $.ajax({
                    type: 'POST',
                    beforeSend: function (xhr, settings) {
                        if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                            // Only send the token to relative URLs i.e. locally.
                            xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
                        }
                        $(".result-loading").html('');
                        $(".result-loading").html('<img src="/static/images/ajax-loader.gif" class="loading-functions contact-loading" />');
                    },
                    url: '/send-message/',
                    data: {
                        'name': form_name,
                        'email': form_mail,
                        'phone': form_phone,
                        'message': form_message,
                        'language': get_current_language(),
                    },
                    success: function (data) {
                        $(".result-loading").html('');
                        notification_message("email",data.situation,data.message);
                        $(".form-name").val('');
                        $(".form-email").val('');
                        $(".form-phone").val('');
                        $(".form-message").val('');
                    },
                    error: function (data) {

                    }
                });
            } else {
                $(".result-loading").html('');

                if (get_current_language() === "tr") {

                    notification_message("email",false,'Sizinle sağlıklı iletişim kurabilmemiz için formu tam olarak doldurmalısınız.');
                } else {
                    notification_message("email",false,'You must fill out the form completely so that we can communicate with you properly.');

                }

            }

        });
    }

}