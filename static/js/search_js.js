function autocomplete_ajax(search_text) {

    $.ajax({
        type: 'POST',
        beforeSend: function (xhr, settings) {
            if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                // Only send the token to relative URLs i.e. locally.
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
            }
            $(".search-results").html('');
            $(".search-results").hide();
            // $(".all-results").hide();
        },
        url: '/search-products/',
        data: {
            'search_text': search_text,
            'language':get_current_language(),
        },
        success: function (data) {
            /*enter a basınca ilk sonuca git*/
            if (data.length > 0) {
                $(".search-results").html('');
                $(".search-results").show();


                $.each(data, function (index, value) {

                    $(".search-results").append(search_html(value.slug, value.title, value.cover, value.oem_number, value.sub_number));


                });

                searchbox_height(data.length);

                $(".search-results").customScrollbar({
                    updateOnWindowResize: true,
                });

                // if (data.length > 4) {
                //     if ($(".autocomplete-box").has(".all-results").length < 1) {
                //         $(".scrollable").after('<a href="#!" class="all-results">Tüm Sonuçlar</a> </div>')
                //         $(".all-results").show();
                //     }
                //     else{
                //         $(".all-results").show();
                //     }
                // }
                // else{
                //     $(".all-results").hide();
                // }


            } else {
                $(".search-results").html('');
                $(".search-results").hide();
                // $(".all-results").hide();

            }



        },
        error: function (data) {
            $(".search-results").html('');
            $(".search-results").hide();

        }
    });


}

function searchbox_height(box_size) {
    if (box_size > 4) {
        $(".search-results").css("height", "240");
    } else if (box_size > 2 && box_size < 4) {
        $(".search-results").css("height", "180");
    } else if (box_size > 1 && box_size < 3) {
        $(".search-results").css("height", "120");
    } else {
        $(".search-results").css("height", "60");
    }
}

function search_html(product_slug, product_title, cover_image, oem_number, sub_number) {
    var html = "";

    if (cover_image.length === 0) {
        cover_image = "/static/images/empty.png";
    }
    if (sub_number !== "" || sub_number !== null || sub_number !== " ") {
        html = '<li itemprop="product" itemscope itemtype="http://schema.org/Product"><a href="' + product_slug + '"><img src="' + cover_image + '"/> <h4 itemprop="oem-number">' + oem_number + '</h4><h5 itemprop="name">' + product_title + '</h5> </a> </li>';
    } else {
        html = '<li itemprop="product" itemscope itemtype="http://schema.org/Product"><a href="' + product_slug + '"><img src="' + cover_image + '"/> <h4 itemprop="oem-number">' + oem_number + " - " + sub_number + '</h4><h5 itemprop="name">' + product_title + '</h5> </a> </li>';
    }

    return html;
}

function search_html_more() {
    var html = "";

    html = '<li><a href="#!">Bütün Sonuçlar</a> </li>';

    return html;
}

