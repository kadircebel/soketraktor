from django.db import models
from django.contrib.auth.models import User

class Customers(models.Model):
    id = models.BigAutoField(primary_key=True)
    users = models.ForeignKey(User, models.DO_NOTHING, unique=True, verbose_name="Müşteri")
    phone_number = models.CharField(max_length=25,verbose_name="Telefon No",blank=True,null=True)
    email_approved = models.BooleanField(default=False,verbose_name="Email Onaylı Mı?")
    is_pass_reset = models.BooleanField(default=False,verbose_name="Parola Restleme İsteği ?")
    random_pass = models.CharField(max_length=50,verbose_name="Random Şifre",default=None,blank=True,null=True)

    class Meta:
        managed = True
        db_table = "customers"
        verbose_name = "Müşteri"
        verbose_name_plural = "Müşteriler"

    def __str__(self):
        return "{} {}".format(self.users.first_name,self.users.last_name)


    @classmethod
    def random_pass_customer(cls,user_obj,random_pass):
        selected =  cls.objects.get(users=user_obj)
        selected.random_pass = random_pass
        selected.is_pass_reset = True
        selected.save()
        return selected


    @classmethod
    def create_customer(cls,user_obj):
        if not cls.objects.filter(users=user_obj).exists():
            new_customer = cls(users=user_obj)
            new_customer.save()
            return new_customer
        else:
            return None

    @classmethod
    def get_customer(cls,user_obj):
        return cls.objects.get(users=user_obj)

    @classmethod
    def get_customer_by_id(cls,customer_id):
        if cls.objects.filter(id=customer_id).exists():
            return cls.objects.get(id=customer_id)
        else:
            return None

    @classmethod
    def get_customer_info(cls,user_obj):
        selected = cls.objects.get(users=user_obj)
        context_address = []
        phone_number =""
        if selected.customersaddress_set.exists():
            for item in selected.customersaddress_set.all():
                item_address = {
                    'address_type':item.address_type,
                    'address_text':item.address_text,
                    'zipcode':item.post_code,
                    'is_cargo' : item.is_cargo_address,
                    'is_vat':item.is_vat_address,
                }

                context_address.append(item_address)
        else:

            if selected.phone_number != None:
                phone_number = selected.phone_number
            else:
                phone_number = ""


        context = {
            'firstname':selected.users.first_name,
            'lastname':selected.users.last_name,
            'phone':phone_number,
            'address_list':context_address,
        }



        return context

    @classmethod
    def save_customer_accounts(cls,user_obj,firstname,lastname,phone,address_type,address_text,is_cargo,is_vat,postcode):

        selected = cls.objects.get(users=user_obj)
        if address_type != None:
            CustomersAddress.save_customer_address(selected,address_type,address_text,is_cargo,is_vat,postcode)

        selected.phone_number = phone
        selected.users.first_name = firstname
        selected.users.last_name = lastname
        selected.users.save()
        selected.save()


        return selected



class CustomersAddress(models.Model):
    customers = models.ForeignKey('customers.Customers',on_delete=models.CASCADE,db_index=True,verbose_name="Müşteri")
    DEFAULT_ADDRESS = (None,"Seçiniz")
    ADDRESS_TYPES ={
        ('work', 'İş Adresi'),
        ('vat', 'Fatura Adresi'),
        ('cargo', 'Kargo Adresi'),
    }
    address_type = models.CharField(max_length=15,verbose_name="Adres Tipi",default=DEFAULT_ADDRESS,choices=ADDRESS_TYPES)

    address_text = models.TextField(verbose_name="Adres Bilgisi",blank=True,null=True)
    post_code = models.CharField(verbose_name="Posta Kodu",blank=True,null=True,max_length=15)
    is_cargo_address = models.BooleanField(default=True,verbose_name="Kargo Adresi Mi?")
    is_vat_address = models.BooleanField(default=True,verbose_name="Fatura Adresi Mi?")

    class Meta:
        managed = True
        db_table = "customers_address"
        verbose_name = "Müşteri Adresi"
        verbose_name_plural = "Müşteri Adresleri"

    def __str__(self):
        return self.address_type

    @classmethod
    def remove_check_all(cls,customer_obj):
        try:
            selected_list = cls.objects.filter(customers=customer_obj).all()

            for item in selected_list:
                item.is_cargo_address = False
                item.is_vat_address = False
                item.save()

            return True
        except:
            return False


    @classmethod
    def save_customer_address(cls,customer_obj,address_type,address_text,is_cargo,is_vat,postcode):

        if address_type == "cargo":
            is_cargo = True
        elif address_type == "vat":
            is_vat = True

        if is_cargo is True:
            CustomersAddress.remove_check_all(customer_obj)

        if is_vat is True:
            CustomersAddress.remove_check_all(customer_obj)


        if cls.objects.filter(customers=customer_obj,address_type=address_type).exists():

            #kayıt güncelleme
            selected_address = cls.objects.get(customers=customer_obj, address_type=address_type)
            selected_address.address_text = address_text
            selected_address.is_cargo_address = is_cargo
            selected_address.is_vat_address = is_vat
            selected_address.post_code = postcode
            selected_address.save()


            return selected_address
        else:


            new_address = cls(customers=customer_obj,address_type=address_type,address_text=address_text,is_cargo_address=is_cargo,is_vat_address=is_vat,post_code=postcode)
            new_address.save()
            return new_address

    @classmethod
    def isthere_address(cls,customer_obj,address_type):
        if cls.objects.filter(customers=customer_obj, address_type=address_type).exists():
            return True
        elif cls.objects.filter(customers=customer_obj,is_cargo_address=True):
            return True
        elif cls.objects.filter(customers=customer_obj,is_vat_address=True):
            return True
        else:
            return False

    @classmethod
    def get_customer_address(cls,customer_obj):
        return cls.objects.filter(customers=customer_obj).all()

    @classmethod
    def get_customer_selected_address(cls,customer_obj,address_type):
        if cls.objects.filter(customers=customer_obj,address_type=address_type).exists():
            selected = cls.objects.get(customers=customer_obj,address_type=address_type)
            return selected.address_text + " - " + selected.post_code
        elif address_type == "vat":
            if cls.objects.filter(customers=customer_obj,is_vat_address=True).exists():
                selected = cls.objects.get(customers=customer_obj,is_vat_address=True)
                return selected.address_text + " - " + selected.post_code
            else:
                return None
        elif address_type == "cargo":
            if cls.objects.filter(customers=customer_obj,is_cargo_address=True).exists():
                selected = cls.objects.get(customers=customer_obj,is_cargo_address=True)
                return selected.address_text + " - " + selected.post_code
            else:
                return None
        else:
            return None
