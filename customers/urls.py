from django.urls import path
from .views import *

app_name = "customers"

urlpatterns = [
    path('dashboard/', dashboard.as_view(), name='dashboard'),
    path('accounts/', accounts_view.as_view(), name='accounts'),
    path('orders/', orders.as_view(), name='orders'),
    path('credit-card/', payment_credit_card.as_view(), name='credit_card'),
    path('current-payment/', payment_custom_payment.as_view(), name='custom-payment'),
    path('basket/', basket.as_view(), name='basket'),
    path('reset-password/', reset_password_view.as_view(), name='reset_password_view'),
]
