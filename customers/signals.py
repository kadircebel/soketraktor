from django.db.models.signals import post_save
from django.dispatch import receiver
from .models import Customers
from django.contrib.auth.models import User

@receiver(post_save, sender=User)
def create_customer(sender, instance, created, **kwargs):

    if created:
        if not Customers.objects.filter(users=instance).exists():
            Customers.create_customer(instance)
            print("customer created")

