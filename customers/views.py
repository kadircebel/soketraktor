from django.shortcuts import render, HttpResponse, redirect
from django.views import View
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
from django.utils import translation
import json
from pages.models import Menus, Pages,CompanyInfos,MetaDescriptionTags
from django.contrib.auth.models import User
from customers.models import Customers, CustomersAddress
from django.contrib.auth import authenticate, login, logout
from helpers.views import create_random_password, forgetten_password_mail, basket_count, total_basket_price, change_basket_for_language, calculate_basket, update_basket_in_dashboard, address_notifications,notification_messages, phone_notifications, unzip_basket_from_session, complete_creditcard_shopping,test_credit_cards,notifications_count,sendmail_accounting
from basket.models import Basket,CurrentPayments



@method_decorator(csrf_exempt, name='dispatch')
class register(View):
    template_name = "customers/register.html"

    def get(self, request):
        # logout(request)
        # del request.session["customer_id"]
        # print(request.session["customer_id"])
        menus = Menus.get_menus(translation.get_language(), "top")
        bottom_menu = Menus.get_menus(translation.get_language(), "bottom")
        if translation.get_language() == "tr":
            selected_menu = Menus.get_menu(translation.get_language(), "kayit-ol", "")
            selected_meta = MetaDescriptionTags.get_selected_meta("kayit-ol", translation.get_language())
            change_lang = "/en/register"
        else:
            change_lang = "/tr/register"
            selected_menu = Menus.get_menu(translation.get_language(), "register", "")
            selected_meta = MetaDescriptionTags.get_selected_meta("register", translation.get_language())

        selected_page = Pages.objects.page_detail(selected_menu, translation.get_language())

        # basket operations
        if "basket" in request.session:
            request.session["basket"] = change_basket_for_language(request, translation.get_language())
            basket_totalcount = basket_count(request.session["basket"])
            basket_totalprice = total_basket_price(request.session["basket"], translation.get_language())
        else:
            basket_totalprice = None
            basket_totalcount = 0

        # basket operations

        context = {
            'top_menu': menus,
            'bottom_menu': bottom_menu,
            'page': selected_page,
            'page_type': 'register',
            'basket_count': basket_totalcount,
            'basket_price': basket_totalprice,
            'footer_address': CompanyInfos.get_company_address(),
            'footer_email': CompanyInfos.get_company_email('email'),
            'meta_tags': selected_meta,
            'change_language': change_lang,
        }

        return render(request, self.template_name, context)

    def post(self, request):
        first_name = request.POST.get("firstname")
        last_name = request.POST.get("lastname")
        email_address = request.POST.get("email_address")
        passsword = request.POST.get("password")

        if "customer_id" not in request.session:
            if not User.objects.filter(email=email_address).exists():
                user = User.objects.create_user(email=email_address, username=email_address, password=passsword)
                user.first_name = first_name
                user.last_name = last_name
                user.save()
                login(request, user)

                if user is not None:
                    request.session["customer_id"] = Customers.get_customer(user).id

                if translation.get_language() == "tr":
                    message = "Başarılı bir şekilde kayıt oluşturdunuz. Yönlendiriliyorsunuz..."
                else:
                    message = "You've successfully registered. Redirecting..."

                context = {
                    'situation': True,
                    'redirect_url': '/' + translation.get_language() + "/customers/dashboard",
                    'message': message,
                }
            else:
                # todo kullanıcı var
                if translation.get_language() == "tr":
                    message = "Girdiğiniz e-posta adresine ait bir kullanıcı zaten bulunuyor. Farklı bir e-posta adresi ile kayıt olmanız gerekmektedir."
                else:
                    message = "It seems that you have already signed up with this email account. You've to register with different email address."

                context = {
                    'situation': False,
                    'message': message,
                    'redirect_url': '/' + translation.get_language() + "/login",
                }

            return HttpResponse(json.dumps(context), content_type="application/json")

        else:
            # logout(request)
            # del request.session["customer_id"]
            if translation.get_language() == "tr":
                message = "Yeni bir kullanıcı oluşturmak için öncelikle kendi kullanıcınız üzerinden çıkış yapmalısınız."
            else:
                message = "You can register when you logged out. Thanks."

            context = {
                'situation': False,
                'redirect_url': '/' + translation.get_language() + "/customers/dashboard",
                'message': message,
            }
            return HttpResponse(json.dumps(context), content_type="application/json")


@method_decorator(csrf_exempt, name='dispatch')
class login_view(View):
    template_name = "customers/login_html.html"

    def get(self, request):
        menus = Menus.get_menus(translation.get_language(), "top")
        bottom_menu = Menus.get_menus(translation.get_language(), "bottom")

        if translation.get_language() == "tr":
            selected_menu = Menus.get_menu(translation.get_language(), "giris-yap", "")
            selected_meta = MetaDescriptionTags.get_selected_meta("giris-yap", translation.get_language())
            change_lang = "/en/login"
        else:
            change_lang = "/tr/login"
            selected_meta = MetaDescriptionTags.get_selected_meta("log-in", translation.get_language())
            selected_menu = Menus.get_menu(translation.get_language(), "log-in", "")

        selected_page = Pages.objects.page_detail(selected_menu, translation.get_language())

        # basket operations
        if "basket" in request.session:
            request.session["basket"] = change_basket_for_language(request, translation.get_language())
            basket_totalcount = basket_count(request.session["basket"])
            basket_totalprice = total_basket_price(request.session["basket"], translation.get_language())
        else:
            basket_totalprice = None
            basket_totalcount = 0

        # basket operations

        context = {
            'top_menu': menus,
            'bottom_menu': bottom_menu,
            'page': selected_page,
            'page_type': 'login',
            'basket_count': basket_totalcount,
            'basket_price': basket_totalprice,
            'footer_address': CompanyInfos.get_company_address(),
            'footer_email': CompanyInfos.get_company_email('email'),
            'meta_tags': selected_meta,
            'change_language': change_lang,
        }
        return render(request, self.template_name, context)

    def post(self, request):
        email = request.POST.get("email")
        password = request.POST.get("password")
        user = authenticate(request, username=email, password=password)
        if user is not None:
            login(request, user)
            request.session["customer_id"] = Customers.get_customer(user).id
            # // todo superuser mi diye kontrol et. ona göre yönlendirme linki oluştur.
            redirect_url = "/" + translation.get_language() + "/customers/dashboard"
            message = ""
            situation = True
        else:
            redirect_url = ""
            situation = False
            if translation.get_language() == "tr":
                message = "Hatalı kullanıcı adı veya şifre girişi yaptınız."
            else:
                message = "Wrong username or password."

        context = {
            'redirect_url': redirect_url,
            'message': message,
            'situation': situation,
        }

        return HttpResponse(json.dumps(context), content_type="application/json")


@method_decorator(csrf_exempt, name='dispatch')
class logout_view(View):
    template_name = ""

    def get(self, request):
        pass

    def post(self, request):

        if request.user.is_superuser:
            logout(request)
            context = {
                'redirect_url': '/' + translation.get_language() + "/login",
            }
            return HttpResponse(json.dumps(context), content_type="application/json")

        else:

            if "customer_id" in request.session:
                logout(request)
                # del request.session["customer_id"]

                context = {
                    'redirect_url': '/' + translation.get_language() + "/login",
                }

                return HttpResponse(json.dumps(context), content_type="application/json")


@method_decorator(csrf_exempt, name='dispatch')
class dashboard(View):
    template_name = "customers/dashboard.html"

    def get(self, request):
        notification_count = 0
        context_notifications = []
        menus = Menus.get_menus(translation.get_language(), "top")
        bottom_menu = Menus.get_menus(translation.get_language(), "bottom")
        # basket operations
        if "basket" in request.session:
            request.session["basket"] = change_basket_for_language(request, translation.get_language())
            basket_totalcount = basket_count(request.session["basket"])
            basket_totalprice = total_basket_price(request.session["basket"], translation.get_language())
        else:
            basket_totalprice = None
            basket_totalcount = 0

        # basket operations

        if request.user.is_superuser:
            return redirect("/admin")
        else:
            if not request.user.is_authenticated and "customer_id" not in request.session:
                return redirect("/{}/{}".format(translation.get_language(), 'login'))
            else:
                # print(request.session["customer_id"])
                selected_customer = Customers.get_customer_by_id(request.session["customer_id"])
                if selected_customer is not None:
                    # notifications - address
                    notification_count = notifications_count(Customers.get_customer_by_id(request.session["customer_id"]))
                    if len(address_notifications(selected_customer)) < 1:
                        item_message = {
                            'message_type': 'address',
                            'message': notification_messages("address", translation.get_language()),
                            'title_tr': 'Kargo ve fatura adres bilgilerinizi girmelisiniz.',
                            'title_en': 'You must enter your billing and shipping addresses.',
                            'slug': '/' + translation.get_language() + '/customers/accounts/#address-form'
                        }
                        context_notifications.append(item_message)

                    if phone_notifications(request.session["customer_id"]) is not None:
                        item_message = {
                            'message_type': 'phone',
                            'message': notification_messages("phone", translation.get_language()),
                            'title_tr': 'Telefon numarası bilginizi girmelisiniz.',
                            'title_en': 'You must enter your phone number.',
                            'slug': '/' + translation.get_language() + '/customers/accounts'
                        }
                        context_notifications.append(item_message)

                else:
                    customer_address = []

            if translation.get_language() == "tr":
                change_lang = "/en/customers/dashboard/"
            else:
                change_lang = "/tr/customers/dashboard/"

            context = {
                'top_menu': menus,
                'bottom_menu': bottom_menu,
                'page_type': 'notifications',
                'basket_count': basket_totalcount,
                'basket_price': basket_totalprice,
                'notifications': context_notifications,
                'notification_count': notification_count,
                'order_count':Basket.get_order_count(selected_customer),
                'footer_address': CompanyInfos.get_company_address(),
                'footer_email': CompanyInfos.get_company_email('email'),
                'change_language': change_lang,
            }

            return render(request, self.template_name, context)

    def post(self, request):
        pass


@method_decorator(csrf_exempt, name='dispatch')
class update_password_view(View):
    template_name = ""

    def get(self, request):
        pass

    def post(self, request):

        current_pass = request.POST.get("current_pass")
        new_pass = request.POST.get("new_pass")
        email = request.POST.get("email")

        if len(str(current_pass)) == 0 and len(str(email)) > 0:
            user = User.objects.get(username=email)
        else:
            user = authenticate(request, username=request.user.email, password=current_pass)

        if (request.user.is_authenticated and "customer_id" in request.session) or user is not None:
            if user.customers_set.get().is_pass_reset is True and user is not None:
                if len(str(new_pass)) > 5:
                    selected_user = User.objects.get(username=user.username)
                    selected_user.set_password(new_pass)
                    selected_user.save()

                    if translation.get_language() == "tr":
                        message = "Şifreniz başarılı bir şekilde değişmiştir."
                    else:
                        message = "Your password has been changed successfully."

                    situation = True

                    selected_customer = selected_user.customers_set.get()
                    selected_customer.random_pass = None
                    selected_customer.is_pass_reset = False
                    selected_customer.save()
                    redirect_url = "/" + translation.get_language() + "/login"

                else:
                    if translation.get_language() == "tr":
                        message = "Şifreniz en az 6 karakterli olmalıdır."
                    else:
                        message = "Your password must be at least 6 characters."

                    situation = False
                    redirect_url = ""
            else:
                if translation.get_language() == "tr":
                    message = "Güncel şifrenizi doğru girmelisiniz."
                else:
                    message = "You must enter your current password correctly."

                situation = False

                redirect_url = ""
        else:
            redirect_url = "/" + translation.get_language() + "/login"
            situation = False
            message = ""

        context = {
            'redirect_url': redirect_url,
            'situation': situation,
            'message': message,
        }
        return HttpResponse(json.dumps(context), content_type="application/json")


@method_decorator(csrf_exempt, name='dispatch')
class accounts_view(View):
    template_name = "customers/dashboard.html"

    def get(self, request):
        notification_count = 0
        menus = Menus.get_menus(translation.get_language(), "top")
        bottom_menu = Menus.get_menus(translation.get_language(), "bottom")

        # basket operations
        if "basket" in request.session:
            request.session["basket"] = change_basket_for_language(request, translation.get_language())
            basket_totalcount = basket_count(request.session["basket"])
            basket_totalprice = total_basket_price(request.session["basket"], translation.get_language())
        else:
            basket_totalprice = None
            basket_totalcount = 0

        # basket operations

        if translation.get_language() == "tr":
            change_lang = "/en/customers/accounts/"
        else:
            change_lang = "/tr/customers/accounts/"

        if request.user.is_superuser:
            return redirect("/admin")
        else:
            if not request.user.is_authenticated and "customer_id" not in request.session:
                return redirect("/{}/{}".format(translation.get_language(), 'login'))
            else:
                selected_customer = Customers.get_customer_info(request.user)
                notification_count = notifications_count(Customers.get_customer_by_id(request.session["customer_id"]))

                context = {
                    'top_menu': menus,
                    'bottom_menu': bottom_menu,
                    'page_type': 'accounts',
                    'customer': selected_customer,
                    'basket_count': basket_totalcount,
                    'basket_price': basket_totalprice,
                    'order_count':Basket.get_order_count(Customers.get_customer_by_id(request.session["customer_id"])),
                    'notification_count': notification_count,
                    'footer_address': CompanyInfos.get_company_address(),
                    'footer_email': CompanyInfos.get_company_email('email'),
                    'change_language': change_lang,
                }

                return render(request, self.template_name, context)

    def post(self, request):
        firstname = request.POST.get("firstname")
        lastname = request.POST.get("lastname")
        phone = request.POST.get("phone_number")
        address_type = request.POST.get("address_type")
        address_text = request.POST.get("address_text")
        is_cargo = request.POST.get("is_cargo")
        is_vat = request.POST.get("is_vat")
        postcode = request.POST.get("postcode")

        if is_cargo == "true":
            is_cargo = True
        else:
            is_cargo = False

        if is_vat == "true":
            is_vat = True
        else:
            is_vat = False

        if request.user.is_authenticated and "customer_id" in request.session:
            if address_type == "none":
                address_type = None

            selected_customer = Customers.save_customer_accounts(request.user, firstname, lastname, phone, address_type,
                                                                 address_text, bool(is_cargo), bool(is_vat), postcode)

            if selected_customer != None:
                situation = True
                if translation.get_language() == "tr":
                    message = "Bilgileriniz başarılı bir şekilde güncellenmiştir."
                else:
                    message = "Your information has been updated successfully."
            else:
                situation = False
                if translation.get_language() == "tr":
                    message = "Bilgilerinizin kayıt edilme aşamasında bir problem ile karşılaşıldı. Daha sonra tekrar deneyiniz."
                else:
                    message = "A problem was encountered during the recording of your information. Please try again later."

            context = {
                'situation': situation,
                'message': message,
                'redirect_url': "",
            }

        else:
            context = {
                'situation': False,
                'message': "",
                'redirect_url': "/{}/{}".format(translation.get_language(), 'login'),
            }

        return HttpResponse(json.dumps(context), content_type="applications/json")


@method_decorator(csrf_exempt, name='dispatch')
class reset_password_view(View):
    template_name = "customers/reset_password.html"

    def get(self, request):
        menus = Menus.get_menus(translation.get_language(), "top")
        bottom_menu = Menus.get_menus(translation.get_language(), "bottom")

        # basket operations
        if "basket" in request.session:
            request.session["basket"] = change_basket_for_language(request, translation.get_language())
            basket_totalcount = basket_count(request.session["basket"])
            basket_totalprice = total_basket_price(request.session["basket"], translation.get_language())
        else:
            basket_totalprice = None
            basket_totalcount = 0

        # basket operations

        if translation.get_language() == "tr":
            selected_menu = Menus.get_menu(translation.get_language(), "sifre-sifirlama", "")
        else:
            selected_menu = Menus.get_menu(translation.get_language(), "reset-password", "")

        selected_page = Pages.objects.page_detail(selected_menu, translation.get_language())

        if not request.user.is_authenticated:

            if request.method == "GET":
                try:
                    user_name = request.GET.get("user")
                    random_pass = request.GET.get("rp")

                    selected_user = User.objects.get(username=user_name)

                    if selected_user is not None and selected_user.customers_set.get().is_pass_reset:
                        context = {
                            'top_menu': menus,
                            'bottom_menu': bottom_menu,
                            'page_type': 'change-password',
                            'page': selected_page,
                            'email': user_name,
                            'message': '',
                            'basket_count': basket_totalcount,
                            'basket_price': basket_totalprice,
                            'footer_address': CompanyInfos.get_company_address(),
                            'footer_email': CompanyInfos.get_company_email('email')
                        }

                        return render(request, self.template_name, context)
                    else:
                        if translation.get_language() == "tr":
                            message = "Şifre yenileme talebi daha önceden gerçekleştirildiği için yeniden 'şifremi unuttum' bölümüne gitmelisiniz."
                        else:
                            message = "Since the password renewal request has been made before, you should go to the 'forgot password' section again."

                        context = {
                            'top_menu': menus,
                            'bottom_menu': bottom_menu,
                            'page_type': 'change-password',
                            'page': selected_page,
                            'email': user_name,
                            'message': message,
                            'basket_count': basket_totalcount,
                            'basket_price': basket_totalprice,
                            'footer_address': CompanyInfos.get_company_address(),
                            'footer_email': CompanyInfos.get_company_email('email')
                        }

                        return render(request, self.template_name, context)

                except:

                    context = {
                        'top_menu': menus,
                        'bottom_menu': bottom_menu,
                        'page_type': 'remember-password',
                        'page': selected_page,
                        'footer_address': CompanyInfos.get_company_address(),
                        'footer_email': CompanyInfos.get_company_email('email')
                    }

                    return render(request, self.template_name, context)

        else:
            logout(request)
            return redirect("/{}/{}".format(translation.get_language(), 'login'))

    def post(self, request):
        page_type = request.POST.get("page_type")
        if request.is_secure():
            host_address = "https://" + request.get_host() + "/"
        else:
            host_address = "http://" + request.get_host() + "/"

        if page_type == "accounts":
            if request.user.is_authenticated and "customer_id" in request.session:

                random_pass = create_random_password()

                selected_customer = Customers.random_pass_customer(request.user, random_pass)
                is_mail_send = forgetten_password_mail(request.user.email, host_address, random_pass,
                                                       translation.get_language())

                if selected_customer is not None and is_mail_send is True:
                    situation = True
                else:
                    situation = False

                if situation is True:
                    if translation.get_language() == "tr":
                        message = "Şifre sıfırlama talebiniz başarılı bir şekilde e-posta adresinize gönderildi."
                    else:
                        message = "Your password reset request has been successfully sent to your e-mail address."
                else:
                    if translation.get_language() == "tr":
                        message = "Şifre sıfırlama talebi sırasında bir sorun meydana geldi. Daha sonra tekrar deneyiniz."
                    else:
                        message = "A problem occurred during the password reset request."

            else:
                situation = False
                if translation.get_language() == "tr":
                    message = "Öncelikle giriş yapmış olmalısınız. Çıkış yapıp şifre sıfırlama talebinde bulunabilirsiniz."
                else:
                    message = "First, you must be logged in. You can log out and request a password reset."

            redirect_url = "/" + translation.get_language() + "/login"
            logout(request)

            context = {
                'redirect_url': redirect_url,
                'message': message,
                'situation': situation,
            }

            return HttpResponse(json.dumps(context), content_type="application/json")

        else:
            email_address = request.POST.get("email")
            if User.objects.filter(username=email_address).exists():
                selected_user = User.objects.get(username=email_address)
                if selected_user is not None:
                    random_pass = create_random_password()
                    selected_customer = Customers.random_pass_customer(selected_user, random_pass)
                    is_mail_send = forgetten_password_mail(selected_user.email, host_address, random_pass,
                                                           translation.get_language())
                    if selected_customer is not None and is_mail_send is True:
                        situation = True
                    else:
                        situation = False

                    if situation:
                        if translation.get_language() == "tr":
                            message = "Şifre yenileme mesajınız e-posta adresinize gönderilmiştir."
                        else:
                            message = "Your password message has been sent to your e-mail address."
                    else:
                        if translation.get_language() == "tr":
                            message = "Girilen e-posta adresine ait bir kullanıcı bulunmuyor."
                        else:
                            message = "There's no user for this email address."

                    context = {
                        'redirect_url': "/" + translation.get_language() + "/login",
                        'message': message,
                        'situation': situation,
                    }

                    return HttpResponse(json.dumps(context), content_type="application/json")

                else:
                    situation = False
                    if translation.get_language() == "tr":
                        message = "Girilen e-posta adresine ait bir kullanıcı bulunmuyor."
                    else:
                        message = "There's no user for this email address."

                    context = {
                        'redirect_url': "",
                        'message': message,
                        'situation': situation,
                    }

                    return HttpResponse(json.dumps(context), content_type="application/json")

            else:
                situation = False
                if translation.get_language() == "tr":
                    message = "Girilen e-posta adresine ait bir kullanıcı bulunmuyor."
                else:
                    message = "There's no user for this email address."

                context = {
                    'redirect_url': "",
                    'message': message,
                    'situation': situation,
                }

                return HttpResponse(json.dumps(context), content_type="application/json")


@method_decorator(csrf_exempt, name='dispatch')
class basket(View):
    template_name = "customers/dashboard.html"

    def get(self, request):

        menus = Menus.get_menus(translation.get_language(), "top")
        bottom_menu = Menus.get_menus(translation.get_language(), "bottom")
        notification_count = 0
        # basket operations
        if "basket" in request.session:
            request.session["basket"] = change_basket_for_language(request, translation.get_language())
            basket_totalcount = basket_count(request.session["basket"])
            basket_totalprice = total_basket_price(request.session["basket"], translation.get_language())

            context_basket = calculate_basket(request.session["basket"], translation.get_language())
            message = ""

        else:
            if translation.get_language() == "tr":
                message = "Sepetinizde henüz bir ürün bulunmuyor."
            else:
                message = "There is no product in your basket yet."

            context_basket = []
            basket_totalprice = None
            basket_totalcount = 0

        # basket operations

        if translation.get_language() == "tr":
            change_lang = "/en/customers/basket"
        else:
            change_lang = "/tr/customers/basket"

        if request.user.is_superuser:
            return redirect("/admin")
        else:
            if not request.user.is_authenticated and "customer_id" not in request.session:
                return redirect("/{}/{}".format(translation.get_language(), 'login'))
            else:
                # print(request.session["customer_id"])
                selected_customer = Customers.get_customer_by_id(request.session["customer_id"])
                notification_count = notifications_count(selected_customer)

                # if request.session["customer_id"] = :

            # print("*******////////////*************")
            # print(context_basket)
            # print("*******////////////*************")

            context = {
                'top_menu': menus,
                'bottom_menu': bottom_menu,
                'page_type': 'basket',
                'basket_count': basket_totalcount,
                'basket_price': basket_totalprice,
                'products': context_basket,
                'notification_count': notification_count,
                'message': message,
                'order_count':Basket.get_order_count(selected_customer),
                'footer_address': CompanyInfos.get_company_address(),
                'footer_email': CompanyInfos.get_company_email('email'),
                'change_language': change_lang,
            }

            return render(request, self.template_name, context)

    def post(self, request):

        if "basket" in request.session:
            product_id = request.POST.get("product_id")
            product_type = request.POST.get("product_type")
            quantity = request.POST.get("quantity")
            language = request.POST.get("language")
            operation = request.POST.get("operation")

            request.session["basket"] = update_basket_in_dashboard(request, product_id, product_type, language,
                                                                   quantity, operation)

            if len(request.session["basket"]) > 0:

                basket_totalcount = basket_count(request.session["basket"])

                if language == "tr":
                    # basket_totalprice = "₺ {}".format(total_basket_price(request.session["basket"],translation.get_language()))
                    basket_totalprice = total_basket_price(request.session["basket"], translation.get_language())

                    if operation == "minus":
                        message = "Ürün adeti başarılı bir şekilde çıkarıldı."
                    elif operation == "add":
                        message = "Ürün adeti başarılı bir şekilde eklendi."
                    else:
                        message = "Ürün başarılı bir şekilde kaldırıldı."
                else:
                    # basket_totalprice = "$ {}".format(total_basket_price(request.session["basket"],translation.get_language()))
                    basket_totalprice = total_basket_price(request.session["basket"], translation.get_language())
                    if operation == "minus":
                        message = "Product count was successfully decreased."
                    elif operation == "add":
                        message = "Product count was successfully increased."
                    else:
                        message = "Product was successfully removed."

                context = {
                    'products': request.session["basket"],
                    'basket_count': basket_totalcount,
                    'basket_price': basket_totalprice,
                    'situation': True,
                    'message': message,
                    'redirect_url': '',
                }

                return HttpResponse(json.dumps(context), content_type="application/json")

            else:
                del request.session["basket"]

                if language == "tr":
                    message = "Sepetinizde henüz bir ürün bulunmuyor."
                else:
                    message = "There is no product in your basket yet."

                context = {
                    'products': [],
                    'basket_count': 0,
                    'basket_price': 0,
                    'situation': False,
                    'message': message,
                    'redirect_url': '/' + language + '/products',
                }

                return HttpResponse(json.dumps(context), content_type="application/json")


@method_decorator(csrf_exempt, name='dispatch')
class orders(View):
    template_name = "customers/dashboard.html"

    def get(self, request):
        menus = Menus.get_menus(translation.get_language(), "top")
        bottom_menu = Menus.get_menus(translation.get_language(), "bottom")
        notification_count = 0
        orders = []
        # basket operations
        if "basket" in request.session:
            request.session["basket"] = change_basket_for_language(request, translation.get_language())
            basket_totalcount = basket_count(request.session["basket"])
            basket_totalprice = total_basket_price(request.session["basket"], translation.get_language())
        else:
            basket_totalprice = None
            basket_totalcount = 0

        # basket operations

        if translation.get_language() == "tr":
            change_lang = "/en/customers/orders"
        else:
            change_lang = "/tr/customers/orders"

        if request.user.is_superuser:
            return redirect("/admin")
        else:
            if not request.user.is_authenticated and "customer_id" not in request.session:
                return redirect("/{}/{}".format(translation.get_language(), 'login'))
            else:
                selected_customer = Customers.get_customer_by_id(request.session["customer_id"])
                notification_count = notifications_count(selected_customer)
                orders = Basket.select_paid_baskets(selected_customer, translation.get_language())
                current_orders = CurrentPayments.get_current_payments(selected_customer)


            context = {
                'top_menu': menus,
                'bottom_menu': bottom_menu,
                'page_type': 'orders',
                'basket_count': basket_totalcount,
                'basket_price': basket_totalprice,
                'orders':orders,
                'order_count':Basket.get_order_count(selected_customer),
                'current_orders':current_orders,
                'notification_count': notification_count,
                'footer_address': CompanyInfos.get_company_address(),
                'footer_email': CompanyInfos.get_company_email('email'),
                'change_language': change_lang,
            }

            return render(request, self.template_name, context)

    def post(self, request):
        pass

@method_decorator(csrf_exempt, name='dispatch')
class payment_credit_card(View):
    template_name = "customers/dashboard.html"

    def get(self, request):
        menus = Menus.get_menus(translation.get_language(), "top")
        bottom_menu = Menus.get_menus(translation.get_language(), "bottom")
        context_basket = []
        notification_count = 0

        if translation.get_language() == "tr":
            change_lang = "/en/customers/credit-card"
        else:
            change_lang = "/tr/customers/credit-card"

        if request.user.is_superuser:
            return redirect("/admin")
        else:
            if not request.user.is_authenticated and "customer_id" not in request.session:
                return redirect("/{}/{}".format(translation.get_language(), 'login'))
            else:
                selected_customer = Customers.get_customer_by_id(request.session["customer_id"])
                if selected_customer is not None:
                    notification_count = notifications_count(selected_customer)
                    if CustomersAddress.isthere_address(selected_customer, "vat") and CustomersAddress.isthere_address(selected_customer, "cargo"):
                        situation = True
                        # basket operations
                        if "basket" in request.session:
                            request.session["basket"] = change_basket_for_language(request, translation.get_language())
                            basket_totalcount = basket_count(request.session["basket"])
                            basket_totalprice = total_basket_price(request.session["basket"],
                                                                   translation.get_language())
                            context_basket = unzip_basket_from_session(request.session["basket"],
                                                                       translation.get_language())
                            basket_situation = True
                        else:
                            basket_totalprice = None
                            basket_totalcount = 0
                            context_basket = []
                            basket_situation = False


                        if basket_situation is False:
                            if translation.get_language() == "tr":
                                message = "Sepetinizde ödeyebileceğiniz bir ürün bulunmuyor."
                            else:
                                message = "There is no product in your basket."
                        else:
                            message = ""

                        # basket operations


                    else:
                        situation = False
                        basket_totalprice = None
                        basket_totalcount = 0
                        context_basket = []
                        basket_situation = False

                        if translation.get_language() == "tr":
                            message = "Sistemde fatura ve kargo adresleriniz kayıtlı görünmüyor. Öncelikle bunları kayıt etmelisiniz. Daha sonra tekrardan ödeme işlemine devam edebilirsiniz."
                        else:
                            message = "Your billing and shipping address not seen in the system. First, you've to add your billing and shipping address. After that you can continue the payment process again."
                        # kargo ve fatura adresini gir mesajı

                else:
                    situation = False
                    basket_totalprice = None
                    basket_totalcount = 0
                    context_basket = []
                    basket_situation = False
                    # böyle bir kullanıcı yok veya giriş yapmalısınız mesajı
                    if translation.get_language() == "tr":
                        message = "Müşteri hesabınız ile ilgili bir problem görünüyor. Tekrardan giriş işlemi yapmayı deneyebilirsiniz veya bizimle iletişim kurabilirsiniz. Anlayışınız için teşekkür ederiz."
                    else:
                        message = "There is a problem in your account. You can log in again or you contact with us. Thanks."


                context = {
                    'top_menu': menus,
                    'bottom_menu': bottom_menu,
                    'page_type': 'credit-card',
                    'basket_count': basket_totalcount,
                    'basket_price': basket_totalprice,
                    'situation': situation,
                    'basket_situation': basket_situation,
                    'message': message,
                    'notification_count': notification_count,
                    'basket': context_basket,
                    'payment_type': 'credit-card',
                    'order_count': Basket.get_order_count(selected_customer),
                    'footer_address': CompanyInfos.get_company_address(),
                    'footer_email': CompanyInfos.get_company_email('email'),
                    'change_language': change_lang,
                }

                # print(context_basket)

                return render(request, self.template_name, context)

    def post(self, request):
        card_number = request.POST.get("card_number")
        card_name = request.POST.get("card_name")
        card_date = request.POST.get("card_date")
        security_number = request.POST.get("security_number")
        language = request.POST.get("language")

        # print("card number : ", card_number)
        # print("card name : ", card_name)
        # print("card date : ", card_date)
        # print("security : ", security_number)

        if request.user.is_superuser:
            return redirect("/admin")
        else:
            # basket operations
            if "basket" in request.session and request.user.is_authenticated:
                request.session["basket"] = change_basket_for_language(request, translation.get_language())
                selected_customer = Customers.get_customer_by_id(request.session["customer_id"])
                basket_totalcount = basket_count(request.session["basket"])
                basket_totalprice = total_basket_price(request.session["basket"], translation.get_language())

                # context_basket = unzip_basket_from_session(request.session["basket"], translation.get_language())

                if test_credit_cards(str(card_number).replace(' ', '')):
                    completed_shopping = complete_creditcard_shopping(request, language, selected_customer, True)
                    situation = True
                    # message = completed_shopping.get("message")
                    message = ""
                else:
                    situation = False
                    message = ""
                    completed_shopping = []
                    # message = "Girdiğiniz kart bilgileriyle ilgili bir sorun görünüyor."

            else:
                basket_totalprice = None
                basket_totalcount = 0
                completed_shopping = []
                situation = False
                message = "Lütfen giriş yapınız."
                # context_basket = []

        if situation:
            redirect_url = "/" + language + "/customers/orders"
        else:
            redirect_url = ""

        context = {
            'situation': situation,
            'message': message,
            'completed': completed_shopping,
            'redirect_url':redirect_url,
        }



        return HttpResponse(json.dumps(context), content_type="application/json")


@method_decorator(csrf_exempt, name='dispatch')
class payment_custom_payment(View):
    template_name = "customers/dashboard.html"

    def get(self, request):
        menus = Menus.get_menus(translation.get_language(), "top")
        bottom_menu = Menus.get_menus(translation.get_language(), "bottom")
        notification_count = 0
        if request.user.is_superuser:
            return redirect("/admin")
        else:
            # basket operations
            if "basket" in request.session:
                request.session["basket"] = change_basket_for_language(request, translation.get_language())
                basket_totalcount = basket_count(request.session["basket"])
                basket_totalprice = total_basket_price(request.session["basket"], translation.get_language())
                context_basket = unzip_basket_from_session(request.session["basket"], translation.get_language())

            else:
                basket_totalprice = None
                basket_totalcount = 0
                context_basket = []

            # basket operations
            message = ""
            customer_address = []

            if translation.get_language() == "tr":
                change_lang = "/en/customers/current-payment"
            else:
                change_lang = "/tr/customers/current-payment"

            if not request.user.is_authenticated and "customer_id" not in request.session:
                return redirect("/{}/{}".format(translation.get_language(), 'login'))
            else:
                # print(request.session["customer_id"])
                selected_customer = Customers.get_customer_by_id(request.session["customer_id"])
                if selected_customer is not None:
                    notification_count = notifications_count(selected_customer)
                    # notifications
                    if CustomersAddress.isthere_address(selected_customer, "vat") and CustomersAddress.isthere_address(
                            selected_customer, "cargo"):
                        customer_address = CustomersAddress.get_customer_address(selected_customer)
                        situation = True
                    else:
                        # adres bilgileri yok
                        customer_address = []
                        situation = False
                        if translation.get_language() == "tr":
                            message = "Sistemde fatura ve kargo adresleriniz kayıtlı görünmüyor. Öncelikle bunları kayıt etmelisiniz. Daha sonra tekrardan ödeme işlemine devam edebilirsiniz."
                        else:
                            message = "Your billing and shipping address not seen in the system. First, you've to add your billing and shipping address. After that you can continue the payment process again."

                else:
                    # böyle bir kullanıcı yok
                    situation = False
                    if translation.get_language() == "tr":
                        message = "Müşteri hesabınız ile ilgili bir problem görünüyor. Tekrardan giriş işlemi yapmayı deneyebilirsiniz veya bizimle iletişim kurabilirsiniz. Anlayışınız için teşekkür ederiz."
                    else:
                        message = "There is a problem in your account. You can log in again or you contact with us. Thanks."

            context = {
                'top_menu': menus,
                'bottom_menu': bottom_menu,
                'page_type': 'custom-payment',
                'basket_count': basket_totalcount,
                'basket_price': basket_totalprice,
                'situation': situation,
                'message': message,
                'customer_address': customer_address,
                'basket': context_basket,
                'payment_type': 'current-payment',
                'order_count': Basket.get_order_count(selected_customer),
                'notification_count': notification_count,
                'footer_address': CompanyInfos.get_company_address(),
                'footer_email': CompanyInfos.get_company_email('email'),
                'change_language': change_lang,
            }

        return render(request, self.template_name, context)

    def post(self, request):
        card_number = request.POST.get("card_number")
        card_name = request.POST.get("card_name")
        card_date = request.POST.get("card_date")
        security_number = request.POST.get("security_number")
        language = request.POST.get("language")
        paycost = request.POST.get("pay_cost")
        currency_type = request.POST.get("currency_type")

        if request.user.is_superuser:
            return redirect("/admin")
        else:
            if request.user.is_authenticated and "customer_id" in request.session:
                selected_customer = Customers.get_customer_by_id(request.session["customer_id"])
                if test_credit_cards(str(card_number).replace(' ', '')):

                    current_payment = CurrentPayments.create_current_payment(selected_customer,  paycost, currency_type)
                    if current_payment is not None:
                        situation = True
                        if language == "tr":
                            if request.is_secure():
                                host_address = "https://" + request.get_host() + "/"
                            else:
                                host_address = "http://" + request.get_host() + "/"

                            sended_mail = sendmail_accounting(host_address, "Cari Ödeme", current_payment.payment_date,current_payment.customer.users.first_name + " " +current_payment.customer.users.last_name,current_payment.customer.phone_number,CustomersAddress.get_customer_selected_address(selected_customer,"vat"),CustomersAddress.get_customer_selected_address(selected_customer,"cargo"),current_payment.customer.users.email,current_payment.total_price,current_payment.currency_type)

                            # print(sended_mail)

                            message = "Cari ödemeniz başarılı bir şekilde gerçekleşmiştir."
                        else:
                            message = "Your current payment has been made successfully."

                    else:
                        situation = False
                        if language == "tr":
                            message = "Girdiğiniz kart bilgileri ile ilgili bir sorun görünüyor."
                        else:
                            message = "There appears to be a problem with the card information you entered."

                else:
                    situation = False
                    if language == "tr":
                        message = "Girdiğiniz kart bilgileriyle ilgili bir sorun görünüyor."
                    else:
                        message = "There appears to be a problem with the card information you entered."

            else:
                situation = False
                if language == "tr":
                    message = "Oturumunuz sonlanmış görünüyor. Tekrardan giriş yapmalısnız."
                else:
                    message = "Your session seems over. You must log in again."




            if situation:
                redirect_url = "/" + language + "/customers/orders"
            else:
                redirect_url = ""

            context = {
                'situation': situation,
                'message': message,
                'redirect_url': redirect_url,
            }

            return HttpResponse(json.dumps(context), content_type="application/json")