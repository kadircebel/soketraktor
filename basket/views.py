from django.shortcuts import render,HttpResponse
from django.views import View
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
import json
from django.utils import translation
from customers.models import Customers
from .models import *
from pages.models import Menus
from products.models import Products,ExcelProdcts
from payments.models import TaxOptions
from helpers.views import total_basket_price,basket_count,change_basket_for_language
from django.conf import settings
from django.contrib.humanize.templatetags.humanize import intcomma

@method_decorator(csrf_exempt, name='dispatch')
class basket_index(View):
    template_name = ""

    def get(self,request):
        pass


    def post(self,request):

        context_basket = []
        product_id = request.POST.get("product_id")
        product_type = request.POST.get("product_type")
        quantity = request.POST.get("quantity")
        language = request.POST.get("language")

        if request.user.is_superuser:
            if language == "tr":
                message = "Yönetici oturumuna sahip kullanıcılar alışveriş yapamazlar."
            else:
                message = "Yönetici oturumuna sahip kullanıcılar alışveriş yapamazlar."

            item_basket = {}

            context = {
                'current_basket': "",
                'total_price': "",
                'basket_count': "",
                'situation': False,
                'message': message,
            }

            return HttpResponse(json.dumps(context), content_type="application/json")
        else:
            if product_type == "regular-product":

                product = Products.objects.get_regular_product(product_id)

                if product.productsprices_set.exists():
                    product_price = TaxOptions.get_kdv_price(product.productsprices_set.get().price_dl,language)
                    total_price_prd =product_price * int(quantity)
                    # product_price = TaxOptions.get_kdv_price(product.productsprices_set.get().price_dl, language) * int(quantity)
                else:
                    product_price = None
                    total_price_prd = None



            else:

                product = ExcelProdcts.get_outlist_product(product_id)
                if product.price_dl is not None:
                    product_price = TaxOptions.get_kdv_price(product.price_dl,language)
                    # product_price = TaxOptions.get_kdv_price(product.price_dl, language) * int(quantity)
                    total_price_prd =product_price * int(quantity)
                else:
                    product_price = None
                    total_price_prd = None

            if language == "tr":
                message = "Ürün başarılı bir şekilde sepetinize eklendi."
            else:
                message = "The product has been successfully added to your cart."



            item_basket = {
                'price': product_price,
                'total': total_price_prd,
                'quantity': quantity,
                'product_id': product_id,
                'product_type':product_type,
            }



            if "basket" in request.session:
                context_basket = request.session["basket"]
                update_item = False

                for item in context_basket:
                    if item.get("product_id") == product_id:
                        # print("bura")
                        item["quantity"] = int(item.get("quantity")) + int(quantity)
                        update_item = True
                        break
                    else:
                        update_item = False



                if update_item is not True:
                    context_basket.append(item_basket)
                    request.session["basket"] = context_basket
                else:
                    request.session["basket"] = context_basket



                total_price = total_basket_price(context_basket,translation.get_language())
            else:
                context_basket.append(item_basket)
                total_price = total_basket_price(context_basket,translation.get_language())
                request.session["basket"] = context_basket




            context = {
                'current_basket':context_basket,
                'total_price':total_price,
                # 'total_price':intcomma(round(total_price,2))[:5],
                'basket_count':basket_count(context_basket),
                'situation': True,
                'message': message,
            }

            return HttpResponse(json.dumps(context),content_type="application/json")



