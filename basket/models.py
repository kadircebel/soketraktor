from django.db import models
from payments.models import TaxOptions


class Basket(models.Model):
    id = models.BigAutoField(primary_key=True)
    customer = models.ForeignKey('customers.Customers', on_delete=models.DO_NOTHING, verbose_name="Müşteri")
    created_date = models.DateTimeField(auto_now_add=True, verbose_name="Sepet Oluşturma Tarihi")
    is_pay = models.BooleanField(default=False, verbose_name="Ödeme Durumu")

    class Meta:
        managed = True
        db_table = "basket"
        verbose_name = "Müşteri Sepeti"
        verbose_name_plural = "Müşteri Sepetleri"

    def __str__(self):
        return str(self.id)

    @classmethod
    def create_basket(cls, customer_obj, is_paid):
        new_basket = cls(customer=customer_obj, is_pay=is_paid)
        new_basket.save()
        return new_basket

    @classmethod
    def get_order_count(cls, customer_obj):
        total_count = CurrentPayments.current_payements_count(customer_obj) + cls.objects.filter(customer=customer_obj,is_pay=True).count()
        return total_count

    @classmethod
    def select_paid_baskets(cls, customer_obj, language):
        context_basket = []
        if cls.objects.filter(customer=customer_obj).exists():
            basket_list = cls.objects.filter(customer=customer_obj).all().order_by("-created_date")

            for item in basket_list:
                selected_order = Orders.get_basket_price(item)
                if language == "tr":
                    payment_type = selected_order.payment_types.title_tr
                else:
                    payment_type = selected_order.payment_types.title_en

                if selected_order.total_price is not None:
                    if language == "tr":
                        total_price = selected_order.total_price
                    else:
                        total_price = TaxOptions.convert_price_tl(selected_order.total_price)
                else:
                    total_price = None

                item_basket = {
                    'basket_date': item.created_date,
                    'products': BasketItems.get_basket_items(item, language),
                    'total_price': total_price,
                    'payment_type': payment_type,
                    'payment_date': selected_order.payment_date,
                }

                context_basket.append(item_basket)

        return context_basket


class BasketItems(models.Model):
    basket = models.ForeignKey('basket.Basket', verbose_name="Sepet", on_delete=models.DO_NOTHING, null=True,
                               blank=True)
    product = models.ForeignKey('products.Products', verbose_name="Ürün", on_delete=models.DO_NOTHING, null=True,
                                blank=True)
    excel_product = models.ForeignKey('products.ExcelProdcts', verbose_name="Liste Dışı Ürün",
                                      on_delete=models.DO_NOTHING, null=True, blank=True)
    product_price = models.DecimalField(default=0.00, verbose_name='Fiyat - $', decimal_places=2, max_digits=12,
                                        blank=True, null=True)
    quantity = models.IntegerField(default=0, verbose_name="Ürün Adeti")
    added_date = models.DateTimeField(auto_now_add=True, verbose_name="Sepete Eklenen Tarih")

    class Meta:
        managed = True
        db_table = "basket_items"
        verbose_name = "Müşteri Sepet İçeriği"
        verbose_name_plural = "Müşterilerin Sepet İçerikleri"

    @classmethod
    def create_basket_items(cls, customer_basket, selected_product, product_type, quantity, product_price):
        if product_type == "regular-product":
            new_basket_item = cls(basket=customer_basket, product=selected_product, excel_product=None,
                                  quantity=quantity, product_price=product_price)
        else:
            new_basket_item = cls(basket=customer_basket, product=None, excel_product=selected_product,
                                  quantity=quantity, product_price=product_price)

        new_basket_item.save()
        return new_basket_item

    @classmethod
    def get_basket_items(cls, selected_basket, language):
        if cls.objects.filter(basket=selected_basket).exists():
            item_list = cls.objects.filter(basket=selected_basket).all().order_by("added_date")
            context = []
            for item in item_list:
                if item.product is not None:
                    if language == "tr":
                        product_name = item.product.title
                        product_url = '/' + language + '/products/detail/' + item.product.slug
                    else:
                        if item.product.productstranslates_set.exists():
                            product_name = item.product.productstranslates_set.get().title
                            product_url = '/' + language + '/products/detail/' + item.product.productstranslates_set.get().slug
                        else:
                            product_name = item.product.title
                            product_url = '/tr/products/detail/' + item.product.slug
                else:

                    if language == "tr":
                        product_name = item.excel_product.title_tr
                        product_url = '/' + language + '/products/detail/' + item.excel_product.slug_tr
                    else:
                        if item.excel_product.title_en is not None:
                            product_name = item.excel_product.title_en
                            product_url = '/' + language + '/products/detail/' + item.excel_product.slug_en
                        else:
                            product_name = item.excel_product.title_tr
                            product_url = '/tr/products/detail/' + item.excel_product.slug_tr

                if item.product_price == 0 or item.product_price is None:
                    product_price = None
                else:
                    product_price = TaxOptions.convert_price_tl(item.product_price)

                item_context = {
                    'title': product_name,
                    'price': product_price,
                    'quantity': item.quantity,
                    'date': item.added_date,
                    'slug':product_url,
                }

                context.append(item_context)

            return context
        else:
            return None


class Orders(models.Model):
    id = models.BigAutoField(primary_key=True)
    baskets = models.ForeignKey('basket.Basket', verbose_name="Sepet", on_delete=models.DO_NOTHING, null=True,
                                blank=True)
    total_price = models.DecimalField(default=0.00, verbose_name='Toplam Ödenen', decimal_places=2, max_digits=12)
    payment_types = models.ForeignKey('payments.PaymentTypes', verbose_name="Ödeme Şekli", on_delete=models.DO_NOTHING,
                                      null=True, blank=True)
    payment_date = models.DateTimeField(auto_now_add=True, verbose_name="Ödeme Tarihi")

    class Meta:
        managed = True
        db_table = "orders"
        verbose_name = "Sipariş"
        verbose_name_plural = "Siparişler"

    def __str__(self):
        return str(self.id)

    @classmethod
    def create_orders_by_customer(cls, selected_basket, total_price, payment_type):
        new_order = cls(baskets=selected_basket, total_price=total_price, payment_types=payment_type)
        new_order.save()
        return new_order

    @classmethod
    def get_basket_price(cls, selected_basket):
        if cls.objects.filter(baskets=selected_basket).exists():
            return cls.objects.get(baskets=selected_basket)
        else:
            return None


class CurrentPayments(models.Model):
    id = models.BigAutoField(primary_key=True)
    customer = models.ForeignKey('customers.Customers', on_delete=models.DO_NOTHING, verbose_name="Müşteri")
    total_price = models.DecimalField(default=0.00, verbose_name='Ödeme Miktarı', decimal_places=2, max_digits=12)
    currency_type = models.CharField(max_length=12,verbose_name="Para Birimi",default='₺',blank=True,null=True)
    installment = models.IntegerField(default=0, blank=True, null=True, verbose_name="Taksit Sayısı")
    customer_notes = models.TextField(blank=True, null=True, verbose_name="Müşteri Notu")
    payment_date = models.DateTimeField(auto_now_add=True, verbose_name="Ödeme Tarihi")

    class Meta:
        managed = True
        db_table = "current_payments"
        verbose_name = "Cari Ödeme"
        verbose_name_plural = "Cari Ödemeler"

    def __str__(self):
        return str(self.total_price)

    @classmethod
    def create_current_payment(cls,customer_obj,total_price,currency_type):
        new_payment = cls(customer=customer_obj,total_price=total_price,currency_type=currency_type)
        new_payment.save()
        return new_payment

    @classmethod
    def get_current_payments(cls,customer_obj):
        context = []
        if cls.objects.filter(customer=customer_obj).exists():

            current_list = cls.objects.filter(customer=customer_obj).all().order_by("-payment_date")

            for item in current_list:
                item_current = {
                    'payment_date':item.payment_date,
                    'total':item.total_price,
                    'currency_type':item.currency_type,
                    'installment':item.installment,
                }
                context.append(item_current)


        return context

    @classmethod
    def current_payements_count(cls,customer_obj):
        return cls.objects.filter(customer=customer_obj).count()

