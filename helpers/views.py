from django.shortcuts import render
from django.conf import settings
from django.core.mail import EmailMultiAlternatives, BadHeaderError
from django.contrib.auth.hashers import make_password
from django.core.files.images import get_image_dimensions
from django.contrib.auth.models import User
import requests
from bs4 import BeautifulSoup
import decimal

def get_currency(currency_type):
    url = "https://www.x-rates.com/calculator/?from={}&to=TRY&amount=1".format("USD")
    page = requests.get(url)
    # page = requests.get('https://www.x-rates.com/calculator/?from=USD&to=TRY&amount=1')
    # page = requests.get('https://www.x-rates.com/calculator/?from=EUR&to=TRY&amount=1')
    soup = BeautifulSoup(page.text, 'html.parser')

    part1 = soup.find(class_="ccOutputTrail").previous_sibling
    part2 = soup.find(class_="ccOutputTrail").get_text(strip=True)
    rate = "{}{}".format(part1, part2)

    return rate


def create_random_password():
    random_pass = User.objects.make_random_password(length=10,
                                                    allowed_chars='abcdefghjkmnpqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ23456789')

    return random_pass


def change_searchtext(value):
    new_value = ""
    for item in str(value).strip():
        if str(item) == "i":
            new_value += "İ"
        elif str(item) == "ı":
            new_value += "I"
        else:
            new_value += str(item).upper()

    return new_value


def custom_slugify(value):
    import unicodedata
    import re

    value = value.replace(u'\u0131', 'i')
    value = unicodedata.normalize('NFKD', value).encode('ascii', 'ignore').decode('ascii')
    value = re.sub(r'[^\w\s-]', '', value).strip().lower()

    return re.sub(r'[-\s]+', '-', value)


def cover_width(cover_obj):
    try:
        if cover_obj is not None:
            return get_image_dimensions(cover_obj)[0]
        else:
            return None
    except:
        return None


def cover_url_forpages(cover_obj):
    try:
        if cover_obj.url is not None:
            return cover_obj.url
        else:
            return ""
    except:
        return ""


def cover_url(cover_obj):
    try:
        if cover_obj.url is not None:
            return cover_obj.url
        else:
            return "/static/images/empty.png"
    except:
        return "/static/images/empty.png"


def seperate_subnumber(sub_number):
    if sub_number != None:
        seperated = str(sub_number).split(',')

        context_number = []
        for item in seperated:
            item_number = {
                'suboem': item
            }
            context_number.append(item_number)

        return context_number
    else:
        return ""


def sendmail_accounting(host_address, payment_type, paid_date,customer_name,customer_phone,customer_invoice_address,customer_cargo_address,customer_emailaddress,payment_cost,currency_type):
    from pages.models import CompanyInfos
    from datetime import datetime
    from django.template import defaultfilters

    #soketraktor admin mail
    emailaddressList = [CompanyInfos.get_company_email("accounting")]

    style_content = '<style type="text/css">* {-ms-text-size-adjust:100%;-webkit-text-size-adjust:none;-webkit-text-resize:100%;text-resize:100%;}a{outline:none;color:#40aceb;text-decoration:underline;}a:hover{text-decoration:none !important;}.nav a:hover{text-decoration:underline !important;}.title a:hover{text-decoration:underline !important;}.title-2 a:hover{text-decoration:underline !important;}.btn:hover{opacity:0.8;}.btn a:hover{text-decoration:none !important;}.btn{-webkit-transition:all 0.3s ease;-moz-transition:all 0.3s ease;-ms-transition:all 0.3s ease;transition:all 0.3s ease;}table td {border-collapse: collapse !important;}.ExternalClass, .ExternalClass a, .ExternalClass span, .ExternalClass b, .ExternalClass br, .ExternalClass p, .ExternalClass div{line-height:inherit;}@media only screen and (max-width:500px) {table[class="flexible"]{width:100% !important;}table[class="center"]{float:none !important;margin:0 auto !important;}*[class="hide"]{display:none !important;width:0 !important;height:0 !important;padding:0 !important;font-size:0 !important;line-height:0 !important;}td[class="img-flex"] img{width:100% !important;height:auto !important;}td[class="aligncenter"]{text-align:center !important;}th[class="flex"]{display:block !important;width:100% !important;}td[class="wrapper"]{padding:0 !important;}td[class="holder"]{padding:30px 15px 20px !important;}td[class="nav"]{padding:20px 0 0 !important;text-align:center !important;}td[class="h-auto"]{height:auto !important;}td[class="description"]{padding:30px 20px !important;}td[class="i-120"] img{width:120px !important;height:auto !important;}td[class="footer"]{padding:5px 20px 20px !important;}td[class="footer"] td[class="aligncenter"]{line-height:25px !important;padding:20px 0 0 !important;}tr[class="table-holder"]{display:table !important;width:100% !important;}th[class="thead"]{display:table-header-group !important; width:100% !important;}th[class="tfoot"]{display:table-footer-group !important; width:100% !important;}}</style>'
    address_hyperlink = host_address + "static/images/mail_cover.jpg"


    newsletter_title = "Ödeme Mesajı"

    module1message = "Merhabalar,<br/> Söke Traktör sitesinden online bir ödeme yapıldı.<br/>"

    module1message += "<b>Müşteri Adı:</b><br/> " + customer_name
    if customer_phone is not None:
        module1message += "<br/><b>Müşteri Telefonu:</b><br/> " + customer_phone
    else:
        module1message += "<br/><b>Müşteri Telefonu:</b><br/> " + "Telefon numarası girilmemiş. Teyit etmeniz gerekebilir."

    if customer_invoice_address is not None:
        module1message += "<br/><b>Fatura Adresi:</b><br/> " + customer_invoice_address
    else:
        module1message += "<br/><b>Fatura Adresi:</b><br/> " + "Fatura adresi bulunmuyor. Teyit etmeniz gerekebilir."

    if customer_cargo_address is not None:
        module1message += "<br/><b>Kargo Adresi:</b><br/> " + customer_cargo_address
    else:
        module1message += "<br/><b>Kargo Adresi:</b><br/> " + "Kargo adresi bulunmuyor. Teyit etmeniz gerekebilir."

    module1message += "<br/><b>E-posta:</b><br/> " + customer_emailaddress

    module1message += "<br/><b>Ödeme Türü:</b><br/> " + payment_type

    module1message += "<br/><b>Ödeme Tarihi:</b><br/> " + defaultfilters.date(paid_date,'SHORT_DATETIME_FORMAT')

    module1message += "<br/><b>Tutar:</b><br/> " + payment_cost + " " + currency_type

    message_title = "Soketraktor: Yeni Sipariş"
    subject = "Yeni Sipariş Aldınız."

    # message_title = "Şifre Yenileme Talebi"
    table_module1 = '<table data-module="module-2" data-thumb="' + address_hyperlink + '" width="100%" cellpadding="0" cellspacing="0"><tr><td data-bgcolor="bg-module" bgcolor="#eaeced"><table class="flexible" width="600" align="center" style="margin:0 auto;" cellpadding="0" cellspacing="0"><tr><td class="img-flex"><img src="' + address_hyperlink + '" style="vertical-align:top;" width="600" height="auto" alt=""></td></tr><tr><td data-bgcolor="bg-block" class="holder" style="padding:58px 60px 52px;" bgcolor="#f9f9f9"><table width="100%" cellpadding="0" cellspacing="0"><tr><td data-color="title" data-size="size title" data-min="25" data-max="45" data-link-color="link title color" data-link-style="text-decoration:none; color:#292c34;" class="title" align="center" style="font:35px/38px Arial, Helvetica, sans-serif; color:#292c34; padding:0 0 24px;">' + newsletter_title + '</td></tr><tr><td data-color="text" data-size="size text" data-min="10" data-max="26" data-link-color="link text color" data-link-style="font-weight:bold; text-decoration:underline; color:#40aceb;" align="left" style="font:bold 16px/25px Arial, Helvetica, sans-serif; color:#888; padding:0 0 23px;">' + module1message + '</td></tr><tr><td data-bgcolor="bg-button" data-size="size button" data-min="10" data-max="16" class="btn" align="center" style="font:12px/14px Arial, Helvetica, sans-serif; color:#f8f9fb; text-transform:uppercase; mso-padding-alt:12px 10px 10px; border-radius:2px;" bgcolor="#7bb84f"></td></tr></table></td></tr></table></td></tr><tr><td height="28"></td></tr></table>'

    table_content = '<table style="min-width:320px;" width="100%" cellspacing="0" cellpadding="0" bgcolor="#eaeced"><tr><td class="hide"><table width="600" cellpadding="0" cellspacing="0" style="width:600px !important;"><tr><td style="min-width:600px; font-size:0; line-height:0;">&nbsp;</td></tr></table></td></tr><tr><td class="wrapper" style="padding:0 10px;"><table data-module="module-1" data-thumb="thumbnails/01.png" width="100%" cellpadding="0" cellspacing="0"><tr><td data-bgcolor="bg-module" bgcolor="#eaeced"><table class="flexible" width="600" align="center" style="margin:0 auto;" cellpadding="0" cellspacing="0"><tr><td style="padding:29px 0 30px;"><table width="100%" cellpadding="0" cellspacing="0"><tr><th class="flex" width="113" align="left" style="padding:0;"><table class="center" cellpadding="0" cellspacing="0"><tr><td style="line-height:0;"></td></tr></table></th><th lass="flex" align="left" style="padding:0;"><table width="100%" cellpadding="0" cellspacing="0"><tr><td data-color="text" data-size="size navigation" data-min="10" data-max="22" data-link-style="text-decoration:none; color:#888;" class="nav" align="right" style="font:bold 13px/15px Arial, Helvetica, sans-serif; color:#888;"></td></tr></table></th></tr></table></td></tr></table></td></tr></table>' + table_module1 + '</td></tr></table>'

    # subject = user_fullprofile.full_profile.first_name + " " + user_fullprofile.full_profile.last_name + ugettext(" sent a team request invitation.")
    # subject = "Şifre yenileme isteğinde bulundunuz."
    from_email = settings.EMAIL_HOST_USER
    to = emailaddressList

    html_content = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><title>' + message_title + '</title><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><meta name="viewport" content="width=device-width, initial-scale=1.0" />' + style_content + '<body style="margin:0; padding:0;" bgcolor="#eaeced">' + table_content + '</body></head></html>'  # send_mail(

    try:
        msg = EmailMultiAlternatives(subject=subject, body=html_content, from_email=from_email, to=to)
        msg.attach_alternative(html_content, "text/html")
        msg.send()
        return True

    except BadHeaderError:
        return False


def forgetten_password_mail(user_emailaddress, host_address, random_pass, language):
    emailaddressList = [user_emailaddress]

    style_content = '<style type="text/css">* {-ms-text-size-adjust:100%;-webkit-text-size-adjust:none;-webkit-text-resize:100%;text-resize:100%;}a{outline:none;color:#40aceb;text-decoration:underline;}a:hover{text-decoration:none !important;}.nav a:hover{text-decoration:underline !important;}.title a:hover{text-decoration:underline !important;}.title-2 a:hover{text-decoration:underline !important;}.btn:hover{opacity:0.8;}.btn a:hover{text-decoration:none !important;}.btn{-webkit-transition:all 0.3s ease;-moz-transition:all 0.3s ease;-ms-transition:all 0.3s ease;transition:all 0.3s ease;}table td {border-collapse: collapse !important;}.ExternalClass, .ExternalClass a, .ExternalClass span, .ExternalClass b, .ExternalClass br, .ExternalClass p, .ExternalClass div{line-height:inherit;}@media only screen and (max-width:500px) {table[class="flexible"]{width:100% !important;}table[class="center"]{float:none !important;margin:0 auto !important;}*[class="hide"]{display:none !important;width:0 !important;height:0 !important;padding:0 !important;font-size:0 !important;line-height:0 !important;}td[class="img-flex"] img{width:100% !important;height:auto !important;}td[class="aligncenter"]{text-align:center !important;}th[class="flex"]{display:block !important;width:100% !important;}td[class="wrapper"]{padding:0 !important;}td[class="holder"]{padding:30px 15px 20px !important;}td[class="nav"]{padding:20px 0 0 !important;text-align:center !important;}td[class="h-auto"]{height:auto !important;}td[class="description"]{padding:30px 20px !important;}td[class="i-120"] img{width:120px !important;height:auto !important;}td[class="footer"]{padding:5px 20px 20px !important;}td[class="footer"] td[class="aligncenter"]{line-height:25px !important;padding:20px 0 0 !important;}tr[class="table-holder"]{display:table !important;width:100% !important;}th[class="thead"]{display:table-header-group !important; width:100% !important;}th[class="tfoot"]{display:table-footer-group !important; width:100% !important;}}</style>'
    address_hyperlink = host_address + "static/images/mail_cover.jpg"

    if language == "tr":
        message_button = "Şifremi Sıfırla"
        newsletter_title = "Şifre Yenileme Mesajı"
        module1message = "Merhabalar,<br/> Söke Traktör, size ait olan bir hesabın şifre değiştirme talebine yanıt göndermiş bulunmaktadır."
        message_title = "Şifre Yenileme Talebi"
        subject = "Şifre yenileme isteğinde bulundunuz."
    else:
        message_button = "Reset Password"
        newsletter_title = "Reset Password Message"
        module1message = "Merhabalar,<br/> Söke Traktör has sent a request for changing password. If this request is not yours, please ignore it."
        message_title = "Password Reset Request"
        subject = "You have requested a password reset."

    reset_hyperlink = '<a target="_blank" style="text-decoration:none; color:#f8f9fb; display:block; padding:12px 10px 10px;" href="' + host_address + language + '/customers/reset-password/?rp=' + random_pass + '&user=' + user_emailaddress + '">' + message_button + '</a>'

    # module1message = "Merhabalar,<br/> Çocuk Dostu platformu, size ait olan bir hesabın şifre değiştirme talebine yanıt göndermiş bulunmaktadır."

    # message_title = "Şifre Yenileme Talebi"
    table_module1 = '<table data-module="module-2" data-thumb="' + address_hyperlink + '" width="100%" cellpadding="0" cellspacing="0"><tr><td data-bgcolor="bg-module" bgcolor="#eaeced"><table class="flexible" width="600" align="center" style="margin:0 auto;" cellpadding="0" cellspacing="0"><tr><td class="img-flex"><img src="' + address_hyperlink + '" style="vertical-align:top;" width="600" height="auto" alt=""></td></tr><tr><td data-bgcolor="bg-block" class="holder" style="padding:58px 60px 52px;" bgcolor="#f9f9f9"><table width="100%" cellpadding="0" cellspacing="0"><tr><td data-color="title" data-size="size title" data-min="25" data-max="45" data-link-color="link title color" data-link-style="text-decoration:none; color:#292c34;" class="title" align="center" style="font:35px/38px Arial, Helvetica, sans-serif; color:#292c34; padding:0 0 24px;">' + newsletter_title + '</td></tr><tr><td data-color="text" data-size="size text" data-min="10" data-max="26" data-link-color="link text color" data-link-style="font-weight:bold; text-decoration:underline; color:#40aceb;" align="center" style="font:bold 16px/25px Arial, Helvetica, sans-serif; color:#888; padding:0 0 23px;">' + module1message + '</td></tr><tr><td data-bgcolor="bg-button" data-size="size button" data-min="10" data-max="16" class="btn" align="center" style="font:12px/14px Arial, Helvetica, sans-serif; color:#f8f9fb; text-transform:uppercase; mso-padding-alt:12px 10px 10px; border-radius:2px;" bgcolor="#7bb84f">' + reset_hyperlink + '</td></tr></table></td></tr></table></td></tr><tr><td height="28"></td></tr></table>'

    table_content = '<table style="min-width:320px;" width="100%" cellspacing="0" cellpadding="0" bgcolor="#eaeced"><tr><td class="hide"><table width="600" cellpadding="0" cellspacing="0" style="width:600px !important;"><tr><td style="min-width:600px; font-size:0; line-height:0;">&nbsp;</td></tr></table></td></tr><tr><td class="wrapper" style="padding:0 10px;"><table data-module="module-1" data-thumb="thumbnails/01.png" width="100%" cellpadding="0" cellspacing="0"><tr><td data-bgcolor="bg-module" bgcolor="#eaeced"><table class="flexible" width="600" align="center" style="margin:0 auto;" cellpadding="0" cellspacing="0"><tr><td style="padding:29px 0 30px;"><table width="100%" cellpadding="0" cellspacing="0"><tr><th class="flex" width="113" align="left" style="padding:0;"><table class="center" cellpadding="0" cellspacing="0"><tr><td style="line-height:0;"></td></tr></table></th><th lass="flex" align="left" style="padding:0;"><table width="100%" cellpadding="0" cellspacing="0"><tr><td data-color="text" data-size="size navigation" data-min="10" data-max="22" data-link-style="text-decoration:none; color:#888;" class="nav" align="right" style="font:bold 13px/15px Arial, Helvetica, sans-serif; color:#888;"></td></tr></table></th></tr></table></td></tr></table></td></tr></table>' + table_module1 + '</td></tr></table>'

    # subject = user_fullprofile.full_profile.first_name + " " + user_fullprofile.full_profile.last_name + ugettext(" sent a team request invitation.")
    # subject = "Şifre yenileme isteğinde bulundunuz."
    from_email = settings.EMAIL_HOST_USER
    to = emailaddressList

    html_content = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><title>' + message_title + '</title><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><meta name="viewport" content="width=device-width, initial-scale=1.0" />' + style_content + '<body style="margin:0; padding:0;" bgcolor="#eaeced">' + table_content + '</body></head></html>'  # send_mail(

    try:
        msg = EmailMultiAlternatives(subject=subject, body=html_content, from_email=from_email, to=to)
        msg.attach_alternative(html_content, "text/html")
        msg.send()
        return True

    except BadHeaderError:
        return False


def get_product_price(product_obj):
    try:
        if product_obj.productsprices_set.exists():
            # return product_obj.productsprices_set.get().price_dl
            return reformat_price(product_obj.productsprices_set.get().price_dl)
        else:
            return None
    except:
        if product_obj.price_dl is not None and product_obj.price_dl != float("0.0"):
            return reformat_price(product_obj.price_dl)
        else:
            return None


def reformat_price(price_value):
    if str(price_value).find(',') > -1:
        return str(price_value).replace(',', '.')
    else:
        return str(price_value)


def basket_count(basket):
    total_basket = 0
    for item in basket:
        total_basket += int(item.get("quantity"))

    return total_basket


def total_basket_price(basket, language):
    total_price = 0.00
    for item in basket:
        # and type(item.get("price")) == float and type(item.get("price")) != float("0.0")
        if item.get("price") != None:
            total_price += float(item.get("price")) * int(item.get("quantity"))

    return total_price


def revised_price(price, language):
    if price is None:
        if language == "tr":
            return "İletişime Geçiniz"
        else:
            return "Req. Information"


def change_basket_for_language(request, language):
    from products.models import Products, ExcelProdcts
    from payments.models import TaxOptions
    context_basket = []

    if "basket" in request.session:
        context = request.session["basket"]
        for item in context:
            if item.get("product_type") == "regular-product":
                product = Products.objects.get_regular_product(item.get("product_id"))

                if product.productsprices_set.exists():
                    product_price = TaxOptions.get_kdv_price(product.productsprices_set.get().price_dl, language)
                    total_price = product_price * int(item.get("quantity"))
                else:
                    product_price = None
                    total_price = None
            else:

                product = ExcelProdcts.get_outlist_product(item.get("product_id"))
                if product.price_dl is not None:
                    product_price = TaxOptions.get_kdv_price(product.price_dl, language)
                    total_price = product_price * int(item.get("quantity"))
                else:
                    product_price = None
                    total_price = None

            item_basket = {
                'price': product_price,
                'total': total_price,
                'quantity': item.get("quantity"),
                'product_id': item.get("product_id"),
                'product_type': item.get("product_type"),
            }

            context_basket.append(item_basket)

        del request.session["basket"]

    return context_basket


def update_basket_in_dashboard(request, product_id, product_type, language, quantity, operation):
    from products.models import Products, ExcelProdcts
    from payments.models import TaxOptions

    if operation == "remove":
        context_basket = []

        for item in request.session["basket"]:
            if (item.get("product_id") == product_id):
                del item
                continue
            else:

                item_basket = {
                    'price': item.get("price"),
                    'total': item.get("total"),
                    'quantity': item.get("quantity"),
                    'product_id': item.get("product_id"),
                    'product_type': item.get("product_type"),
                }

                context_basket.append(item_basket)

        del request.session["basket"]

        return context_basket

    else:
        context_basket = []

        for item in request.session["basket"]:
            if (item.get("product_id") == product_id):
                if product_type == "regular-product":
                    product = Products.objects.get_regular_product(item.get("product_id"))
                    if product.productsprices_set.exists():
                        product_price = TaxOptions.get_kdv_price(product.productsprices_set.get().price_dl, language)
                        total_price = round((product_price * int(quantity)), 2)
                    else:
                        product_price = None
                        total_price = None

                else:
                    # excel products
                    product = ExcelProdcts.get_outlist_product(item.get("product_id"))
                    if product.price_dl is not None:
                        product_price = TaxOptions.get_kdv_price(product.price_dl, language)
                        total_price = round((product_price * int(quantity)), 2)
                    else:
                        product_price = None
                        total_price = None

                item_basket = {
                    'price': product_price,
                    'total': total_price,
                    'quantity': quantity,
                    'product_id': item.get("product_id"),
                    'product_type': item.get("product_type"),
                }

            else:
                # değiştirilen ürünler dışındaki ürünleri tekrardan al
                if item.get("product_type") == "regular-product":
                    product = Products.objects.get_regular_product(item.get("product_id"))
                    if product.productsprices_set.exists():
                        product_price = TaxOptions.get_kdv_price(product.productsprices_set.get().price_dl, language)
                        total_price = round((product_price * int(quantity)), 2)
                    else:
                        product_price = None
                        total_price = None

                else:
                    # excel products
                    product = ExcelProdcts.get_outlist_product(item.get("product_id"))
                    if product.price_dl is not None:
                        product_price = TaxOptions.get_kdv_price(product.price_dl, language)
                        total_price = round((product_price * int(quantity)), 2)
                    else:
                        product_price = None
                        total_price = None

                item_basket = {
                    'price': product_price,
                    'total': total_price,
                    'quantity': item.get("quantity"),
                    'product_id': item.get("product_id"),
                    'product_type': item.get("product_type"),
                }

            context_basket.append(item_basket)

        del request.session["basket"]

        return context_basket


def unzip_basket_from_session(session_basket, language):
    from products.models import Products, ExcelProdcts
    context_basket = []
    # print(session_basket)
    if language == "tr":
        for item in session_basket:
            if item.get("product_type") == "regular-product":
                selected_product = Products.objects.get_regular_product(item.get("product_id"))

                item_basket = {
                    'title': selected_product.title,
                    'price': item.get("price"),
                    'total': item.get("total"),
                    'id': item.get("product_id"),
                    'quantity': item.get("quantity"),
                    'oem_number': selected_product.productsoemnumbers_set.get().oem_number,
                    'sub_number': seperate_subnumber(selected_product.productsoemnumbers_set.get().sub_number),
                    'product_type': 'regular-product',
                }

            else:
                selected_product = ExcelProdcts.get_outlist_product(item.get("product_id"))
                if item.get("price") is not None or item.get("price") != "0.0":
                    total_price = item.get("price") * int(item.get("quantity"))
                else:
                    total_price = None

                item_basket = {
                    'title': selected_product.title_tr,
                    'price': item.get("price"),
                    'total': item.get("total"),
                    'quantity': item.get("quantity"),
                    'id': item.get("product_id"),
                    'oem_number': selected_product.oem_number,
                    'sub_number': "",
                    'product_type': 'outlist-product',
                }

            context_basket.append(item_basket)


    else:
        for item in session_basket:
            if item.get("product_type") == "regular-product":
                selected_product = Products.objects.get_regular_product(item.get("product_id"))
                if selected_product.productstranslates_set.exists():
                    product_name = selected_product.productstranslates_set.get().title
                else:
                    # en adı yoksa tr olacak
                    product_name = selected_product.title

                if item.get("price") is not None:
                    total_price = item.get("price") * int(item.get("quantity"))
                else:
                    total_price = None

                item_basket = {
                    'title': product_name,
                    'price': item.get("price"),
                    'total': item.get("total"),
                    'id': item.get("product_id"),
                    'quantity': item.get("quantity"),
                    'oem_number': selected_product.productsoemnumbers_set.get().oem_number,
                    'sub_number': seperate_subnumber(selected_product.productsoemnumbers_set.get().sub_number),
                    'product_type': 'regular-product',
                }

            else:
                selected_product = ExcelProdcts.get_outlist_product(item.get("product_id"))
                if selected_product.title_en is not None:
                    product_name = selected_product.title_en
                else:
                    product_name = selected_product.title_tr

                if item.get("price") is not None or item.get("price") != "0.0":
                    total_price = item.get("price") * int(item.get("quantity"))
                else:
                    total_price = None

                item_basket = {
                    'title': product_name,
                    'price': item.get("price"),
                    'quantity': item.get("quantity"),
                    'id': item.get("product_id"),
                    'total': item.get("total"),
                    'oem_number': selected_product.oem_number,
                    'sub_number': "",
                    'product_type': 'outlist-product',
                }

            context_basket.append(item_basket)

    return context_basket


def test_credit_cards(card_number):
    context_cards = ['4000056655665556', '5200828282828210', '371449635398431', '6011000990139424', '30569309025904',
                     '3566002020360505', '6200000000000005', '6759649826438453', ]
    for item in context_cards:
        if card_number == item:
            return True

    return False


def complete_creditcard_shopping(request, language, selected_customer, is_basket_paid):
    from products.models import Products, ExcelProdcts
    from payments.models import PaymentTypes
    from basket.models import Basket, BasketItems, Orders
    context = []

    # bu fonksiyonun sonunda eğer başarılı ise basket session silinecek
    # basket tablosunu sadece geçmiş ve ödenmemiş basket'larda göstereceğiz
    # ayrıca geçmiş siparişlerde göstereceğiz.

    created_basket = Basket.create_basket(selected_customer, is_basket_paid)
    for item in request.session["basket"]:
        if item.get("product_type") == "regular-product":
            # normal produc
            selected_product = Products.objects.get_regular_product(item.get("product_id"))
        else:
            # excel products
            selected_product = ExcelProdcts.get_outlist_product(item.get("product_id"))

        if selected_product is not None:
            created_basket_items = BasketItems.create_basket_items(created_basket, selected_product,item.get("product_type"), item.get("quantity"),item.get("price"))

    created_order = Orders.create_orders_by_customer(created_basket, total_basket_price(request.session["basket"], language),PaymentTypes.get_payment_type("credit-card"))

    if is_basket_paid and created_order is not None:
        if language == "tr":
            message = "Kredi kartı ödemesi başarılı oldu."
        else:
            message = "Credit card payment was successful."

        situation = True
        del request.session["basket"]
    else:
        if language == "tr":
            message = "Kredi kartı ödemesi başarısız oldu."
        else:
            message = "Credit card payment failed."

        situation = False


    item_basket = {
        'message': message,
        'situation': situation,
    }

    context.append(item_basket)

    return context


def calculate_basket(session_basket, language):
    from products.models import Products, ExcelProdcts
    context_basket = []

    if language == "tr":
        for item in session_basket:
            context_models = []
            if item.get("product_type") == "regular-product":

                selected_product = Products.objects.get_regular_product(item.get("product_id"))

                if selected_product.productsbrandmodels_set.exists():
                    for prd_models in selected_product.productsbrandmodels_set.all():
                        item_models = {
                            'title': prd_models.brandmodels.title_tr
                        }
                        context_models.append(item_models)

                if item.get("price") is not None:
                    total_price = item.get("price") * int(item.get("quantity"))
                else:
                    total_price = None

                item_basket = {
                    'title': selected_product.title,
                    'price': item.get("price"),
                    'total': total_price,
                    'id': item.get("product_id"),
                    'quantity': item.get("quantity"),
                    'cover': cover_url(selected_product.product_cover),
                    'oem_number': selected_product.productsoemnumbers_set.get().oem_number,
                    'sub_number': seperate_subnumber(selected_product.productsoemnumbers_set.get().sub_number),
                    'is_stock': selected_product.is_stock,
                    'is_active': selected_product.is_active,
                    'slug': "/" + language + "/products/detail/" + selected_product.slug,
                    'prd_models': context_models,
                    'product_type': 'regular-product',
                    'category': {'name': selected_product.categories.title,
                                 'slug': '/' + language + '/products/' + selected_product.categories.slug}
                }

            else:
                # context_models = []
                selected_product = ExcelProdcts.get_outlist_product(item.get("product_id"))

                if item.get("price") is not None or item.get("price") != "0.0":
                    total_price = item.get("price") * int(item.get("quantity"))
                else:
                    total_price = None

                item_basket = {
                    'title': selected_product.title_tr,
                    'price': item.get("price"),
                    'total': total_price,
                    'quantity': item.get("quantity"),
                    'id': item.get("product_id"),
                    'cover': "/static/images/empty.png",
                    'oem_number': selected_product.oem_number,
                    'sub_number': "",
                    'is_stock': selected_product.is_stock,
                    'is_active': selected_product.is_active,
                    'slug': "/" + language + "/products/detail/" + selected_product.slug_tr,
                    'prd_models': context_models,
                    'product_type': 'outlist-product',
                }

            context_basket.append(item_basket)

    else:
        # english
        context_basket = []

        for item in session_basket:
            context_models = []
            if item.get("product_type") == "regular-product":
                selected_product = Products.objects.get_regular_product(item.get("product_id"))

                if selected_product.productstranslates_set.exists():
                    product_name = selected_product.productstranslates_set.get().title
                else:
                    # en adı yoksa tr olacak
                    product_name = selected_product.title

                if selected_product.categories.categoriestranslates_set.exists():
                    category = {
                        'name': selected_product.categories.categoriestranslates_set.get().title,
                        'slug': '/' + language + '/products/' + selected_product.categories.categoriestranslates_set.get().slug}
                else:
                    category = {
                        'name': selected_product.categories.title,
                        'slug': '/' + language + '/products/' + selected_product.categories.slug,
                    }

                # if selected_product.product_cover is not None:
                #     product_cover = selected_product.product_cover.url
                # else:
                #     product_cover = "/static/images/empty.png"

                if selected_product.productsbrandmodels_set.exists():
                    for prd_models in selected_product.productsbrandmodels_set.all():
                        item_models = {
                            'title': prd_models.brandmodels.title_en
                        }
                        context_models.append(item_models)

                if item.get("price") is not None:
                    total_price = item.get("price") * int(item.get("quantity"))
                else:
                    total_price = None

                item_basket = {
                    'title': product_name,
                    'price': item.get("price"),
                    'total': total_price,
                    'id': item.get("product_id"),
                    'quantity': item.get("quantity"),
                    'cover': cover_url(selected_product.product_cover),
                    'oem_number': selected_product.productsoemnumbers_set.get().oem_number,
                    'sub_number': seperate_subnumber(selected_product.productsoemnumbers_set.get().sub_number),
                    'is_stock': selected_product.is_stock,
                    'is_active': selected_product.is_active,
                    'slug': "/" + language + "/products/detail/" + selected_product.slug,
                    'prd_models': context_models,
                    'product_type': 'regular-product',
                    'category': category,
                }

            else:
                selected_product = ExcelProdcts.get_outlist_product(item.get("product_id"))
                if selected_product.title_en is not None:
                    product_name = selected_product.title_en
                else:
                    product_name = selected_product.title_tr

                if selected_product.slug_en is not None:
                    prd_slug = selected_product.slug_en
                else:
                    prd_slug = selected_product.slug_tr

                if item.get("price") is not None or item.get("price") != "0.0":
                    total_price = item.get("price") * int(item.get("quantity"))
                else:
                    total_price = None

                item_basket = {
                    'title': product_name,
                    'price': item.get("price"),
                    'quantity': item.get("quantity"),
                    'id': item.get("product_id"),
                    'total': total_price,
                    'cover': "/static/images/empty.png",
                    'oem_number': selected_product.oem_number,
                    'sub_number': "",
                    'is_stock': selected_product.is_stock,
                    'is_active': selected_product.is_active,
                    'slug': "/" + language + "/products/detail/" + prd_slug,
                    'prd_models': context_models,
                    'product_type': 'outlist-product',
                }

            context_basket.append(item_basket)

    return context_basket


def notifications_count(customer_obj):
    if phone_notifications(customer_obj.id):
        return len(address_notifications(customer_obj)) + 1
    else:
        return len(address_notifications(customer_obj))

def address_notifications(customer_obj):
    from customers.models import Customers, CustomersAddress
    if CustomersAddress.isthere_address(customer_obj, "vat") and CustomersAddress.isthere_address(customer_obj,
                                                                                                  "cargo"):
        customer_address = CustomersAddress.get_customer_address(customer_obj)
    else:
        customer_address = []

    return customer_address


def phone_notifications(customer_id):
    from customers.models import Customers
    if Customers.get_customer_by_id(customer_id).phone_number is not None:
        return True
    else:
        return False


def notification_messages(message_type, language):
    if language == "tr":
        if message_type == "address":
            message = "Sistemde fatura ve kargo adresleriniz kayıtlı görünmüyor. Öncelikle bunları kayıt etmelisiniz. Daha sonra tekrardan ödeme işlemine devam edebilirsiniz."
        elif message_type == "phone":
            message = "Sizinle daha sağlıklı iletişim kurabilmemiz için lütfen telefon numaranızı giriniz."
        else:
            message = ""

    else:
        if message_type == "address":
            message = "Your billing and shipping address not seen in the system. First, you've to add your billing and shipping address. After that you can continue the payment process again."
        elif message_type == "phone":
            message = "Please enter your phone number. So, we can communicate with you much better."
        else:
            message = ""

    return message

def revise_phone_info(str_value):
    str_value = str(str_value).replace('(','')
    str_value = str(str_value).replace(')', '')
    str_value = str(str_value).replace(' ', '')
    return str_value


def sendmail_company(host_address, name,email,phone,message):
    from pages.models import CompanyInfos
    from datetime import datetime
    from django.template import defaultfilters

    #soketraktor admin mail
    emailaddressList = [CompanyInfos.get_company_email("email")]

    style_content = '<style type="text/css">* {-ms-text-size-adjust:100%;-webkit-text-size-adjust:none;-webkit-text-resize:100%;text-resize:100%;}a{outline:none;color:#40aceb;text-decoration:underline;}a:hover{text-decoration:none !important;}.nav a:hover{text-decoration:underline !important;}.title a:hover{text-decoration:underline !important;}.title-2 a:hover{text-decoration:underline !important;}.btn:hover{opacity:0.8;}.btn a:hover{text-decoration:none !important;}.btn{-webkit-transition:all 0.3s ease;-moz-transition:all 0.3s ease;-ms-transition:all 0.3s ease;transition:all 0.3s ease;}table td {border-collapse: collapse !important;}.ExternalClass, .ExternalClass a, .ExternalClass span, .ExternalClass b, .ExternalClass br, .ExternalClass p, .ExternalClass div{line-height:inherit;}@media only screen and (max-width:500px) {table[class="flexible"]{width:100% !important;}table[class="center"]{float:none !important;margin:0 auto !important;}*[class="hide"]{display:none !important;width:0 !important;height:0 !important;padding:0 !important;font-size:0 !important;line-height:0 !important;}td[class="img-flex"] img{width:100% !important;height:auto !important;}td[class="aligncenter"]{text-align:center !important;}th[class="flex"]{display:block !important;width:100% !important;}td[class="wrapper"]{padding:0 !important;}td[class="holder"]{padding:30px 15px 20px !important;}td[class="nav"]{padding:20px 0 0 !important;text-align:center !important;}td[class="h-auto"]{height:auto !important;}td[class="description"]{padding:30px 20px !important;}td[class="i-120"] img{width:120px !important;height:auto !important;}td[class="footer"]{padding:5px 20px 20px !important;}td[class="footer"] td[class="aligncenter"]{line-height:25px !important;padding:20px 0 0 !important;}tr[class="table-holder"]{display:table !important;width:100% !important;}th[class="thead"]{display:table-header-group !important; width:100% !important;}th[class="tfoot"]{display:table-footer-group !important; width:100% !important;}}</style>'
    address_hyperlink = host_address + "static/images/mail_cover.jpg"


    newsletter_title = "Soketraktor: Yeni Mesaj"

    module1message = "Merhabalar,<br/> Söke Traktör sitesinden bir mesaj aldınız.<br/><hr/><br/>"

    module1message += "<b>İsim - Soyisim:</b><br/> " + name
    module1message += "<br/><br/><b>Telefon:</b><br/> " + phone
    module1message += "<br/><br/><b>Eposta Adresi:</b><br/> " + email
    module1message += "<br/><br/><b>Mesaj İçeriği:</b><br/> " + message
    module1message += "<br/><br/><b>Mesaj Tarihi:</b><br/> " + str(datetime.now().date())

    message_title = "Soketraktor: Yeni Mesaj"
    subject = "Soketraktor: Websitesinden Yeni Mesaj."

    table_module1 = '<table data-module="module-2" data-thumb="' + address_hyperlink + '" width="100%" cellpadding="0" cellspacing="0"><tr><td data-bgcolor="bg-module" bgcolor="#eaeced"><table class="flexible" width="600" align="center" style="margin:0 auto;" cellpadding="0" cellspacing="0"><tr><td class="img-flex"><img src="' + address_hyperlink + '" style="vertical-align:top;" width="600" height="auto" alt=""></td></tr><tr><td data-bgcolor="bg-block" class="holder" style="padding:58px 60px 52px;" bgcolor="#f9f9f9"><table width="100%" cellpadding="0" cellspacing="0"><tr><td data-color="title" data-size="size title" data-min="25" data-max="45" data-link-color="link title color" data-link-style="text-decoration:none; color:#292c34;" class="title" align="center" style="font:35px/38px Arial, Helvetica, sans-serif; color:#292c34; padding:0 0 24px;">' + newsletter_title + '</td></tr><tr><td data-color="text" data-size="size text" data-min="10" data-max="26" data-link-color="link text color" data-link-style="font-weight:bold; text-decoration:underline; color:#40aceb;" align="left" style="font:bold 16px/25px Arial, Helvetica, sans-serif; color:#888; padding:0 0 23px;">' + module1message + '</td></tr><tr><td data-bgcolor="bg-button" data-size="size button" data-min="10" data-max="16" class="btn" align="center" style="font:12px/14px Arial, Helvetica, sans-serif; color:#f8f9fb; text-transform:uppercase; mso-padding-alt:12px 10px 10px; border-radius:2px;" bgcolor="#7bb84f"></td></tr></table></td></tr></table></td></tr><tr><td height="28"></td></tr></table>'

    table_content = '<table style="min-width:320px;" width="100%" cellspacing="0" cellpadding="0" bgcolor="#eaeced"><tr><td class="hide"><table width="600" cellpadding="0" cellspacing="0" style="width:600px !important;"><tr><td style="min-width:600px; font-size:0; line-height:0;">&nbsp;</td></tr></table></td></tr><tr><td class="wrapper" style="padding:0 10px;"><table data-module="module-1" data-thumb="thumbnails/01.png" width="100%" cellpadding="0" cellspacing="0"><tr><td data-bgcolor="bg-module" bgcolor="#eaeced"><table class="flexible" width="600" align="center" style="margin:0 auto;" cellpadding="0" cellspacing="0"><tr><td style="padding:29px 0 30px;"><table width="100%" cellpadding="0" cellspacing="0"><tr><th class="flex" width="113" align="left" style="padding:0;"><table class="center" cellpadding="0" cellspacing="0"><tr><td style="line-height:0;"></td></tr></table></th><th lass="flex" align="left" style="padding:0;"><table width="100%" cellpadding="0" cellspacing="0"><tr><td data-color="text" data-size="size navigation" data-min="10" data-max="22" data-link-style="text-decoration:none; color:#888;" class="nav" align="right" style="font:bold 13px/15px Arial, Helvetica, sans-serif; color:#888;"></td></tr></table></th></tr></table></td></tr></table></td></tr></table>' + table_module1 + '</td></tr></table>'

    # subject = user_fullprofile.full_profile.first_name + " " + user_fullprofile.full_profile.last_name + ugettext(" sent a team request invitation.")
    # subject = "Şifre yenileme isteğinde bulundunuz."
    from_email = settings.EMAIL_HOST_USER
    to = emailaddressList

    html_content = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><title>' + message_title + '</title><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><meta name="viewport" content="width=device-width, initial-scale=1.0" />' + style_content + '<body style="margin:0; padding:0;" bgcolor="#eaeced">' + table_content + '</body></head></html>'  # send_mail(

    try:
        msg = EmailMultiAlternatives(subject=subject, body=html_content, from_email=from_email, to=to)
        msg.attach_alternative(html_content, "text/html")
        msg.send()
        return True

    except BadHeaderError:
        return False