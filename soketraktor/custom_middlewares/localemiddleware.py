from django.conf import settings
from django.utils import translation

class ForceLanguageMiddleware:

    def process_request(self,request):
        if request.path == '/':
            request.LANG = getattr(settings,'LANGUAGE_CODE',settings.LANGUAGE_CODE)
            translation.activate(request.LANG)
            request.LANGUAGE_CODE=request.LANG

# from django.utils import translation
# from django.shortcuts import redirect
# from django.conf import settings
#
# def set_language(request):
#     language = request.POST.get('language', settings.LANGUAGE_CODE)
#     translation.activate(language)
#     request.session[translation.LANGUAGE_SESSION_KEY] = language
#     return redirect('root')