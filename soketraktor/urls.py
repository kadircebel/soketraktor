"""soketraktor URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path,include
from django.conf import settings
from django.conf.urls.i18n import i18n_patterns
from django.conf.urls.static import static
from home.views import index,contact_form,export_products_excel
from products.views import product_list,products_index,products_search
from categories.views import categories_list,category_index
from customers.views import register,logout_view,login_view,accounts_view,update_password_view,reset_password_view,basket,payment_credit_card,payment_custom_payment
from basket.views import basket_index
from django.contrib.sitemaps.views import sitemap
from .sitemap import *
# from django.views.decorators.cache import cache_page
# from django.conf.urls import handler404


sitemaps = {
    'static': StaticViewSitemap,
    'pages': PagesList,
    'categories': CategoriesList,
    'products': ProducstList,
}



urlpatterns = [
    path('sitemap.xml', sitemap, {'sitemaps': sitemaps}, name='django.contrib.sitemaps.views.sitemap'),
    path('admin/', admin.site.urls),
    # path('frola_editor/',include('froala_editor.urls')),
    path('ckeditor/', include('ckeditor_uploader.urls')),
    path('more-products-index/',products_index.as_view(),name='more_products_index'),
    path('more-products/',product_list.as_view(),name='more_products'),
    path('more-categories/',categories_list.as_view(),name='more_categories'),
    path('search-products/',products_search.as_view(),name='search_products'),
    path('all-categories/',category_index.as_view(),name='all_categories'),
    path('register/',register.as_view(),name='register_customer'),
    path('logout/',logout_view.as_view(),name='logout_customer'),
    path('login/',login_view.as_view(),name='login_customer_ajax'),
    path('account-info/',accounts_view.as_view(),name='customer_account_info'),
    path('update-password/',update_password_view.as_view(),name='update_password_customer'),
    path('reset-password-account/',reset_password_view.as_view(),name='reset_password_account'),
    path('add-basket/',basket_index.as_view(),name='add_basket'),
    path('update-basket/',basket.as_view(),name='update_basket'),
    path('credit-card/',payment_credit_card.as_view(),name='payment_credit_card'),
    path('current-payment/',payment_custom_payment.as_view(),name='payment_custom_payment'),
    path('send-message/',contact_form.as_view(),name='contact_form'),
    path('export/',export_products_excel,name='export_products'),

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

urlpatterns += i18n_patterns(
    path('',index.as_view(),name="homepage"),
    path('logout/',logout_view.as_view(),name="logout_customer"),
    path('login/',login_view.as_view(),name="login_customer"),
    path('register/',register.as_view(),name="register_page"),
    path('products/',include('products.urls'),name="products"),
    path('categories/',include('categories.urls'),name="categories"),
    path('pages/',include('pages.urls'),name="pages"),
    path('customers/',include('customers.urls'),name="customers"),
    # path('reset-password-account/',reset_password_view.as_view(),name='reset_password_account'),
)


handler404 = 'home.views.handler404'
handler500 = 'home.views.handler500'
handler403 = 'home.views.handler403'
handler400 = 'home.views.handler400'



admin.autodiscover()
admin.site.enable_nav_sidebar = False

admin.sites.AdminSite.site_header = "Söke Traktör CMS"
admin.sites.AdminSite.site_title = "Söke Traktör"
