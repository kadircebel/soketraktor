from django.contrib.sitemaps import Sitemap
from categories.models import Categories
from categories_translates.models import CategoriesTranslates
from products.models import Products,ExcelProdcts
from products_translates.models import ProductsTranslates
from pages.models import Pages,PagesTranslates
from django.urls import reverse
from django.utils import translation

class CategoriesList(Sitemap):
    priority = 0.5
    # protocol = "https"
    protocol = "http"
    changefreq = "always"

    def items(self):
        language = translation.get_language()
        if language == "tr":
            categories_list = Categories.objects.filter(is_active=True).all().order_by("id")
        else:
            categories_list = CategoriesTranslates.objects.filter(is_active=True).order_by("id")
        url_list = []


        for item in categories_list:
            if language == "tr":
                url_list.append('/tr/categories/{}'.format(item.slug))
            else:
                url_list.append('/en/categories/{}'.format(item.slug))

        return url_list

    def location(self, item):
        return item

class ProducstList(Sitemap):
    priority = 0.5
    # protocol = "https"
    protocol = "http"
    changefreq = "always"

    def items(self):
        language = translation.get_language()
        if language == "tr":
            products_list = Products.objects.filter(is_active=True).order_by("id")
        else:
            products_list = ProductsTranslates.objects.filter(is_active=True).order_by("id")

        url_list = []

        for item in products_list:
            if language == "tr":
                url_list.append('/tr/products/detail/{}'.format(item.slug))
            else:
                url_list.append('/en/products/detail/{}'.format(item.slug))

        excel_products_list = ExcelProdcts.objects.filter(is_active=True).order_by("id")

        for item in excel_products_list:
            if language == "tr":
                url_list.append('/tr/products/detail/{}'.format(item.slug_tr))
            else:
                if item.slug_en is not None:
                    url_list.append('/en/products/detail/{}'.format(item.slug_en))
                else:
                    url_list.append('/tr/products/detail/{}'.format(item.slug_tr))

        return url_list

    def location(self, item):
        return item

class PagesList(Sitemap):
    priority = 0.5
    # protocol = "https"
    protocol = "http"
    changefreq = "always"

    def items(self):
        url_list = []
        language = translation.get_language()
        if language == "tr":
            pages_list = Pages.objects.filter(is_active=True).order_by("id")
        else:
            pages_list = PagesTranslates.objects.filter(is_active=True).all()


        for item in pages_list:
            if language == "tr":
                url_list.append('/tr/pages/{}'.format(item.slug))
            else:
                url_list.append('/en/pages/{}'.format(item.slug))

        return url_list

    def location(self, item):
        return item

class StaticViewSitemap(Sitemap):
    changefreq = "always"
    priority = 0.5
    # protocol = "https"
    protocol = "http"

    def items(self):
        return ["register_page","login_customer","logout_customer"]

    def location(self, item):
        return reverse(item)
