from django.db import models
from django.utils.text import slugify
from helpers.views import custom_slugify

class Languages(models.Model):
    id = models.BigAutoField(primary_key=True)
    title = models.CharField(max_length=150,verbose_name="Dil Adı")
    slug = models.SlugField(max_length=200,verbose_name="Url",blank=True,null=True)
    is_active = models.BooleanField(default=True)

    class Meta:
        managed = True
        db_table = "languages"
        verbose_name = "Site Dili"
        verbose_name_plural = "Site Dilleri"

    def __str__(self):
        return self.title

    def save(self,*args,**kwargs):
        super(Languages,self).save(*args,**kwargs)
