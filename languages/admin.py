from django.contrib import admin
from .models import *

class LanguagesAdmin(admin.ModelAdmin):
    list_display = ["title","slug","is_active"]

    list_editable = ["is_active"]

    class Meta:
        model = Languages

admin.site.register(Languages,LanguagesAdmin)
