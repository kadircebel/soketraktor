from django.shortcuts import render,HttpResponse
from django.views import View
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
import json
from django.utils import translation
from .models import Menus,PagesTranslates,Pages,CompanyInfos,MetaDescriptionTags
from helpers.views import basket_count,total_basket_price,change_basket_for_language,notifications_count
from customers.models import Customers
from basket.models import Basket

@method_decorator(csrf_exempt, name='dispatch')
class detail_pages(View):
    template_name = "pages/index.html"

    def get(self,request,slug):
        notification_count = 0
        menus = Menus.get_menus(translation.get_language(), "top")
        bottom_menu = Menus.get_menus(translation.get_language(), "bottom")
        selected_menu = Menus.get_menu(translation.get_language(),slug,"top")
        selected_page = Pages.objects.page_detail(selected_menu,translation.get_language())

        if slug == "iletisim" or slug == "contact":
            context_company = CompanyInfos.get_company_infos()
        else:
            context_company =[]

        selected_meta = MetaDescriptionTags.get_selected_meta(slug, translation.get_language())



        # basket operations
        if "basket" in request.session:
            request.session["basket"] = change_basket_for_language(request, translation.get_language())
            basket_totalcount = basket_count(request.session["basket"])
            basket_totalprice = total_basket_price(request.session["basket"], translation.get_language())
        else:
            basket_totalprice = None
            basket_totalcount = 0

        # basket operations

        if "customer_id" in request.session and request.user.is_authenticated:
            order_count = Basket.get_order_count(Customers.get_customer_by_id(request.session["customer_id"]))
            notification_count = notifications_count(Customers.get_customer_by_id(request.session["customer_id"]))
        else:
            order_count = 0

        if translation.get_language() == "tr":
            if selected_menu.slug_en is not None:
                change_lang = "/en/pages/" + selected_menu.slug_en
            else:
                change_lang = "javascript:void(0);"
        else:
            change_lang = "/tr/pages/" + selected_menu.slug_tr



        context={
            'top_menu': menus,
            'bottom_menu': bottom_menu,
            'slug':slug,
            'page':selected_page,
            'bread_menu':selected_menu,
            'basket_count': basket_totalcount,
            'basket_price': basket_totalprice,
            'order_count':order_count,
            'notification_count': notification_count,
            'footer_address': CompanyInfos.get_company_address(),
            'footer_email': CompanyInfos.get_company_email('email'),
            'meta_tags': selected_meta,
            'change_language': change_lang,
            'contact_infos':context_company,
        }

        return render(request,self.template_name,context)

    def post(self,request):
        pass

