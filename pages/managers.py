from django.db import models
from helpers.views import cover_url_forpages


class PagesManager(models.Manager):

    def page_detail(self, menu, languages):
        selected_page = self.get(menus=menu)
        if selected_page.menus.footer_position != False and selected_page.menus.header_position != False:
            child_pages = selected_page.menus.get_submenus_for_pages(languages, menu.slug_tr, "top")
        else:
            child_pages = None

        context_sub = []
        # bu kısım sonra bakılacak.
        # sayfanın parent id si var ise alt sayfadır. dolayısı ile diğer sub sayfalar için
        # burada gösterim yakalamalıyız.
        if child_pages != None:
            for item in selected_page.menus.get_submenus_for_pages(languages, menu.slug_tr, "top"):
                if languages == "tr":
                    item_sub = {
                        'title': item.title_tr,
                        'slug': 'pages/' + item.slug_tr,
                    }
                else:
                    item_sub = {
                        'title': item.title_en,
                        'slug': 'pages/' + item.slug_en,
                    }

                context_sub.append(item_sub)

        if languages == "tr":
            context = {
                'title': selected_page.title,
                'content_text': selected_page.content_text,
                'page_cover': cover_url_forpages(selected_page.page_cover),
                'order_number': selected_page.order_number,
                'is_active': selected_page.is_active,
                'sub_pages': context_sub,
                'menu_title': selected_page.menus.title_tr,
            }
        else:
            if selected_page.pages_translates.exists():
                context = {
                    'title': selected_page.pages_translates.get().title,
                    'content_text': selected_page.pages_translates.get().content_text,
                    'page_cover': cover_url_forpages(selected_page.page_cover),
                    'order_number': selected_page.order_number,
                    'is_active': selected_page.is_active,
                    'sub_pages': context_sub,
                    'menu_title': selected_page.menus.title_en,
                }
            else:
                context = {
                    'title': selected_page.title,
                    'content_text': selected_page.content_text,
                    'page_cover': cover_url_forpages(selected_page.page_cover),
                    'order_number': selected_page.order_number,
                    'is_active': selected_page.is_active,
                    'sub_pages': context_sub,
                    'menu_title': selected_page.menus.title_tr,
                }

        return context
