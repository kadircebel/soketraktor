from django.urls import path
from .views import *

app_name = "pages"

urlpatterns=[
    path('<slug:slug>/', detail_pages.as_view(), name='page_detail'),
]
