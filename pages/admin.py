from django.contrib import admin
from .models import *


class MetaDescriptionTagsInlineAdmin(admin.StackedInline):
    min_num = 0
    max_num = 1
    extra = 0
    model = MetaDescriptionTags
    fields = [
        'menu',
        'desc_tr',
        'keywords_tr',
        'desc_en',
        'keywords_en',
        'page_cover',
        'meta_cover'
    ]
    readonly_fields = ['meta_cover']

class MenusAdmin(admin.ModelAdmin):
    list_display = ["parent_id","title_tr","title_en","order_number","header_position","footer_position","is_active"]
    list_editable = ["order_number","header_position","footer_position","is_active"]
    list_display_links = ["title_tr","title_en","parent_id"]
    list_per_page = 50
    readonly_fields = ["slug_tr","slug_en"]
    list_filter = ["header_position","footer_position"]
    search_fields = ["title_tr","title_en","slug_tr","slug_en"]

    inlines = [
        MetaDescriptionTagsInlineAdmin,
    ]

    class Meta:
        model = Menus

admin.site.register(Menus,MenusAdmin)

class PagesAdmin(admin.ModelAdmin):
    list_display = ["menus","title","order_number","is_active"]
    list_editable = ["order_number","is_active"]
    fields = [
        'menus',
        'title',
        'content_text',
        'page_cover',
        'preview_cover',
        'order_number',
        'is_active',
        'slug',
    ]
    readonly_fields = ["slug","preview_cover"]
    list_per_page = 50

    class Meta:
        model = Pages

admin.site.register(Pages,PagesAdmin)

class PagesTranslatesAdmin(admin.ModelAdmin):
    list_display = ["menu_title_tr","languages","title","order_number","is_active"]

    list_editable = ["order_number","is_active"]

    list_per_page = 50

    readonly_fields = ["slug"]

    list_filter = ["languages"]

    class Meta:
        model = PagesTranslates

    def menu_title_tr(self,instance):
        return instance.pages.menus.title_tr

admin.site.register(PagesTranslates,PagesTranslatesAdmin)

class CompanyInfosAdmin(admin.ModelAdmin):

    list_per_page = 50
    list_display = ["title","title_value","slug"]

    fields = [
        'title',
        'title_value',
        'slug',
    ]

    readonly_fields = ["slug"]

    class Meta:
        model = CompanyInfos

admin.site.register(CompanyInfos,CompanyInfosAdmin)

class BannersAdmin(admin.ModelAdmin):
    list_per_page = 50
    list_display = ["title_tr","banner_cover","order"]
    fields = [
        'title_tr',
        'title_en',
        'content_tr',
        'content_en',
        'banner_cover',
        'custom_url',
        'order',
        'slug',
    ]
    list_editable = ["order"]
    readonly_fields = ["slug","preview_cover"]

admin.site.register(Banners,BannersAdmin)
