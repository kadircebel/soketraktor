from django.db import models
from django.utils.safestring import mark_safe
from ckeditor.fields import RichTextField
from helpers.views import custom_slugify,cover_url_forpages
from django.utils.text import slugify
from .managers import PagesManager


def upload_pagecover(instance, filename):
    import os, random
    basefilename, file_extension = os.path.splitext(filename)
    basefilename = slugify(basefilename)
    chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890'
    randomstr = ''.join((random.choice(chars)) for x in range(10))
    return 'pagecovers/pg_{basename}{randomstring}{ext}'.format(basename=basefilename, randomstring=randomstr,
                                                                ext=file_extension)


class Pages(models.Model):
    id = models.BigAutoField(primary_key=True)
    menus = models.ForeignKey('pages.Menus', on_delete=models.DO_NOTHING, verbose_name="Menü", default=None, blank=True,
                              null=True)
    title = models.CharField(max_length=250, verbose_name="Sayfa Adı TR", blank=True, null=True)
    content_text = RichTextField(verbose_name="İçerik TR", blank=True, null=True)
    page_cover = models.ImageField(verbose_name="Kapak Resmi", upload_to=upload_pagecover, blank=True, null=True,
                                   default=None, help_text="Sayfaya Banner resmi yüklenebilir. Zorunlu değildir.")
    slug = models.SlugField(max_length=650, verbose_name="Sayfa Url", blank=True, null=True)
    order_number = models.IntegerField(default=0, verbose_name="Sayfa Sırası")
    is_active = models.BooleanField(default=True, verbose_name="Aktif Mi?")

    objects = PagesManager()

    class Meta:
        managed = True
        db_table = "pages"
        verbose_name = "Sayfa"
        verbose_name_plural = "Sayfalar"

    def __str__(self):
        return "{} -|- {}".format(self.menus.title_tr, self.title)

    def preview_cover(self):
        if self.page_cover is None:
            return '<img src="/static/images/empty.png" style="max-height:112px;" width="" height="" />'
        else:
            return mark_safe(
                '<img src="/media/{}" style="max-height:150px;" width="" height="" />'.format(self.page_cover))

    preview_cover.short_description = 'Kapak Resmi'
    preview_cover.allow_tags = True

    def save(self, *args, **kwargs):
        self.slug = custom_slugify(self.title)
        super(Pages, self).save(*args, **kwargs)


class PagesTranslates(models.Model):
    pages = models.ForeignKey('pages.Pages', related_name="pages_translates", verbose_name="Sayfa",
                              on_delete=models.DO_NOTHING)
    languages = models.ForeignKey('languages.Languages', verbose_name="Dil", on_delete=models.DO_NOTHING)
    title = models.CharField(max_length=250, verbose_name="Sayfa Adı EN", blank=True, null=True)
    content_text = RichTextField(verbose_name="", blank=True, null=True)
    slug = models.SlugField(max_length=650, verbose_name="Sayfa Url", blank=True, null=True)
    order_number = models.IntegerField(default=0, verbose_name="Sayfa Sırası")
    is_active = models.BooleanField(default=True, verbose_name="Aktif Mi?")

    class Meta:
        managed = True
        db_table = "pages_translates"
        verbose_name = "Page"
        verbose_name_plural = "Pages"

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        self.slug = custom_slugify(self.title)
        super(PagesTranslates, self).save(*args, **kwargs)

    @classmethod
    def get_page(cls, slug):
        return cls.objects.get(slug=slug).pages


class Menus(models.Model):
    id = models.BigAutoField(primary_key=True)
    parent_id = models.ForeignKey('self', on_delete=models.DO_NOTHING, verbose_name="Ana Menü", default=None, null=True,
                                  blank=True)
    categories = models.ForeignKey('categories.Categories', on_delete=models.DO_NOTHING, verbose_name="Kategori",
                                   default=None, blank=True, null=True,
                                   help_text="Bu sayfanın bağlanacağı kategoriyi seçebilirsiniz. Böylelikle o sayfada doğrudan kategori görünecektir.")
    title_tr = models.CharField(max_length=150, verbose_name="Menu Adı TR")
    title_en = models.CharField(max_length=150, verbose_name="Menu Adı EN")
    slug_tr = models.CharField(max_length=150, verbose_name="Url TR", blank=True, null=True)
    slug_en = models.CharField(max_length=150, verbose_name="Url EN", blank=True, null=True)
    order_number = models.IntegerField(default=0, verbose_name="Sıra No")
    header_position = models.BooleanField(default=True, verbose_name="Üst Menü")
    footer_position = models.BooleanField(default=False, verbose_name="Alt Menü")
    is_active = models.BooleanField(default=True, verbose_name="Aktif Mi?")

    class Meta:
        managed = True
        db_table = "menus"
        verbose_name = "Sayfa Menüsü"
        verbose_name_plural = "Sayfa Menüleri"

    def __str__(self):
        if self.parent_id != None:
            return "{} - {}".format(self.parent_id, self.title_tr)
        else:
            return "{}".format(self.title_tr)

    def save(self, *args, **kwargs):
        self.slug_tr = custom_slugify(self.title_tr)
        self.slug_en = custom_slugify(self.title_en)
        super(Menus, self).save(*args, **kwargs)

    @classmethod
    def get_menus(cls, language, position):
        context = []

        if position == "top":
            menu_list = cls.objects.filter(header_position=True, is_active=True, parent_id=None).order_by("order_number").all()
        elif position == "bottom":
            menu_list = cls.objects.filter(footer_position=True, is_active=True, parent_id=None).order_by("order_number").all()
        else:
            menu_list = cls.objects.filter(footer_position=False, is_active=True, parent_id=None).order_by("order_number").all()

        if language == "tr":
            for item in menu_list:
                context_sub = []
                if cls.objects.filter(parent_id=item.id, is_active=True).count() > 0:
                    for itemsub in cls.objects.filter(parent_id=item.id, is_active=True).order_by("order_number").all():
                        if itemsub.categories is not None:
                            cat_slug = "categories/" + itemsub.slug_tr
                        else:
                            cat_slug = "pages/" + itemsub.slug_tr

                        item_sub = {
                            'title': itemsub.title_tr,
                            'slug': cat_slug,
                            'parent_id': itemsub.parent_id
                        }
                        context_sub.append(item_sub)

                    if item.categories is not None:
                        cat_slug = "categories/" + item.slug_tr
                    else:
                        cat_slug = "pages/" + item.slug_tr

                    item_menu = {
                        'title': item.title_tr,
                        'slug': cat_slug,
                        'id': item.id,
                        "sub_menu": context_sub,
                    }

                else:
                    if item.categories is not None:
                        cat_slug = "categories/" + item.slug_tr
                    else:
                        cat_slug = "pages/" + item.slug_tr

                    item_menu = {
                        'title': item.title_tr,
                        'slug': cat_slug,
                        'parent_id': item.parent_id,
                        "sub_menu": context_sub,
                    }

                context.append(item_menu)

        else:
            for item in menu_list:
                context_sub = []
                if cls.objects.filter(parent_id=item.id, is_active=True).count() > 0:
                    for itemsub in cls.objects.filter(parent_id=item, is_active=True).order_by("order_number").all():
                        if itemsub.categories is not None:
                            cat_slug = "categories/" + itemsub.slug_en
                        else:
                            cat_slug = "pages/" + itemsub.slug_en
                        item_sub = {
                            'title': itemsub.title_en,
                            'slug': cat_slug,
                            'parent_id': itemsub.parent_id
                        }
                        context_sub.append(item_sub)

                    if item.categories is not None:
                        cat_slug = "categories/" + item.slug_en
                    else:
                        cat_slug = "pages/" + item.slug_en

                    item_menu = {
                        'title': item.title_en,
                        'slug': cat_slug,
                        'id': item.id,
                        "sub_menu": context_sub,
                    }

                else:

                    if item.categories is not None:
                        cat_slug = "categories/" + item.slug_en
                    else:
                        cat_slug = "pages/" + item.slug_en

                    item_menu = {
                        'title': item.title_en,
                        'slug': cat_slug,
                        'parent_id': item.parent_id,
                        "sub_menu": context_sub,
                    }

                context.append(item_menu)

        return context

    @classmethod
    def get_menu(cls, language, slug, position):
        if language == "tr":
            if position == "top":
                return cls.objects.get(slug_tr=slug, header_position=True, footer_position=False)
            elif position == "bottom":
                return cls.objects.get(slug_tr=slug, header_position=False, footer_position=True)
            else:
                return cls.objects.get(slug_tr=slug, header_position=False, footer_position=False)

        else:
            if position == "top":
                return cls.objects.get(slug_en=slug, header_position=True, footer_position=False)
            elif position == "bottom":
                return cls.objects.get(slug_en=slug, header_position=False, footer_position=True)
            else:
                return cls.objects.get(slug_en=slug, header_position=False, footer_position=False)

    @classmethod
    def get_parent(cls, language, slug, position):
        if language == "tr":
            if position == "top":
                selected = cls.objects.get(slug_tr=slug, header_position=True, footer_position=False)
            elif position == "bottom":
                selected = cls.objects.get(slug_tr=slug, header_position=False, footer_position=True)
            else:
                selected = cls.objects.get(slug_tr=slug, header_position=False, footer_position=False)

            if selected.parent_id != None:
                return selected.parent_id
            else:
                return None

        else:
            if cls.objects.filter(slug_en=slug).exists():
                if position == "top":
                    selected = cls.objects.get(slug_en=slug, header_position=True, footer_position=False)
                elif position == "bottom":
                    selected = cls.objects.get(slug_en=slug, header_position=False, footer_position=True)
                else:
                    selected = cls.objects.get(slug_en=slug, header_position=False, footer_position=False)

                if selected.parent_id != None:
                    return selected.parent_id
                else:
                    return None

    @classmethod
    def get_submenus_for_pages(cls, language, slug, position):
        parent_item = Menus.get_parent(language, slug, position)

        if parent_item != None:
            return cls.objects.filter(parent_id=parent_item.id, is_active=True).order_by("order_number").all()
        else:
            return None

    @classmethod
    def category_page_for_metatag(cls, category_obj,language):
        if language == "tr":
            if cls.objects.filter(categories=category_obj).exists():
                return cls.objects.get(categories=category_obj)
            else:
                return None
        else:
            if cls.objects.filter(categories=category_obj.categories).exists():
                return cls.objects.get(categories=category_obj.categories)
            elif cls.objects.filter(categories=category_obj.categories.parent_id).exists():
                return cls.objects.get(categories=category_obj.categories.parent_id)
            else:
                return None



    @classmethod
    def get_parentmenupage_for_metatag(cls, slug, language):

        if language == "tr":
            if cls.objects.filter(slug_tr=slug).exists():
                selected = cls.objects.get(slug_tr=slug)
                if selected.parent_id is not None:
                    return selected
            else:
                return None
        else:
            if cls.objects.filter(slug_en=slug).exists():
                selected = cls.objects.get(slug_en=slug)
                if selected.parent_id is not None:
                    return selected
            else:
                return None

    @classmethod
    def get_menupage_for_metatag(cls, slug, language):

        if language == "tr":
            if cls.objects.filter(slug_tr=slug).exists():
                return cls.objects.get(slug_tr=slug)
            else:
                selected = Menus.get_parentmenupage_for_metatag(slug, language)
                if selected is not None:
                    return selected
                else:
                    # sayfa bir kategori sayfasıdır
                    from categories.models import Categories
                    category = Categories.objects.get_category(slug, language)
                    selected = Menus.category_page_for_metatag(category,language)

                    if selected is not None:
                        return selected
                    elif category.parent_id is not None:
                        return category.parent_id
                    else:
                        return None

        else:
            if cls.objects.filter(slug_en=slug).exists():
                return cls.objects.get(slug_en=slug)
            else:
                selected = Menus.get_parentmenupage_for_metatag(slug, language)
                if selected is not None:
                    return selected
                else:
                    # sayfa kategori sayfasıdır. - ingilizce
                    from categories.models import Categories
                    category = Categories.objects.get_category(slug, language)

                    # selected = Menus.category_page_for_metatag(category.categories,language)
                    selected = Menus.category_page_for_metatag(category, language)
                    if selected is not None:
                        return selected
                    else:
                        return None


def upload_metaimage(instance, filename):
    import os, random
    basefilename, file_extension = os.path.splitext(filename)
    basefilename = slugify(basefilename)
    chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890'
    randomstr = ''.join((random.choice(chars)) for x in range(10))
    return 'meta_images/pg_{basename}{randomstring}{ext}'.format(basename=basefilename, randomstring=randomstr,
                                                                 ext=file_extension)


class MetaDescriptionTags(models.Model):
    id = models.BigAutoField(primary_key=True)
    menu = models.ForeignKey('pages.Menus', on_delete=models.DO_NOTHING, verbose_name="Menü - Sayfa", default=None,
                             null=True, blank=True)
    desc_tr = models.TextField(verbose_name="Description Açıklaması TR", max_length=500, blank=True, null=True,
                               help_text="En fazla 160 karakter olmalı")
    desc_en = models.TextField(verbose_name="Description Açıklaması EN", max_length=500, blank=True, null=True,
                               help_text="En fazla 160 karakter olmalı")
    keywords_tr = models.TextField(verbose_name="Keywords TR", max_length=500, blank=True, null=True,
                                   help_text="10 adetten fazla kullanılmamaya özen gösterilmeli. Virgül koyarak yazılmalı.")
    keywords_en = models.TextField(verbose_name="Keywords EN", max_length=500, blank=True, null=True,
                                   help_text="10 adetten fazla kullanılmamaya özen gösterilmeli.Virgül koyarak yazılmalı.")
    page_cover = models.ImageField(verbose_name="Sayfa Resmi", upload_to=upload_metaimage, blank=True, null=True,
                                   default=None, help_text="Paylaşımlarda gösterilecek resimin belirlenmesini sağlar.")

    class Meta:
        managed = True
        db_table = "meta_tags"
        verbose_name = "Meta Ayarı"
        verbose_name_plural = "Meta Ayarları"

    def __str__(self):
        return self.menu.title_tr

    def meta_cover(self):
        if self.page_cover is None:
            return '<img src="/static/images/empty.png" style="max-height:112px;" width="" height="" />'
        else:
            return mark_safe(
                '<img src="/media/{}" style="max-height:150px;" width="" height="" />'.format(self.page_cover))

    meta_cover.short_description = 'Meta Resmi'
    meta_cover.allow_tags = True

    @classmethod
    def get_selected_meta(cls, slug, language):
        from helpers.views import cover_url_forpages
        from categories.models import Categories
        from categories_translates.models import CategoriesTranslates
        context = []
        selected = Menus.get_menupage_for_metatag(slug, language)

        if type(selected) == Categories:
            selected_metacategory = Menus.category_page_for_metatag(selected,language)
            if selected_metacategory.parent_id.menus_set.exists():
                selected_meta = cls.objects.get(menu=selected_metacategory.parent_id.menus_set.last())
                if len(cover_url_forpages(selected_meta.page_cover)) > 0:
                    meta_cover = cover_url_forpages(selected_meta.page_cover)
                else:
                    meta_cover = "/static/images/mail_cover.jpg"

                if language == "tr":
                    item_context = {
                        'description': selected_meta.desc_tr,
                        'keywords': selected_meta.keywords_tr,
                        'cover': meta_cover,
                        'page_title': selected_metacategory.categories.title,
                    }
                else:
                    item_context = {
                        'description': selected_meta.desc_en,
                        'keywords': selected_meta.keywords_en,
                        'cover': meta_cover,
                        'page_title': selected_metacategory.categories.categoriestranslates_set.get().title,
                    }

                context.append(item_context)

        else:

            if selected is not None:
                if cls.objects.filter(menu=selected).exists():
                    selected_meta = cls.objects.get(menu=selected)
                    if len(cover_url_forpages(selected_meta.page_cover)) > 0:
                        meta_cover = cover_url_forpages(selected_meta.page_cover)
                    else:
                        meta_cover = "/static/images/mail_cover.jpg"

                    if language == "tr":
                        item_context = {
                            'description': selected_meta.desc_tr,
                            'keywords': selected_meta.keywords_tr,
                            'cover': meta_cover,
                            'page_title': selected.title_tr,
                        }
                    else:
                        item_context = {
                            'description': selected_meta.desc_en,
                            'keywords': selected_meta.keywords_en,
                            'cover': meta_cover,
                            'page_title': selected.title_en,
                        }

                    context.append(item_context)


        return context

class CompanyInfos(models.Model):
    id = models.BigAutoField(primary_key=True)
    title = models.CharField(max_length=250, verbose_name="Bilgi Tipi", blank=True, null=True)
    title_value = models.CharField(max_length=900, verbose_name="Bilgi Değeri", blank=True, null=True)
    slug = models.SlugField(max_length=300, verbose_name="Url", null=True, blank=True)

    class Meta:
        managed = True
        db_table = "company_infos"
        verbose_name = "Şirket Bilgisi"
        verbose_name_plural = "Şirket Bilgileri"

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        self.slug = custom_slugify(self.title)
        super(CompanyInfos, self).save(*args, **kwargs)

    @classmethod
    def get_company_email(cls,mail_type):
        return cls.objects.get(slug=mail_type).title_value

    @classmethod
    def get_company_address(cls):
        return cls.objects.get(slug="sirket-adresi").title_value

    @classmethod
    def get_company_infos(cls):
        from helpers.views import revise_phone_info
        context = [{
            'address':CompanyInfos.get_company_address(),
            'email':CompanyInfos.get_company_email("email"),
            'phone': CompanyInfos.get_company_email('phone'),
            'phone_value':revise_phone_info(CompanyInfos.get_company_email('phone')),
        }]
        return context


def upload_bannerimage(instance, filename):
    import os, random
    basefilename, file_extension = os.path.splitext(filename)
    basefilename = slugify(basefilename)
    chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890'
    randomstr = ''.join((random.choice(chars)) for x in range(10))
    return 'banners/bn_{basename}{randomstring}{ext}'.format(basename=basefilename, randomstring=randomstr,
                                                                 ext=file_extension)

class Banners(models.Model):
    id = models.BigAutoField(primary_key=True)
    title_tr = models.CharField(max_length=500,verbose_name="Başlık TR",blank=True,null=True)
    title_en = models.CharField(max_length=500, verbose_name="Başlık EN", blank=True, null=True)
    content_tr = models.TextField(verbose_name="Açıklama TR", blank=True, null=True)
    content_en = models.TextField(verbose_name="Açıklama EN", blank=True, null=True)
    banner_cover = models.ImageField(verbose_name="Banner Resmi", upload_to=upload_bannerimage, blank=True, null=True)
    custom_url = models.CharField(max_length=800,verbose_name="Banner Link Url",blank=True,null=True,help_text="Slayt nesnesine tıklandığında gitmesini istediğiniz linki buraya yapıştırabilirsiniz. - Zorunlu değildir.")
    order = models.IntegerField(default=0, verbose_name="Banner Sırası", blank=True, null=True)
    slug = models.SlugField(max_length=550,verbose_name="Banner Özel Url",blank=True,null=True)

    class Meta:
        managed = True
        db_table = "banners"
        verbose_name = "Slayt Resimi"
        verbose_name_plural = "Slayt Resimleri"

    def __str__(self):
        return self.title_tr

    def save(self, *args, **kwargs):
        self.slug = custom_slugify(self.title_tr)
        super(Banners, self).save(*args, **kwargs)

    def preview_cover(self):
        if self.banner_cover is None:
            return '<img src="/static/images/empty.png" style="max-height:112px;" width="" height="" />'
        else:
            return mark_safe(
                '<img src="/media/{}" style="max-height:150px;" width="" height="" />'.format(self.banner_cover))

    preview_cover.short_description = 'Banner Resmi'
    preview_cover.allow_tags = True


    @classmethod
    def get_banners(cls,language):
        context = []
        if cls.objects.count() > 0:
            for item in cls.objects.all().order_by("order"):
                if language == "tr":
                    item_banner = {
                        'title': item.title_tr,
                        'content': item.content_tr,
                        'custom_url': item.custom_url,
                        'cover': cover_url_forpages(item.banner_cover),
                        'order': item.order,
                    }
                else:
                    item_banner = {
                        'title': item.title_en,
                        'content': item.content_en,
                        'custom_url': item.custom_url,
                        'cover': cover_url_forpages(item.banner_cover),
                        'order': item.order,
                    }

                context.append(item_banner)

        return context