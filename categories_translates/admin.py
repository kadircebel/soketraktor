from django.contrib import admin
from .models import *

class CategoriesTranslatesAdmin(admin.ModelAdmin):

    fields = ["categories", "languages", "title", "content_text", "order_number", "is_active"]

    list_display = ["categories","languages","title","order_number","is_active"]

    list_editable = ["order_number","is_active"]

    list_filter = ["languages"]

    search_fields = ["title", "slug"]

    list_per_page = 50

    readonly_fields = ["slug"]

    class Meta:
        model = CategoriesTranslates

admin.site.register(CategoriesTranslates,CategoriesTranslatesAdmin)

