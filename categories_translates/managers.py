from django.db import models

class CategoriesTranslatesManager(models.Manager):
    def get_category(self,slug):
        return self.get(slug=slug)

    def get_categories(self):
        return self.filter(is_active=True).all()