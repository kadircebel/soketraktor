from django.apps import AppConfig


class CategoriesTranslatesConfig(AppConfig):
    name = 'categories_translates'
