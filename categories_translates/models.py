from django.db import models
from django.utils.text import slugify
from ckeditor.fields import RichTextField
from helpers.views import custom_slugify
from .managers import CategoriesTranslatesManager

class CategoriesTranslates(models.Model):
    id = models.BigAutoField(primary_key=True)
    categories = models.ForeignKey('categories.Categories',db_index=True,verbose_name="Kategori",on_delete=models.DO_NOTHING)
    languages = models.ForeignKey('languages.Languages',verbose_name="Dil",on_delete=models.DO_NOTHING)
    title = models.CharField(max_length=600,verbose_name="Başlık",help_text="Dilinizin çeşidine göre başlık bilgisi giriniz.")
    content_text = RichTextField(verbose_name="", blank=True, null=True)
    slug = models.SlugField(max_length=650,verbose_name="Kategori Url",blank=True,null=True)
    order_number = models.IntegerField(default=0, verbose_name="Sıra Numarası", help_text="Kategori Sırası")
    is_active = models.BooleanField(default=True)

    objects = CategoriesTranslatesManager()

    class Meta:
        managed = True
        db_table = "categories_translates"
        verbose_name = "Category"
        verbose_name_plural = "Categories"
        ordering = ["order_number"]

    def __str__(self):
        return self.title

    def save(self,*args,**kwargs):
        self.slug = slugify(custom_slugify(self.title))
        super(CategoriesTranslates,self).save(*args,**kwargs)