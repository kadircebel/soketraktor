from django.shortcuts import render,HttpResponse

from django.views import View
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
from django.utils import translation
from django.utils.text import slugify
from pages.models import Menus,CompanyInfos,MetaDescriptionTags,Banners
from products.models import Products,ExcelProdcts,ProductsOemNumbers
from categories.models import Categories
from customers.models import Customers
from helpers.views import basket_count,total_basket_price,change_basket_for_language,notifications_count,sendmail_company
from basket.models import Basket
import json,csv


@method_decorator(csrf_exempt, name='dispatch')
class index(View):
    template_name = "home/index.html"

    def get(self,request):
        # from django.db.models import Count, Q
        translation.activate(translation.get_language())
        notification_count = 0

        product_counts = Products.objects.product_count()
        categories_count = Categories.objects.category_count("",translation.get_language())
        categories = Categories.objects.categories_list(0,16,"id",translation.get_language(),"html")
        menus = Menus.get_menus(translation.get_language(),"top")
        bottom_menu = Menus.get_menus(translation.get_language(), "bottom")
        products = Products.objects.list_with_pager(0,16,True,"id",translation.get_language())

        # for item in ExcelProdcts.objects.all():
        #     if ExcelProdcts.objects.filter(slug_tr=item.slug_tr).count() > 1:
        #         item.slug_tr = "{}-{}".format(item.slug_tr,custom_slugify(item.oem_number))
        #         item.save()



        # dublicates = ProductsOemNumbers.objects.values('oem_number',"products__slug").annotate(slug_count=Count('oem_number'))
        # # print(dublicates)
        # for item in dublicates:
        #     if item["slug_count"] == 2:
        #         # print(item)
        #         print(item["products__slug"])
        #         print("********")
        #         for itemprd in Products.objects.filter(slug=item["products__slug"]).all().order_by("-id")[:(Products.objects.filter(slug=item["products__slug"]).count() - (Products.objects.filter(slug=item["products__slug"]).count()-1))]:
        #             print(itemprd.title)
        #             selected = Products.objects.get(id=itemprd.id)
        #             selected.is_active = False
        #             selected.save()
        #             # itemprd.filter = False
        #             # itemprd.save()
        #             print("********")


        if translation.get_language() == "tr":
            selected_meta = MetaDescriptionTags.get_selected_meta("anasayfa",translation.get_language())
        else:
            selected_meta = MetaDescriptionTags.get_selected_meta("home",translation.get_language())

        # basket operations
        if "basket" in request.session:
            request.session["basket"] = change_basket_for_language(request, translation.get_language())
            basket_totalcount = basket_count(request.session["basket"])
            basket_totalprice = total_basket_price(request.session["basket"], translation.get_language())
        else:
            basket_totalprice = None
            basket_totalcount = 0

        # basket operations

        if "customer_id" in request.session and request.user.is_authenticated:
            selected_customer = Customers.get_customer_by_id(request.session["customer_id"])
            order_count = Basket.get_order_count(selected_customer)
            notification_count = notifications_count(selected_customer)
        else:
            order_count = 0


        if translation.get_language() == "tr":
            change_lang = "/en"
        else:
            change_lang = "/tr"


        context = {
            'products_count':product_counts,
            'categories_count':categories_count,
            'categories':categories,
            'top_menu':menus,
            'bottom_menu':bottom_menu,
            'products':products,
            'basket_count':basket_totalcount,
            'basket_price':basket_totalprice,
            'order_count':order_count,
            'notification_count': notification_count,
            'footer_address':CompanyInfos.get_company_address(),
            'footer_email':CompanyInfos.get_company_email('email'),
            'meta_tags':selected_meta,
            'change_language':change_lang,
            'banners':Banners.get_banners(translation.get_language()),
        }
        return render(request,self.template_name,context)

    def post(self,request):
        pass


@method_decorator(csrf_exempt, name='dispatch')
class contact_form(View):
    template_name = ""

    def get(self,request):
        pass

    def post(self,request):
        name = request.POST.get("name")
        email_address = request.POST.get("email")
        phone_number = request.POST.get("phone")
        message_text = request.POST.get("message")
        language = request.POST.get("language")

        if request.is_secure():
            host_address = "https://" + request.get_host() + "/"
        else:
            host_address = "http://" + request.get_host() + "/"

        if sendmail_company(host_address,name,email_address,phone_number,message_text):
            situation = True
            if language == "tr":
                message = "Mesajınız başarılı bir şekilde iletilmiştir. En kısa zamanda bilgilendirme yapacağız. Teşekkürler."
            else:
                message = "Your message has been sent successfully. We will inform you as soon as possible. Thank you."
        else:
            situation = False
            if language == "tr":
                message = "Mesajınızın gönderimi sırasında bir hata meydana geldi. Bizi telefon ile arayabilirsiniz veya doğrudan eposta gönderebilirsiniz. Teşekkürler."
            else:
                message = "An error occurred while sending your message. You can call us or send an e-mail directly. Thank you."


        context = {
            'situation':situation,
            'message':message,
        }

        return HttpResponse(json.dumps(context),content_type="application/json")


def handler404(request,exception):
    return render(request,'404.html')

def handler500(request):
    return render(request,'500.html')

def handler400(request,exception):
    return render(request,'400.html')

def handler403(request,exception):
    return render(request,'403.html')


def export_products_excel(request):
    response = HttpResponse(content_type="text/csv")
    response['Content-Disposition'] = 'attachment; filename="products.csv"'
    writer = csv.writer(response)
    writer.writerow(['Ürün Adı', 'OEM Numarası', 'SUB OEM Numarası', 'Fiyat (Dolar Cinsinden) '])
    products = Products.objects.filter(is_active=True).all()
    for item in products:
        writer.writerow([item.title,item.productsoemnumbers_set.get().oem_number,item.productsoemnumbers_set.get().sub_number,'',])

    for item in ExcelProdcts.objects.filter(is_active=True).all():
        writer.writerow([item.title_tr, item.oem_number, '','', ])

    return response