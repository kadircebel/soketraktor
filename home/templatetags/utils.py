from django import template

register = template.Library()

@register.filter()
def calc_mod(number_value):
    return int(number_value) % 4

@register.filter()
def get_tag(dict,key_value):
    for item in dict:
        if item.get(key_value):
            return item.get(key_value)
        else:
            return ""
