from django.contrib import admin
from .models import *


class PaymentTypesAdmin(admin.ModelAdmin):
    list_display = ["title_tr", "title_en", "is_active"]
    list_editable = ["is_active"]
    list_per_page = 50


admin.site.register(PaymentTypes, PaymentTypesAdmin)


class PaymentCardsAdmin(admin.ModelAdmin):
    list_display = ["title_tr", "title_en", "is_active"]
    list_editable = ["is_active"]

    fields = [
        "payment_types",
        "title_tr",
        "title_en",
        "account_number",
        "iban_number",
        "account_person",
        "page_cover",
        "preview_cover",
        "is_active",
    ]

    readonly_fields = ["preview_cover", "slug"]
    list_per_page = 50


admin.site.register(PaymentCards, PaymentCardsAdmin)


class CompanyCurrenciesAdmin(admin.ModelAdmin):
    list_display = ["title", "currency_logo", "currency_rate"]

    fields = [
        "title",
        "currency_logo",
        "currency_rate",
        "slug",
    ]

    readonly_fields = ["slug"]
    list_per_page = 50


admin.site.register(CompanyCurrencies, CompanyCurrenciesAdmin)


class TaxOptionsAdmin(admin.ModelAdmin):
    list_display = ["title", "percent_value", "is_active"]
    list_editable = ["is_active","percent_value"]

    fields = [
        "title",
        "percent_value",
        "slug",
        "is_active",
    ]

    readonly_fields = ["slug"]
    list_per_page = 50


admin.site.register(TaxOptions, TaxOptionsAdmin)
