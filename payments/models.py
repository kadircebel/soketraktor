from django.db import models
from django.utils.text import slugify
from helpers.views import custom_slugify,get_currency
from django.utils.safestring import mark_safe


class PaymentTypes(models.Model):
    id = models.BigAutoField(primary_key = True)
    title_tr = models.CharField(max_length=250,verbose_name="Başlık TR",blank=True,null=True)
    title_en = models.CharField(max_length=250, verbose_name="Başlık EN", blank=True, null=True)
    slug = models.SlugField(max_length=300,verbose_name="Url",blank=True,null=True)
    is_active = models.BooleanField(default=True,verbose_name="Aktif Mi?")

    class Meta:
        managed = True
        db_table = "payment_types"
        verbose_name = "Ödeme Şekili"
        verbose_name_plural = "Ödeme Şekilleri"

    def __str__(self):
        return self.title_tr

    def save(self, *args,**kwargs):
        super(PaymentTypes,self).save(*args,**kwargs)

    @classmethod
    def get_payment_type(cls,slug):
        return cls.objects.get(slug=slug)



def upload_paymentscover(instance,filename):
    import os,random
    basefilename,file_extension = os.path.splitext(filename)
    basefilename = slugify(basefilename)
    chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890'
    randomstr = ''.join((random.choice(chars)) for x in range(10))
    return 'paymentscover/pymnts_{basename}{randomstring}{ext}'.format(basename=basefilename,randomstring=randomstr, ext=file_extension)


class PaymentCards(models.Model):
    id = models.BigAutoField(primary_key=True)
    payment_types = models.ForeignKey('payments.PaymentTypes',on_delete=models.DO_NOTHING,blank=True,null=True,verbose_name="Ödeme Tipi")
    title_tr = models.CharField(max_length=150,verbose_name="Hesap Adı TR")
    title_en = models.CharField(max_length=150,verbose_name="Hesap Adı EN")
    account_number = models.CharField(max_length=100,verbose_name="Hesap No",null=True,blank=True)
    iban_number = models.CharField(max_length=250,verbose_name="IBAN No",null=True,blank=True)
    account_person = models.CharField(max_length=250,verbose_name="Hesap Sahibi Adı",null=True,blank=True)
    page_cover = models.ImageField(verbose_name="Banka Logosu", upload_to=upload_paymentscover, blank=True, null=True,default=None)
    slug = models.SlugField(max_length=300,verbose_name="Url",blank=True,null=True)
    is_active = models.BooleanField(default=True,verbose_name="Aktif Mi")

    class Meta:
        managed = True
        db_table = "payment_cards"
        verbose_name = "Hesap Bilgisi"
        verbose_name_plural = "Hesap Bilgileri"

    def __str__(self):
        return self.title_tr

    def save(self, *args,**kwargs):
        if self.title_tr is not None:
            self.slug = custom_slugify(self.title_tr)

        super(PaymentCards,self).save(*args,**kwargs)

    def preview_cover(self):
        if self.page_cover is None:
            return '<img src="/static/images/empty.png" style="max-height:112px;" width="" height="" />'
        else:
            return mark_safe('<img src="/media/{}" style="max-height:150px;" width="" height="" />'.format(self.page_cover))

    preview_cover.short_description = 'Hesap Resmi'
    preview_cover.allow_tags = True


class CompanyCurrencies(models.Model):
    id = models.BigAutoField(primary_key=True)
    title = models.CharField(max_length=150,verbose_name="Para Birimi")
    currency_logo = models.CharField(max_length=2,verbose_name="Para Birimi Logo",default=None)
    currency_rate = models.CharField(max_length=150,verbose_name="TL Değeri",default=None,blank=True)
    slug = models.SlugField(max_length=150,verbose_name="Para Birimi Url",blank=True,null=True)

    class Meta:
        managed = True
        db_table = "company_currencies"
        verbose_name = "Sistem Nakit Ayarı"
        verbose_name_plural = "Sistem Nakit Ayarları"

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        if self.title is not None:
            self.slug = custom_slugify(self.title)

        super(CompanyCurrencies, self).save(*args, **kwargs)

    @classmethod
    def get_currency_value(cls):
        return cls.objects.get(slug="dolar").currency_rate

    @classmethod
    def check_dollar_rate(cls):
        currency_rate = get_currency('USD')
        selected = cls.objects.get(slug="dolar")
        selected.currency_rate = currency_rate
        selected.save()
        return selected


class TaxOptions(models.Model):
    id = models.BigAutoField(primary_key=True)
    title = models.CharField(max_length=150,verbose_name="Ayar Adı")
    percent_value = models.CharField(max_length=150,verbose_name="Yüzdelik Değer",help_text="Sayı olarak giriniz. Örn: 18, 32, vs.")
    slug = models.SlugField(max_length=150,verbose_name="Özel Url",blank=True,null=True)
    is_active = models.BooleanField(default=True,verbose_name="Aktif Mi?")

    class Meta:
        managed = True
        db_table = "taxoptions"
        verbose_name = "Fatura Ayarı"
        verbose_name_plural = "Fatura Ayarları"

    def __str__(self):
        return self.title

    def save(self, *args,**kwargs):
        if self.title is not None:
            self.slug = custom_slugify(self.title)

        super(TaxOptions,self).save(*args,**kwargs)

    @classmethod
    def convert_price_tl(cls,price):
        converted = float(price) / float(CompanyCurrencies.get_currency_value())
        return round(converted,2)

    @classmethod
    def get_kdv_price(cls,price,language):
        # print(language)

        kdv = cls.objects.get(slug="kdv").percent_value
        if price is not None:
            new_price = float(price) + ((float(price) * (float(kdv)) / 100))
            if language == "tr":
                return round(float(new_price) * float(CompanyCurrencies.get_currency_value()),2)
                # return float(new_price) * float(CompanyCurrencies.get_currency_value())
            else:
                return round(float(new_price),2)
                # return float(new_price)

        else:
            return None


