from django.contrib import admin
from .models import *

class VariationsAdmin(admin.ModelAdmin):
    list_display = ["title_tr","title_en","slug"]
    list_per_page = 50
    readonly_fields = ["slug"]

    class Meta:
        model = Variations


admin.site.register(Variations,VariationsAdmin)

class VariationUnitsAdmin(admin.ModelAdmin):
    list_per_page = 50
    readonly_fields = ["unit_slug"]
    list_display = ["variations","unit_value_tr","unit_value_en","unit_slug"]
    list_filter = ["variations"]
    search_fields = ["unit_value_tr","unit_value_en","unit_slug"]

    class Meta:
        model = VariationUnits

admin.site.register(VariationUnits,VariationUnitsAdmin)