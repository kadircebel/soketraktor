from django.db import models
from django.utils.text import slugify
from helpers.views import custom_slugify

class Variations(models.Model):
    id = models.BigAutoField(primary_key = True)
    title_tr = models.CharField(max_length=500,verbose_name="Varyasyon Adı TR")
    title_en = models.CharField(max_length=500,verbose_name="Varyasyon Adı EN")
    slug = models.SlugField(max_length=650, verbose_name="Slug Adı", blank=True, null=True)

    class Meta:
        managed = True
        db_table = "variations"
        verbose_name = "Varyasyon"
        verbose_name_plural = "Varyasyonlar"

    def __str__(self):
        return self.title_tr

    def save(self,*args,**kwargs):
        self.slug = slugify(custom_slugify(self.title_tr))
        super(Variations,self).save(*args,**kwargs)


class VariationUnits(models.Model):
    id = models.BigAutoField(primary_key=True)
    variations = models.ForeignKey('variations.Variations',on_delete=models.DO_NOTHING,verbose_name="Varyasyon Tipi")
    unit_value_tr = models.CharField(max_length=150,verbose_name="Birim Açıklaması TR")
    unit_value_en = models.CharField(max_length=150, verbose_name="Birim Açıklaması EN")
    unit_slug = models.CharField(max_length=50,verbose_name="Birim Kısaltması")

    class Meta:
        managed = True
        db_table = "variation_units"
        verbose_name = "Ölçü Birimi"
        verbose_name_plural = "Ölçü Birimleri"

    def __str__(self):
        return "{} - {}".format(self.variations.title_tr,self.unit_value_tr)

    def save(self,*args,**kwargs):
        self.unit_slug = custom_slugify(self.unit_value_en)
        super(VariationUnits,self).save(*args,**kwargs)
