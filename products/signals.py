from django.db.models.signals import post_save, pre_save
from .models import Products, ProductsOemNumbers
from django.dispatch import receiver
from helpers.views import custom_slugify


@receiver(post_save, sender=Products)
def update_edited_product_url(sender,created, instance, **kwargs):
    if not created:
        selected_product = instance
        selected_oem = ProductsOemNumbers.objects.filter(products=selected_product).get()
        if Products.objects.filter(slug=selected_product.slug).count() > 2:
            selected_product.slug = custom_slugify("{newslug}-{oem}".format(newslug=selected_product.title, oem=selected_oem.oem_number))



@receiver(post_save, sender=ProductsOemNumbers)
def update_product_url(sender, instance, created, **kwargs):
    # print("created")
    if created:
        selected_oem = ProductsOemNumbers.objects.filter(oem_number=instance).order_by('-id')[0]
        selected = Products.objects.get(id=selected_oem.products.id)
        # print(len(Products.objects.filter(slug=custom_slugify(selected.title))))
        # ürün urlsi başka bir ürünün url'si ile aynı ise url'yi değiştir ve url'nin yanına oem no ekle
        if len(Products.objects.filter(slug=custom_slugify(selected.title))) > 0:
            selected.slug = custom_slugify(
                "{newslug}-{oem}".format(newslug=selected.title, oem=selected_oem.oem_number))
            selected.save()
        else:
            selected.slug = custom_slugify("{newslug}".format(newslug=custom_slugify(selected.title)))
            selected.save()

