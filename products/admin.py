from django.contrib import admin
from .models import *


class OemNumbersAdmin(admin.ModelAdmin):
    list_per_page = 50
    search_fields = ["oem_number","sub_number"]
    list_display = ["products","oem_number","sub_number","is_active"]
    list_editable = ["is_active"]

admin.site.register(ProductsOemNumbers,OemNumbersAdmin)


class ProductsBrandModelsAdminInline(admin.StackedInline):
    model = ProductsBrandModels
    min_num = 0
    extra = 1

class ProductsOemNumbersAdminInline(admin.StackedInline):
    model = ProductsOemNumbers
    min_num = 0
    extra = 1

class ProductsPricesAdminInline(admin.StackedInline):
    model = ProductsPrices
    min_num = 0
    extra = 1

class ProductsGalleriesAdminInline(admin.StackedInline):
    model = ProductsGalleries
    min_num = 0
    extra = 1

class ProductsVariationsAdminInline(admin.StackedInline):
    model = ProductsVariations
    min_num = 0
    extra = 1

class ProductsTypesAdminInline(admin.StackedInline):
    model = ProductsTypes
    min_num = 1
    extra = 0
    fk_name = "child_product"

class ProductsAdmin(admin.ModelAdmin):
    list_per_page = 50
    list_display = ["categories","title","preview_cover","oem_number","order_number","is_stock","is_active"]
    search_fields = ["title"]
    fields = [
        "categories",
        "title",
        "content_text",
        "product_cover",
        "preview_cover",
        "order_number",
        "tech_note",
        "slug",
        "is_stock",
        "is_active"
    ]
    list_display_links = ["title"]
    list_editable = ["order_number","is_stock","is_active"]
    readonly_fields = ["preview_cover"]
    list_filter = ["categories"]

    inlines = [
        ProductsOemNumbersAdminInline,
        ProductsTypesAdminInline,
        ProductsBrandModelsAdminInline,
        ProductsVariationsAdminInline,
        ProductsPricesAdminInline,
        ProductsGalleriesAdminInline,
    ]

    class Meta:
        model = Products

    def oem_number(instance,obj):
        return obj.productsoemnumbers_set.get().oem_number

    # def get_search_results(self, request, queryset, search_term):
    #     print(search_term)
    #     queryset, use_distinct = super().get_search_results(request, queryset, search_term)
    #     try:
    #         search_term_as_int = int(search_term)
    #
    #     except ValueError:
    #         pass
    #     else:
    #         for item in ProductsOemNumbers.objects.filter(oem_number=search_term_as_int):
    #             queryset |= self.model.objects.filter(id=item.products.id)
    #         # queryset |= self.model.objects.filter(age=search_term_as_int)
    #         # queryset |= self.model.objects.filter(age=search_term_as_int)
    #     return queryset, use_distinct

admin.site.register(Products,ProductsAdmin)

class ExcelProdctsAdmin(admin.ModelAdmin):
    list_display = ["title_tr","oem_number","price_tr","price_dl","is_stock","is_active"]

    fields = [
        "title_tr",
        "title_en",
        "oem_number",
        "content_tr",
        "content_en",
        "price_tr",
        "price_dl",
        "price_eu",
        "is_stock",
        "is_active",
        "slug_tr",
        "slug_en",
    ]

    readonly_fields = ["slug_tr","slug_en"]

    list_per_page = 50

    search_fields = ["oem_number","title_tr","title_en"]

    class Meta:
        model = ExcelProdcts

admin.site.register(ExcelProdcts,ExcelProdctsAdmin)