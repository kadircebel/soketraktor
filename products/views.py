from django.shortcuts import render, HttpResponse
from django.views import View
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
import json
from pages.models import Menus,CompanyInfos,MetaDescriptionTags
from customers.models import Customers
from basket.models import Basket
from django.utils import translation
from .models import Products, ProductsTypes, ProductsTranslates, ProductsOemNumbers, ExcelProdcts
from categories.models import Categories
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from helpers.views import change_searchtext, basket_count, total_basket_price, change_basket_for_language,notifications_count


@method_decorator(csrf_exempt, name='dispatch')
class products_index(View):
    template_name = "products/index.html"

    def get(self, request):
        notification_count = 0
        menus = Menus.get_menus(translation.get_language(), "top")
        bottom_menu = Menus.get_menus(translation.get_language(), "bottom")
        products = Products.objects.list_with_pager(0, 16, True, "id", translation.get_language())
        product_count = Products.objects.product_count()

        # basket operations
        if "basket" in request.session:
            request.session["basket"] = change_basket_for_language(request, translation.get_language())
            basket_totalcount = basket_count(request.session["basket"])
            basket_totalprice = total_basket_price(request.session["basket"], translation.get_language())
        else:
            basket_totalprice = None
            basket_totalcount = 0

        # basket operations

        if product_count > len(products):
            has_next = True
        else:
            has_next = False

        if "customer_id" in request.session and request.user.is_authenticated:
            order_count = Basket.get_order_count(Customers.get_customer_by_id(request.session["customer_id"]))
            notification_count = notifications_count(Customers.get_customer_by_id(request.session["customer_id"]))
        else:
            order_count = 0

        if translation.get_language() == "tr":
            change_lang = "/en/products"
        else:
            change_lang = "/tr/products"


        context = {
            'top_menu': menus,
            'bottom_menu': bottom_menu,
            'has_next': has_next,
            'products': products,
            'prd_count': product_count,
            'products_count': product_count,
            'page_type': 'all',
            'page': 2,
            'basket_count': basket_totalcount,
            'basket_price': basket_totalprice,
            'order_count':order_count,
            'notification_count': notification_count,
            'footer_address': CompanyInfos.get_company_address(),
            'footer_email': CompanyInfos.get_company_email('email'),
            'change_language': change_lang,
        }

        return render(request, self.template_name, context)

    def post(self, request):
        page = request.POST.get("page")
        language = request.POST.get("language")
        products = Products.objects.list_with_pager(0, 0, True, "id", language)

        paginator = Paginator(products, 16)

        try:
            prd_list = paginator.page(page)
        except PageNotAnInteger:
            prd_list = paginator.page(1)
        except EmptyPage:
            prd_list = paginator.page(paginator.num_pages)

        pager = str(int(page) + 1)

        if prd_list.has_next() == False:
            if language == "tr":
                message = "Bulunduğunuz kategorideki tüm parçaları görüntülüyorsunuz."
            else:
                message = "You're viewing all spare parts in this category."
        else:
            message = ""

        context = {
            'page': pager,
            'products': list(prd_list),
            'has_next': prd_list.has_next(),
            'message': message,
            'language': language,
            # 'category_url': '/' + translation.get_language() + '/' + category_slug,

        }

        return HttpResponse(json.dumps(context), content_type="application/json")


@method_decorator(csrf_exempt, name='dispatch')
class product_list(View):
    template_name = "products/list.html"

    def get(self, request, slug):
        notification_count = 0
        category = Categories.objects.get_category(slug, translation.get_language())
        selected_meta = MetaDescriptionTags.get_selected_meta(slug, translation.get_language())
        menus = Menus.get_menus(translation.get_language(), "top")
        bottom_menu = Menus.get_menus(translation.get_language(), "bottom")
        products = Products.objects.list_by_categories(0, 16, True, "id", slug, translation.get_language(), "html")
        parent_categories = Categories.objects.get_parents(slug, translation.get_language())
        if translation.get_language() == "en":
            parent_cat = category.categories.parent_id
            change_lang = "/tr/products/" + category.categories.slug
        else:
            # print(category.categoriestranslates_set.exists())
            parent_cat = category.parent_id
            if category.categoriestranslates_set.exists():
                change_lang = "/en/products/" + category.categoriestranslates_set.get().slug
            else:
                change_lang = "javascript:void(0);"

        pocket_list = Categories.objects.get_parent_categories(translation.get_language(), parent_cat)

        paginator = Paginator(products, 16)
        try:
            prd_list = paginator.page(1)
        except PageNotAnInteger:
            prd_list = paginator.page(1)
        except EmptyPage:
            prd_list = paginator.page(paginator.num_pages)

        page = str(int(1) + 1)

        if Products.objects.counts_by_category(True, slug, translation.get_language()) > len(products):
            has_next = True
        else:
            has_next = False

        # basket operations
        if "basket" in request.session:
            request.session["basket"] = change_basket_for_language(request, translation.get_language())
            basket_totalcount = basket_count(request.session["basket"])
            basket_totalprice = total_basket_price(request.session["basket"], translation.get_language())
        else:
            basket_totalprice = None
            basket_totalcount = 0

        # basket operations

        if "customer_id" in request.session and request.user.is_authenticated:
            order_count = Basket.get_order_count(Customers.get_customer_by_id(request.session["customer_id"]))
            notification_count = notifications_count(Customers.get_customer_by_id(request.session["customer_id"]))
        else:
            order_count = 0

        context = {
            "slug": slug,
            "products": prd_list,
            'first_product_records': len(products),
            "category": category,
            "prd_count": Products.objects.counts_by_category(True, slug, translation.get_language()),
            "parent_categories": parent_categories,
            "page": page,
            "has_next": has_next,
            'top_menu': menus,
            "page_type": "list",
            'bottom_menu': bottom_menu,
            'pocket_list': pocket_list,
            'basket_count': basket_totalcount,
            'basket_price': basket_totalprice,
            'order_count':order_count,
            'notification_count': notification_count,
            'footer_address': CompanyInfos.get_company_address(),
            'footer_email': CompanyInfos.get_company_email('email'),
            'meta_tags': selected_meta,
            'change_language': change_lang,
        }
        return render(request, self.template_name, context)

    def post(self, request):
        category_slug = request.POST.get("category")
        # category = Categories.objects.get_category(category_slug,translation.get_language())
        # prd_count = request.POST.get("count")
        page = request.POST.get("page")
        language = request.POST.get("language")

        # prd_count minimum sayımız olacak yani çekeceğimiz kayıdın başladığı index
        # prd_count+2 de maksimum sayımız olacak.
        products = Products.objects.list_by_categories(0, 0, True, "id", category_slug, language, "json")

        # bu kısımda listedeki page kısmında kaç kayıt göstereceğiz onu seçeceğiz.
        # daha sonrasında sayı 15 olabilir.
        paginator = Paginator(products, 16)
        try:
            prd_list = paginator.page(page)
        except PageNotAnInteger:
            prd_list = paginator.page(1)
        except EmptyPage:
            prd_list = paginator.page(paginator.num_pages)

        pager = str(int(page) + 1)

        if prd_list.has_next() == False:
            if language == "tr":
                message = "Bulunduğunuz kategorideki tüm parçaları görüntülüyorsunuz."
            else:
                message = "You're viewing all spare parts in this category."
        else:
            message = ""

        # print(prd_list)
        context = {
            'page': pager,
            'products': list(prd_list),
            'has_next': prd_list.has_next(),
            'message': message,
            'language': language,
            'category_url': '/' + translation.get_language() + '/' + category_slug,

        }
        # return JsonResponse(list(context),safe=False)
        return HttpResponse(json.dumps(context), content_type="application/json")


@method_decorator(csrf_exempt, name='dispatch')
class product_detail(View):
    template_name = "products/product_detail.html"

    def get(self, request, slug):
        notification_count = 0
        menus = Menus.get_menus(translation.get_language(), "top")
        bottom_menu = Menus.get_menus(translation.get_language(), "bottom")
        # selected_meta = []
        if translation.get_language() == "tr":
            # excel products
            product = Products.objects.product_detail(translation.get_language(), slug)

            if len(product) == 0:
                page_type = "offthe-list"
                product = ExcelProdcts.product_detail(translation.get_language(), slug)

                category = ""
                related_products = ""
                parent_categories = ""
                selected_meta = MetaDescriptionTags.get_selected_meta("pamuk-makineleri", translation.get_language())

                if ExcelProdcts.objects.filter(id=product.get("id")).get().slug_en is not None:
                    change_lang = "/en/products/detail/" + ExcelProdcts.objects.filter(id=product.get("id")).get().slug_en
                else:
                    change_lang = "javascript:void(0);"

            else:
                page_type = "detail"
                product_categories = Products.objects.get(id=product.get("id")).categories
                category = Categories.objects.get_category(product_categories.slug, translation.get_language())

                selected_meta = MetaDescriptionTags.get_selected_meta(category.slug, translation.get_language())

                # print(selected_meta)
                related_products = ProductsTypes.related_product(Products.objects.get(slug=slug),
                                                                 translation.get_language())
                parent_categories = Categories.objects.get_parents(category.slug, translation.get_language())

                if Products.objects.get_regular_product(product.get("id")).productstranslates_set.exists():
                    change_lang = "/en/products/detail/" + Products.objects.get_regular_product(product.get("id")).productstranslates_set.get().slug
                else:
                    change_lang = "javascript:void(0);"

        else:
            prd_en = ProductsTranslates.objects.get(slug=slug)
            product = Products.objects.product_detail(translation.get_language(), prd_en.products.slug)
            if len(product) == 0:
                page_type = "offthe-list"
                product = ExcelProdcts.product_detail(translation.get_language(), slug)
                category = ""
                related_products = ""
                parent_categories = ""
                selected_meta = MetaDescriptionTags.get_selected_meta("pamuk-makineleri", translation.get_language())
                change_lang = "/tr/products/detail/" + ExcelProdcts.objects.filter(id=product.get("id")).get().slug_tr

            else:
                page_type = "detail"

                # product = Products.objects.product_detail(translation.get_language(), prd_en.products.slug)
                # category_slug = ProductsTranslates.objects.get(slug=slug).products.categories.slug
                change_lang = "/tr/products/detail/" + Products.objects.filter(id=product.get("id")).get().slug
                category = Categories.objects.get_category(prd_en.products.categories.categoriestranslates_set.get().slug,translation.get_language())
                selected_meta = MetaDescriptionTags.get_selected_meta(category.slug, translation.get_language())

                related_products = ProductsTypes.related_product(Products.objects.get(slug=prd_en.products.slug), translation.get_language())

                parent_categories = Categories.objects.get_parents(category.slug, translation.get_language())
        # basket operations
        if "basket" in request.session:
            request.session["basket"] = change_basket_for_language(request, translation.get_language())
            basket_totalcount = basket_count(request.session["basket"])
            basket_totalprice = total_basket_price(request.session["basket"], translation.get_language())
        else:
            basket_totalprice = None
            basket_totalcount = 0

        # basket operations

        if "customer_id" in request.session and request.user.is_authenticated:
            order_count = Basket.get_order_count(Customers.get_customer_by_id(request.session["customer_id"]))
            notification_count = notifications_count(Customers.get_customer_by_id(request.session["customer_id"]))
        else:
            order_count = 0


        context = {
            "category": category,
            'top_menu': menus,
            'product': product,
            'related_products': related_products,
            "parent_categories": parent_categories,
            "page_type": page_type,
            'bottom_menu': bottom_menu,
            'basket_count': basket_totalcount,
            'basket_price': basket_totalprice,
            'order_count':order_count,
            'notification_count': notification_count,
            'footer_address': CompanyInfos.get_company_address(),
            'footer_email': CompanyInfos.get_company_email('email'),
            'meta_tags': selected_meta,
            'change_language': change_lang,
        }
        return render(request, self.template_name, context)

    def post(self, request):
        pass


@method_decorator(csrf_exempt, name='dispatch')
class products_search(View):
    template_name = ""

    def get(self, request):
        pass

    def post(self, request):
        search_text = request.POST.get("search_text")
        language = request.POST.get("language")
        product_list = Products.objects.search_products_oem(language, change_searchtext(search_text))

        if len(product_list) == 0:
            product_list = ExcelProdcts.search_products_oem(language, change_searchtext(search_text))

        return HttpResponse(json.dumps(list(product_list)), content_type="application/json")
