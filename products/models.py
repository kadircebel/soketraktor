from django.db import models
from django.utils.text import slugify
# from froala_editor.fields import FroalaField
from django.utils.safestring import mark_safe
from products_translates.models import ProductsTranslates
from .managers import ProductsManager
from helpers.views import cover_url,get_product_price
from ckeditor.fields import RichTextField
from payments.models import TaxOptions


def upload_cover(instance,filename):
    import os,random
    basefilename,file_extension = os.path.splitext(filename)
    basefilename = slugify(basefilename)
    chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890'
    randomstr = ''.join((random.choice(chars)) for x in range(10))
    return 'covers/prd_{basename}{randomstring}{ext}'.format(basename=basefilename,randomstring=randomstr, ext=file_extension)

class Products(models.Model):
    id = models.BigAutoField(primary_key=True)
    categories = models.ForeignKey('categories.Categories',related_name="categories_products",verbose_name="Kategori",db_index=True,on_delete=models.DO_NOTHING)
    title = models.CharField(max_length=600, verbose_name="Ürün Adı TR")
    # content_text = FroalaField(verbose_name="", blank=True, null=True, options={'height': 350})
    content_text = RichTextField(verbose_name="", blank=True, null=True)
    product_cover = models.ImageField(verbose_name="Ürün Resmi", upload_to=upload_cover, blank=True, null=True, help_text="Boyut - 203x190")
    tech_note = models.CharField(default="0",max_length=10,verbose_name="Parça No",help_text="Teknik resimde hangi numaralı parça olduğu bilgisini giriniz. Örn: 5,15,20",null=True,blank=True)
    slug = models.SlugField(max_length=800, verbose_name="Ürün Url", blank=True, null=True)
    order_number = models.IntegerField(default=0, verbose_name="Sıra Numarası", help_text="Ürün Sırası")
    is_stock = models.BooleanField(default=True,verbose_name="Stokta Mı?")
    is_active = models.BooleanField(default=True, verbose_name="Aktif Mi?")

    objects = ProductsManager()

    class Meta:
        managed = True
        db_table = "products"
        verbose_name = "Ürün"
        verbose_name_plural = "Ürünler"
        ordering = ["-id"]


    def __str__(self):

        return "{} - {}".format(self.title,self.productsoemnumbers_set.get().oem_number)

    def preview_cover(self):
        if cover_url(self.product_cover) is None:
            return mark_safe('<img src="/static/images/empty.png" style="max-height:112px;" width="52" height="52" />')
        else:
            return mark_safe('<img src="/media/{}" style="max-height:112px;" width="52" height="52" />'.format(self.product_cover))

    preview_cover.short_description = 'Ürün Resmi'
    preview_cover.allow_tags = True


class ProductsBrandModels(models.Model):
    products = models.ForeignKey('products.Products',on_delete=models.DO_NOTHING,db_index=True,verbose_name="Ürün")
    brandmodels = models.ForeignKey('brandmodels.BrandModels',on_delete=models.DO_NOTHING,verbose_name="Model")
    is_active = models.BooleanField(default=True,verbose_name="Aktif Mi?")

    class Meta:
        managed = True
        db_table = "products_brandmodels"
        verbose_name = "Kullanıldığı Model"
        verbose_name_plural = "Kullanıldığı Modeller"

    def __str__(self):
        return self.brandmodels.title_tr

class ProductsOemNumbers(models.Model):
    products = models.ForeignKey('products.Products', on_delete=models.DO_NOTHING,db_index=True, verbose_name="Ürün",unique=True)
    oem_number = models.CharField(max_length=250,verbose_name="Parça Oem No")
    sub_number = models.CharField(max_length=500,verbose_name="Parça Sub No",null=True,blank=True)
    is_active = models.BooleanField(default=True,verbose_name="Aktif Mi?")

    class Meta:
        managed = True
        db_table = "products_oemnumbers"
        verbose_name = "Yedek Parça Oem Numarası"
        verbose_name_plural = "Yedek Parça Oem Numaraları"

    def __str__(self):
        return self.oem_number

    # def save(self, *args,**kwargs):
    #     self.products.is_active = self.is_active
    #     self.products.save()
    #     super(ProductsOemNumbers,self).save(*args,**kwargs)


class ProductsTypes(models.Model):
    parent_products = models.ForeignKey('products.Products', on_delete=models.DO_NOTHING,db_index=True, verbose_name="İlişikili Ürün",related_name="parent_product",blank=True,null=True)
    child_product = models.ForeignKey('products.Products', on_delete=models.DO_NOTHING, verbose_name="Ürün",blank=True,null=True)


    class Meta:
        managed = True
        db_table = "products_types"
        verbose_name = "İlişkili Ürün"
        verbose_name_plural = "İlişkili Ürünler"

    # def __str__(self):
    #     return self.child_product.title

    @classmethod
    def related_product(cls,product,language):
        context_products=[]
        if cls.objects.filter(parent_products=product).exists():

            if cls.objects.filter(parent_products=product).count() > 0:
                # parent
                for item in cls.objects.filter(parent_products=product).all():
                    if language == "tr":
                        item_prd = {
                            'title':item.child_product.title,
                            'slug':'products/detail/{}'.format(item.child_product.slug),
                            'cover': cover_url(item.child_product.product_cover),
                            'oem':item.child_product.productsoemnumbers_set.get().oem_number,
                            'sub':item.child_product.productsoemnumbers_set.get().sub_number,
                        }
                    else:
                        # print(item.child_product.productstranslates_set.exists())
                        if item.child_product.productstranslates_set.exists():
                            item_prd = {
                                'title':item.child_product.productstranslates_set.get().title,
                                'slug':'prodcuts/detail/{}'.format(item.child_product.productstranslates_set.get().slug),
                                'cover':cover_url(item.child_product.product_cover),
                                'oem': item.child_product.productsoemnumbers_set.get().oem_number,
                                'sub': item.child_product.productsoemnumbers_set.get().sub_number,
                            }

                            context_products.append(item_prd)

        elif cls.objects.filter(child_product=product).exists():
            if cls.objects.filter(child_product=product).count() > 0:
                parent_product = cls.objects.get(child_product=product).parent_products
                if language == "tr":
                    item_prd = {
                        'title':parent_product.title,
                        'slug':'products/detail/{}'.format(parent_product.slug),
                        'cover': cover_url(parent_product.product_cover),
                        'oem': parent_product.productsoemnumbers_set.get().oem_number,
                        'sub': parent_product.productsoemnumbers_set.get().sub_number,
                    }
                    context_products.append(item_prd)

                    for item in cls.objects.filter(parent_products=parent_product).all():
                        if item.child_product != product:
                            item_prd = {
                                'title': item.child_product.title,
                                'slug': 'products/detail/{}'.format(item.child_product.slug),
                                'cover': cover_url(item.child_product.product_cover),
                                'oem': item.child_product.productsoemnumbers_set.get().oem_number,
                                'sub': item.child_product.productsoemnumbers_set.get().sub_number,
                            }
                            context_products.append(item_prd)
                else:
                    if parent_product.productstranslates_set.exists():
                        item_prd = {
                            'title': parent_product.productstranslates_set.get().title,
                            'slug': 'products/detail/{}'.format(parent_product.productstranslates_set.get().slug),
                            'cover': cover_url(parent_product.product_cover),
                            'oem': parent_product.productsoemnumbers_set.get().oem_number,
                            'sub': parent_product.productsoemnumbers_set.get().sub_number,

                        }
                        context_products.append(item_prd)

                        for item in cls.objects.filter(parent_products=product).all():
                            if item.child_product != product:
                                item_prd = {
                                    'title': item.child_product.productstranslates_set.get().title,
                                    'slug': 'prodcuts/detail/{}'.format(item.child_product.productstranslates_set.get().slug),
                                    'cover': cover_url(item.child_product.product_cover),
                                    'oem': item.child_product.productsoemnumbers_set.get().oem_number,
                                    'sub': item.child_product.productsoemnumbers_set.get().sub_number,
                                }
                                context_products.append(item_prd)

        return context_products

class ProductsVariations(models.Model):
    products = models.ForeignKey('products.Products', db_index=True, on_delete=models.DO_NOTHING, verbose_name="Ürün")
    variation_units = models.ForeignKey('variations.VariationUnits',verbose_name="Ölçü Birimi",on_delete=models.DO_NOTHING)
    unit_value = models.CharField(max_length=15,verbose_name="Ölçü Değeri",default=None,blank=True,null=True,help_text="Renk seçimi yapıldıysa boş bırakılacaktır.")

    class Meta:
        managed = True
        db_table = "products_variations"
        verbose_name = "Ürün Varyasyon Özelliği"
        verbose_name_plural = "Ürün Varyasyon Özellikleri"

class ProductsPrices(models.Model):
    products = models.ForeignKey('products.Products',db_index=True,on_delete=models.DO_NOTHING,verbose_name="Ürün",unique=True)
    price_tl = models.DecimalField(default=0.00,verbose_name="Fiyat - TL",decimal_places=2,max_digits=12)
    price_dl = models.DecimalField(default=0.00,verbose_name='Fiyat - $',decimal_places=2,max_digits=12)
    price_eu = models.DecimalField(default=0.00,verbose_name='Fiyat - €',decimal_places=2,max_digits=12)
    # price_kdv = models.DecimalField(default=0.18,verbose_name='KDV',decimal_places=2,max_digits=4)
    # price_cargo = models.DecimalField(default=0.18, verbose_name='Kargo Ücreti',decimal_places=2,max_digits=4)
    discount_normal = models.IntegerField(default=0,verbose_name='İndirim %',help_text="Üründe indirim olacaksa indirim yüzdesini giriniz.")
    discount_transfer = models.IntegerField(default=0,verbose_name='İndirim %',help_text="Üründe havale indirimi giriniz.")

    class Meta:
        managed = True
        db_table = "products_prices"
        verbose_name = "Ürün Fiyatı"
        verbose_name_plural = "Ürün Fiyatları"

    def __str__(self):
        return str(self.price_dl)


def gallery_cover(instance,filename):
    import os,random
    basefilename,file_extension = os.path.splitext(filename)
    basefilename = slugify(basefilename)
    chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890'
    randomstr = ''.join((random.choice(chars)) for x in range(10))
    return 'galleries/products/{name}/{basename}{randomstring}{ext}'.format(name=instance.products.slug, basename=basefilename,randomstring=randomstr, ext=file_extension)

class ProductsGalleries(models.Model):
    id = models.BigAutoField(primary_key = True)
    products = models.ForeignKey('products.Products',db_index=True,verbose_name="Ürün",on_delete=models.DO_NOTHING)
    products_image =  models.ImageField(verbose_name="Ürün Resmi", upload_to=gallery_cover, blank=True, null=True, help_text="Boyut - 373x327")
    title_tr = models.CharField(max_length=600, verbose_name="Resim Adı TR",blank=True,null=True)
    title_en = models.CharField(max_length=600, verbose_name="Resim Adı EN",blank=True,null=True)
    content_tr = models.TextField(verbose_name="Resim Açıklama TR",blank=True,null=True)
    content_en = models.TextField(verbose_name="Resim Açıklama EN", blank=True, null=True)
    is_active = models.BooleanField(default=True,verbose_name="Aktif Mi?")

    class Meta:
        managed = True
        db_table = "products_galleries"
        verbose_name = "Ürün Galerisi"
        verbose_name_plural = "Ürün Galerileri"

    def __str__(self):
        return self.title_tr

# excel products model

class ExcelProdcts(models.Model):
    id = models.BigAutoField(primary_key=True)
    title_tr = models.CharField(max_length=500,verbose_name="Ürün Adı TR",blank=True,null=True)
    title_en = models.CharField(max_length=500, verbose_name="Ürün Adı EN", blank=True, null=True)
    oem_number = models.CharField(max_length=500, verbose_name="OEM No", blank=True, null=True)
    content_tr = RichTextField(verbose_name="İçerik TR", blank=True, null=True)
    content_en = RichTextField(verbose_name="İçerik EN", blank=True, null=True)
    price_tr = models.DecimalField(default=0.00, verbose_name='Fiyat - TL', decimal_places=2, max_digits=4)
    price_dl = models.DecimalField(default=0.00, verbose_name='Fiyat - $', decimal_places=2, max_digits=4)
    price_eu = models.DecimalField(default=0.00, verbose_name='Fiyat - €', decimal_places=2, max_digits=4)
    slug_tr = models.SlugField(max_length=800,verbose_name="Ürün Url TR",blank=True,null=True)
    slug_en = models.SlugField(max_length=800, verbose_name="Ürün Url EN", blank=True, null=True)
    is_stock = models.BooleanField(default=True, verbose_name="Stokta Mı?")
    is_active = models.BooleanField(default=True, verbose_name="Aktif Mi?")

    class Meta:
        managed = True
        db_table = "excel_products"
        verbose_name = "Liste Dışı Ürün"
        verbose_name_plural = "Liste Dışı Ürünler"

    def __str__(self):
        return self.title_tr


    @classmethod
    def product_detail(cls,language,slug):
        context_models = []
        context_galleries = []

        if language == "tr":
            selected = cls.objects.get(slug_tr=slug)
            context = {
                'title': selected.title_tr,
                'content': selected.content_tr,
                'cover': "/static/images/empty.png",
                'tech_image': "",
                'brand_models': context_models,
                'galleries': context_galleries,
                'id': selected.id,
                'oem_number': selected.oem_number,
                'sub_number': "",
                'tech_note': "",
                'is_stock': selected.is_stock,
                'price':TaxOptions.get_kdv_price(get_product_price(selected),language),
                'product_type': 'outlist-product',

            }
        else:
            selected = cls.objects.get(slug_en=slug)
            context = {
                'title': selected.title_en,
                'content': selected.content_en,
                'cover': "/static/images/empty.png",
                'tech_image': "",
                'brand_models': context_models,
                'galleries': context_galleries,
                'id': selected.id,
                'oem_number': selected.oem_number,
                'sub_number': "",
                'tech_note': "",
                'is_stock': selected.is_stock,
                'price': TaxOptions.get_kdv_price(get_product_price(selected),language),
                'product_type': 'outlist-product',
            }

        return context

    @classmethod
    def search_products_oem(cls,languages,oem_number):
        context = []
        product_list = cls.objects.filter(oem_number__startswith=oem_number)[0:15]

        if product_list.count() > 0:
            for item in product_list:
                if languages == "tr":
                    search_item = {
                        'title':item.title_tr,
                        'slug':"/" + languages + "/products/detail/" + item.slug_tr,
                        'cover': '',
                        'oem_number':item.oem_number,
                        'sub_number':'',
                    }
                else:
                    if item.slug_en is not None:
                        search_item = {
                            'title': item.title_en,
                            'slug': "/" + languages + "/products/detail/" + item.slug_en,
                            'cover': '',
                            'oem_number': item.oem_number,
                            'sub_number': '',
                        }
                    else:
                        search_item = {
                            'title': item.title_tr,
                            'slug': "/tr/products/detail/" + item.slug_tr,
                            'cover': '',
                            'oem_number': item.oem_number,
                            'sub_number': '',
                        }


                context.append(search_item)

        return context

    @classmethod
    def get_outlist_product(cls,id):
        return cls.objects.get(id=id)

