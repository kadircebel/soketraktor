from django.db import models
from categories.models import Categories
# from languages.models import Languages
from helpers.views import cover_url,seperate_subnumber,get_product_price,revised_price
from payments.models import TaxOptions

class ProductsManager(models.Manager):

    def list_with_pager(self, min, max, is_stock, order_choice,language):
        context_products = []
        if max == 0:
            if order_choice == "order-number":
                product_list = self.filter(is_active=True, is_stock=is_stock).order_by("-order_number")
            else:
                product_list = self.filter(is_active=True, is_stock=is_stock).order_by("-id")
        else:
            if order_choice == "order-number":
                product_list = self.filter(is_active=True, is_stock=is_stock).order_by("-order_number")[min:max]
            else:
                product_list = self.filter(is_active=True, is_stock=is_stock).order_by("-id")[min:max]

        for item in product_list:
            context_models = []

            if item.productsbrandmodels_set.count() > 0:
                for prd_models in item.productsbrandmodels_set.all():
                    if language == "tr":
                        item_mdl = {
                            'title': prd_models.brandmodels.title_tr,
                        }
                    else:
                        item_mdl = {
                            'title': prd_models.brandmodels.title_en,
                        }

                    context_models.append(item_mdl)

            if language == "tr":
                item_prd = {
                    'title': item.title,
                    'cover': cover_url(item.product_cover),
                    'slug': "/" + language + "/products/detail/" + item.slug,
                    'is_stock':item.is_stock,
                    'category': item.categories.title,
                    'category_slug': "/" + language + "/categories/" + item.categories.parent_id.slug,
                    'id': item.id,
                    'price': TaxOptions.get_kdv_price(get_product_price(item),language),
                    'prd_models': context_models,
                    'oem_number': item.productsoemnumbers_set.get().oem_number,
                    'sub_number': seperate_subnumber(item.productsoemnumbers_set.get().sub_number),
                    'category_json': {'name': item.categories.parent_id.title,
                                  'slug': "/" + language + "/categories/" + item.categories.parent_id.slug},
                    'product_type':'regular-product',
                }
                context_products.append(item_prd)
            else:
                if item.productstranslates_set.exists() and item.categories.categoriestranslates_set.exists():
                    item_prd = {
                        'title': item.productstranslates_set.get().title,
                        'cover': cover_url(item.product_cover),
                        'slug': "/" + language + "/products/detail/" + item.productstranslates_set.get().slug,
                        'category': item.categories.categoriestranslates_set.get().title,
                        'category_json': {'name': item.categories.parent_id.categoriestranslates_set.get().title, 'slug': item.categories.parent_id.categoriestranslates_set.get().slug},
                        # 'id': item.productstranslates_set.get().id,
                        'id': item.id,
                        'price': TaxOptions.get_kdv_price(get_product_price(item),language),
                        'is_stock': item.is_stock,
                        'prd_models': context_models,
                        'oem_number': item.productsoemnumbers_set.get().oem_number,
                        'sub_number': seperate_subnumber(item.productsoemnumbers_set.get().sub_number),
                        'product_type': 'regular-product',
                    }

                else:
                    item_prd = {
                        'title': item.title,
                        'cover': cover_url(item.product_cover),
                        'slug': "/tr/products/detail/" + item.slug,
                        'is_stock': item.is_stock,
                        'category': item.categories.title,
                        'category_slug': "/tr/categories/" + item.categories.parent_id.slug,
                        'id': item.id,
                        'price': TaxOptions.get_kdv_price(get_product_price(item), language),
                        'prd_models': context_models,
                        'oem_number': item.productsoemnumbers_set.get().oem_number,
                        'sub_number': seperate_subnumber(item.productsoemnumbers_set.get().sub_number),
                        'category_json': {'name': item.categories.parent_id.title,
                                          'slug': "/tr/categories/" + item.categories.parent_id.slug},
                        'product_type': 'regular-product',
                    }

                context_products.append(item_prd)


        return context_products


    def counts_by_category(self,is_stock,category_slug,language):
        category = Categories.objects.get_category(category_slug, language)
        if language == "tr":
            return self.filter(is_stock=is_stock, is_active=True, categories=category).count()
        else:
            return self.filter(is_stock=is_stock, is_active=True, categories=category.categories).count()


    def list_by_categories(self, min, max, is_stock, order_choice, category_slug, language,data_type):
        category = Categories.objects.get_category(category_slug, language)
        # print(category)
        context = []

        if max == 0:
            if language == "tr":
                if order_choice == "order-number":
                    product_list = self.filter(is_stock=is_stock, is_active=True, categories=category).order_by("-order_number")
                else:
                    product_list = self.filter(is_stock=is_stock, is_active=True, categories=category)
            else:
                if self.filter(is_stock=is_stock, is_active=True, categories=category.categories).count() < 2:
                    # eğer ingilizce ürün sayısı az ise tr çek
                    if order_choice == "order-number":
                        product_list = self.filter(is_stock=is_stock, is_active=True, categories=category).order_by("-order_number")
                    else:
                        product_list = self.filter(is_stock=is_stock, is_active=True, categories=category)

                else:
                    if order_choice == "order-number":
                        product_list = self.filter(is_stock=is_stock, is_active=True, categories=category.categories).order_by("-order_number")
                    else:
                        product_list = self.filter(is_stock=is_stock, is_active=True, categories=category.categories)

        else:
            if language == "tr":
                if order_choice == "order-number":
                    product_list = self.filter(is_stock=is_stock, is_active=True, categories=category).order_by("-order_number")[min:max]
                else:
                    product_list = self.filter(is_stock=is_stock, is_active=True, categories=category)[min:max]

            else:
                if self.filter(is_stock=is_stock, is_active=True, categories=category.categories).count() < 2:
                    # eğer ingilizce ürün az ise tr çek
                    if order_choice == "order-number":
                        product_list = self.filter(is_stock=is_stock, is_active=True, categories=category.categories).order_by("-order_number")[min:max]
                    else:
                        product_list = self.filter(is_stock=is_stock, is_active=True, categories=category.categories)[min:max]
                else:
                    if order_choice == "order-number":
                        product_list = self.filter(is_stock=is_stock, is_active=True, categories=category.categories).order_by("-order_number")[min:max]
                    else:
                        product_list = self.filter(is_stock=is_stock, is_active=True, categories=category.categories)



        if data_type == "json":
            for item in product_list:
                context_models = []
                if item.productsbrandmodels_set.count() > 0:
                    for prd_models in item.productsbrandmodels_set.all():
                        if language == "tr":
                            item_mdl = {
                                'title': prd_models.brandmodels.title_tr,
                            }
                        else:
                            item_mdl = {
                                'title': prd_models.brandmodels.title_en,
                            }

                        context_models.append(item_mdl)

                if language == "tr":
                    item_prd = {
                        'title': item.title,
                        'cover': cover_url(item.product_cover),
                        'slug': "/" + language + "/products/detail/" + item.slug,
                        'category': [{'name':category.parent_id.title, 'slug':"/"+language+"/categories/"+category.parent_id.slug}],
                        'id': item.id,
                        # 'price': '50',
                        'price': TaxOptions.get_kdv_price(get_product_price(item),language),
                        'oem_number': item.productsoemnumbers_set.get().oem_number,
                        'sub_number': seperate_subnumber(item.productsoemnumbers_set.get().sub_number),
                        'is_stock': item.is_stock,
                        'prd_models': context_models,
                        'product_type': 'regular-product',
                    }
                else:
                    if item.productstranslates_set.exists() and category.exists():
                        item_prd = {
                            'title': item.productstranslates_set.get().title,
                            'cover': cover_url(item.product_cover),
                            'slug': "/" + language + "/products/detail/" + item.productstranslates_set.get().slug,
                            'category': [{'name': category.categories.parent_id.title, 'slug': category.categories.parent_id.slug}],
                            # 'id': item.productstranslates_set.get().id,
                            'id': item.id,
                            'price': TaxOptions.get_kdv_price(get_product_price(item),language),
                            'oem_number': item.productsoemnumbers_set.get().oem_number,
                            'sub_number': seperate_subnumber(item.productsoemnumbers_set.get().sub_number),
                            'is_stock': item.is_stock,
                            'prd_models': context_models,
                            'product_type': 'regular-product',
                        }
                    else:
                        # item_prd = {}
                        item_prd = {
                            'title': item.title,
                            'cover': cover_url(item.product_cover),
                            'slug': "/tr/products/detail/" + item.slug,
                            'category': [{'name': category.parent_id.title,
                                          'slug': "/tr/categories/" + category.parent_id.slug}],
                            'id': item.id,
                            'price': TaxOptions.get_kdv_price(get_product_price(item),language),
                            'oem_number': item.productsoemnumbers_set.get().oem_number,
                            'sub_number': seperate_subnumber(item.productsoemnumbers_set.get().sub_number),
                            'is_stock': item.is_stock,
                            'prd_models': context_models,
                            'product_type': 'regular-product',
                        }

                context.append(item_prd)

        else:
            for item in product_list:
                context_models = []
                if item.productsbrandmodels_set.count() > 0:
                    for prd_models in item.productsbrandmodels_set.all():
                        if language == "tr":
                            item_mdl = {
                                'title': prd_models.brandmodels.title_tr,
                            }
                        else:
                            item_mdl = {
                                'title': prd_models.brandmodels.title_en,
                            }

                        context_models.append(item_mdl)

                if language == "tr":

                    item_prd = {
                        'title': item.title,
                        'cover': cover_url(item.product_cover),
                        'slug': "/" + language + "/products/detail/" + item.slug,
                        'category': category.parent_id,
                        'id': item.id,
                        'price': TaxOptions.get_kdv_price(get_product_price(item),language),
                        'oem_number': item.productsoemnumbers_set.get().oem_number,
                        'sub_number': seperate_subnumber(item.productsoemnumbers_set.get().sub_number),
                        'is_stock': item.is_stock,
                        'prd_models': context_models,
                        'product_type': 'regular-product',
                    }
                    context.append(item_prd)
                else:
                    if item.productstranslates_set.exists():
                        item_prd = {
                            'title': item.productstranslates_set.get().title,
                            'cover': cover_url(item.product_cover),
                            'slug': "/" + language + "/products/detail/" + item.productstranslates_set.get().slug,
                            'category': category.categories.parent_id.categoriestranslates_set.get(),
                            # 'id': item.productstranslates_set.get().id,
                            'id': item.id,
                            'price': TaxOptions.get_kdv_price(get_product_price(item),language),
                            'oem_number': item.productsoemnumbers_set.get().oem_number,
                            'sub_number': seperate_subnumber(item.productsoemnumbers_set.get().sub_number),
                            'is_stock': item.is_stock,
                            'prd_models': context_models,
                            'product_type': 'regular-product',
                        }
                    else:
                        item_prd = {
                            'title': item.title,
                            'cover': cover_url(item.product_cover),
                            'slug': "/tr/products/detail/" + item.slug,
                            'category': category.categories.parent_id,
                            'id': item.id,
                            'price': TaxOptions.get_kdv_price(get_product_price(item), language),
                            'oem_number': item.productsoemnumbers_set.get().oem_number,
                            'sub_number': seperate_subnumber(item.productsoemnumbers_set.get().sub_number),
                            'is_stock': item.is_stock,
                            'prd_models': context_models,
                            'product_type': 'regular-product',
                        }

                    context.append(item_prd)

        return context


    def product_count(self):
        return self.filter(is_active=True).count()


    def product_type(self,slug):
        selected = self.get(slug=slug)
        if selected.productstypes_set.count() > 0:
            # bu ürün bir child product
            type_of_prd = "child"
        else:
            type_of_prd = "parent"

        return type_of_prd

    def product_detail(self,languages,slug):
        context_models = []
        context_galleries = []
        context = []
        if self.filter(slug=slug).exists():

            selected = self.get(slug=slug)

            for item in selected.productsbrandmodels_set.all():
                if languages == "tr":
                    item_models = {
                        'title': item.brandmodels.title_tr,
                    }
                else:
                    item_models = {
                        'title': item.brandmodels.title_en,
                    }

                context_models.append(item_models)

            for item in selected.productsgalleries_set.all():
                if languages == "tr":
                    item_gallery = {
                        'image': cover_url(item.products_image),
                        'title':item.title_tr,
                        'content':item.content_tr,
                    }
                else:
                    if item.title_en is not None:
                        item_gallery = {
                            'image': cover_url(item.products_image),
                            'title': item.title_en,
                            'content': item.content_en,
                        }
                    else:
                        item_gallery = {
                            'image': cover_url(item.products_image),
                            'title': item.title_tr,
                            'content': item.content_tr,
                        }

                context_galleries.append(item_gallery)

            # tech_image
            if selected.categories.category_gallery.exists():
                tech_image = cover_url(selected.categories.category_gallery.get(image_type="tech-image", is_active=True).gallery_image)
            else:
                tech_image = ""

            if languages == "tr":
                context={
                    'title':selected.title,
                    'content':selected.content_text,
                    'cover':cover_url(selected.product_cover),
                    'tech_image': tech_image,
                    # 'tech_image': cover_url(selected.categories.category_gallery.get(image_type="tech-image", is_active=True).gallery_image),
                    'brand_models':context_models,
                    'galleries':context_galleries,
                    'id':selected.id,
                    'oem_number':selected.productsoemnumbers_set.get().oem_number,
                    'sub_number':selected.productsoemnumbers_set.get().sub_number,
                    'tech_note':selected.tech_note,
                    'is_stock': selected.is_stock,
                    'price':TaxOptions.get_kdv_price(get_product_price(selected),languages),
                    'product_type': 'regular-product',

                }
            else:
                if selected.productstranslates_set.exists():
                    context = {
                        'title': selected.productstranslates_set.get().title,
                        'content': selected.productstranslates_set.get().content_text,
                        'cover': cover_url(selected.product_cover),
                        # 'tech_image':cover_url(selected.categories.category_gallery.get(image_type="tech-image", is_active=True).gallery_image),
                        'tech_image':tech_image,
                        'brand_models': context_models,
                        'galleries': context_galleries,
                        'id': selected.id,
                        'oem_number': selected.productsoemnumbers_set.get().oem_number,
                        'sub_number': selected.productsoemnumbers_set.get().sub_number,
                        'tech_note': selected.tech_note,
                        'is_stock':selected.is_stock,
                        'price': TaxOptions.get_kdv_price(get_product_price(selected),languages),
                        'product_type': 'regular-product',
                    }
                else:
                    context = {
                        'title': selected.title,
                        'content': selected.content_text,
                        'cover': cover_url(selected.product_cover),
                        'tech_image': tech_image,
                        # 'tech_image': cover_url(selected.categories.category_gallery.get(image_type="tech-image", is_active=True).gallery_image),
                        'brand_models': context_models,
                        'galleries': context_galleries,
                        'id': selected.id,
                        'oem_number': selected.productsoemnumbers_set.get().oem_number,
                        'sub_number': selected.productsoemnumbers_set.get().sub_number,
                        'tech_note': selected.tech_note,
                        'is_stock': selected.is_stock,
                        'price': TaxOptions.get_kdv_price(get_product_price(selected), languages),
                        'product_type': 'regular-product',
                    }


        return context

    def search_products_oem(self,language,oem_text):
        context = []
        product_list = self.filter(productsoemnumbers__oem_number__startswith=oem_text,is_active=True)[0:15]
        if product_list.count() > 0:
            for item in product_list:
                if language == "tr":
                    search_item = {
                        'title':item.title,
                        'oem_number':item.productsoemnumbers_set.get().oem_number,
                        'sub_number':item.productsoemnumbers_set.get().sub_number,
                        'cover':cover_url(item.product_cover),
                        'slug':"/" + language + "/products/detail/" + item.slug
                    }
                else:
                    if item.productstranslates_set.exists():
                        search_item = {
                            'title': item.productstranslates_set.get().title,
                            'cover': cover_url(item.product_cover),
                            'slug': "/" + language + "/products/detail/" + item.productstranslates_set.get().slug,
                            'oem_number': item.productsoemnumbers_set.get().oem_number,
                            'sub_number': item.productsoemnumbers_set.get().sub_number,
                        }
                    else:
                        search_item = {
                            'title': item.title,
                            'cover': cover_url(item.product_cover),
                            'slug': "/tr/products/detail/" + item.slug,
                            'oem_number': item.productsoemnumbers_set.get().oem_number,
                            'sub_number': item.productsoemnumbers_set.get().sub_number,
                        }

                context.append(search_item)
        else:
            product_list = self.filter(productsoemnumbers__sub_number__startswith=oem_text,is_active=True)[0:15]
            for item in product_list:
                if language == "tr":
                    search_item = {
                        'title':item.title,
                        'oem_number':item.productsoemnumbers_set.get().oem_number,
                        'sub_number':item.productsoemnumbers_set.get().sub_number,
                        'cover':cover_url(item.product_cover),
                        'slug':"/" + language + "/products/detail/" + item.slug
                    }
                else:
                    if item.productstranslates_set.exists():
                        search_item = {
                            'title': item.productstranslates_set.get().title,
                            'cover': cover_url(item.product_cover),
                            'slug': "/" + language + "/products/detail/" + item.productstranslates_set.get().slug,
                            'oem_number': item.productsoemnumbers_set.get().oem_number,
                            'sub_number': item.productsoemnumbers_set.get().sub_number,
                        }
                    else:
                        search_item = {
                            'title': item.title,
                            'cover': cover_url(item.product_cover),
                            'slug': "/tr/products/detail/" + item.slug,
                            'oem_number': item.productsoemnumbers_set.get().oem_number,
                            'sub_number': item.productsoemnumbers_set.get().sub_number,
                        }

                context.append(search_item)

        return context

    def get_regular_product(self,id):
        return self.get(id=id)


