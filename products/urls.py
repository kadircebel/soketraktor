from django.urls import path
from .views import *

app_name = "products"

urlpatterns=[
    path('', products_index.as_view(), name='products_index'),
    path('<slug:slug>/', product_list.as_view(), name='list'),
    path('detail/<slug:slug>/', product_detail.as_view(), name='detail'),
]
