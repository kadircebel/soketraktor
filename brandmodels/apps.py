from django.apps import AppConfig


class BrandmodelsConfig(AppConfig):
    name = 'brandmodels'
