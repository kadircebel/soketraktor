from django.contrib import admin
from .models import *

class BrandModelsAdmin(admin.ModelAdmin):
    list_display = ["title_tr","title_en","is_active"]
    fields = ["title_tr","title_en","is_active"]
    readonly_fields = ["slug_tr","slug_en"]
    search_fields = ["title_tr","title_slug","slug_tr","slug_en"]
    list_editable = ["is_active"]
    list_per_page = 50

    class Meta:
        model = BrandModels


admin.site.register(BrandModels,BrandModelsAdmin)
