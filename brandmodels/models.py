from django.db import models
from django.utils.text import slugify
from helpers.views import custom_slugify

class BrandModels(models.Model):
    id = models.BigAutoField(primary_key=True)
    title_tr = models.CharField(max_length=600, verbose_name="Model Adı TR",help_text="Traktör model numarası giriniz",null=True,blank=True)
    title_en = models.CharField(max_length=600, verbose_name="Model Adı EN", null=True,blank=True)
    slug_tr = models.CharField(max_length=650,verbose_name="Url TR",blank=True,null=True)
    slug_en = models.CharField(max_length=650, verbose_name="Url EN", blank=True, null=True)
    is_active = models.BooleanField(default=True,verbose_name="Aktif Mi?")

    class Meta:
        managed = True
        db_table = "brandmodels"
        verbose_name = "Model"
        verbose_name_plural = "Modeller"

    def __str__(self):
        return self.title_tr

    def save(self,*args,**kwargs):
        self.slug_tr = slugify(custom_slugify(self.title_tr))
        if self.title_en is not None:
            self.slug_en = slugify(custom_slugify(self.title_en))
        super(BrandModels,self).save(*args,**kwargs)




