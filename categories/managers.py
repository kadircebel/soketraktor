from django.db import models
# from django.core.files.images import get_image_dimensions
from helpers.views import cover_url
from categories_translates.models import CategoriesTranslates


class CategoriesManager(models.Manager):
    def get_category(self, slug, languages):
        try:
            if slug != "" and slug is not None:
                if languages == "tr":
                    return self.get(slug=slug)
                else:
                    return CategoriesTranslates.objects.get_category(slug)
            else:
                if languages == "tr":
                    return self.filter(is_active=True).all()
                else:
                    return CategoriesTranslates.objects.get_categories()
        except:
            return None

    def category_count(self,slug,languages):
        if slug != None and len(str(slug)) > 0:
            category = self.get_category(slug, languages)
            if languages == "tr":
                return self.filter(is_active=True,parent_id=category).count()
            else:
                return self.filter(is_active=True,parent_id=category.categories).count()
        else:
            return self.filter(is_active=True).count()

    def categories_list(self, min, max, order_choice, language,data_type):
        context = []
        if max == 0:
            if order_choice == "order-number":
                categories_list = self.filter(is_active=True, parent_id__isnull=False).order_by("-order_number")
            else:
                categories_list = self.filter(is_active=True, parent_id__isnull=False).order_by("-id")
        else:
            if order_choice == "order-number":
                categories_list = self.filter(is_active=True, parent_id__isnull=False).order_by("-order_number")[
                                  min:max]
            else:
                categories_list = self.filter(is_active=True, parent_id__isnull=False).order_by("-id")[min:max]

        for item in categories_list:

            if language == "tr":
                if data_type == "html":
                    item_category = {
                        'title': item.title,
                        'cover': cover_url(item.category_cover),
                        # 'slug': item.slug,
                        'slug': "/" + language + "/products/" + item.slug,
                        'parent': item.parent_id,
                        # 'img_width': category_cover_obj(item.category_cover),
                    }
                else:

                    item_category = {
                        'title': item.title,
                        'cover': cover_url(item.category_cover),
                        # 'slug': item.slug,
                        'slug': "/" + language + "/products/" + item.slug,
                        'parent': {'title':item.parent_id.title,'slug':item.parent_id.slug},
                        # 'img_width': category_cover_obj(item.category_cover),
                    }
                context.append(item_category)
            else:
                if item.categoriestranslates_set.exists():
                    if data_type == "html":
                        item_category = {
                            'title': item.categoriestranslates_set.get().title,
                            'cover': cover_url(item.category_cover),
                            # 'slug': item.categoriestranslates_set.get().slug,
                            'slug': "/" + language + "/products/" + item.categoriestranslates_set.get().slug,
                            'parent': item.parent_id.categoriestranslates_set.get()
                            # 'img_width': category_cover_obj(item.category_cover),
                        }
                    else:
                        item_category = {
                            'title': item.categoriestranslates_set.get().title,
                            'cover': cover_url(item.category_cover),
                            # 'slug': item.categoriestranslates_set.get().slug,
                            'slug': "/" + language + "/products/" + item.categoriestranslates_set.get().slug,
                            'parent': {'title':item.parent_id.categoriestranslates_set.get().title,'slug':item.parent_id.categoriestranslates_set.get().slug}
                            # 'img_width': category_cover_obj(item.category_cover),
                        }
                    context.append(item_category)

        return context

    def categories_list_by_parent(self, min, max, order_choice, language, parent_obj):
        context = []
        if max == 0:
            if order_choice == "order-number":
                categories_list = self.filter(is_active=True, parent_id=parent_obj).order_by("-order_number")
            else:
                categories_list = self.filter(is_active=True, parent_id=parent_obj).order_by("-id")

        else:
            if order_choice == "order-number":
                categories_list = self.filter(is_active=True, parent_id=parent_obj).order_by("-order_number")[min:max]
            else:
                categories_list = self.filter(is_active=True, parent_id=parent_obj).order_by("-id")[min:max]

        for item in categories_list:
            if language == "tr":
                item_category = {
                    'title': item.title,
                    'cover': cover_url(item.category_cover),
                    'slug': "/" + language + "/products/" + item.slug,
                    'parent': {'title':item.parent_id.title,'slug':item.parent_id.slug},
                    # 'parent': item.parent_id,
                    # 'img_width': category_cover_obj(item.category_cover),
                }
                context.append(item_category)
            else:
                if item.categoriestranslates_set.exists():
                    item_category = {
                        'title': item.categoriestranslates_set.get().title,
                        'cover': cover_url(item.category_cover),
                        'slug': "/" + language + "/products/" + item.categoriestranslates_set.get().slug,
                        # 'parent': item.parent_id.categoriestranslates_set.get(),
                        'parent': {'title': item.parent_id.categoriestranslates_set.get().title, 'slug': item.parent_id.categoriestranslates_set.get().slug},
                        # 'img_width': category_cover_obj(item.category_cover),
                    }
                    context.append(item_category)


        return context

    def get_parent_obj(self, slug, language):
        return self.get_category(slug, language)

    def get_parents(self, slug, language):
        selected = self.get_category(slug, language)

        if language == "tr":
            context = {
                'title': selected.parent_id.title,
                'slug': 'categories/' + selected.parent_id.slug,
            }
        else:
            context = {
                'title': selected.categories.parent_id.categoriestranslates_set.get().title,
                'slug': 'categories/' + selected.categories.parent_id.categoriestranslates_set.get().slug,
            }

        return context

    def get_parent_categories(self, language,parent_category):
        category_list = self.get_category("", language)
        context = []
        for item in category_list:
            if language == "tr":
                if item.parent_id != None and item.parent_id == parent_category:
                    item_cat = {
                        'title': item.title,
                        'slug': 'products/' + item.slug,
                        'cover': cover_url(item.category_cover),
                    }
                    context.append(item_cat)
                elif parent_category == None and item.parent_id != None:
                    item_cat = {
                        'title': item.title,
                        'slug': 'products/' + item.slug,
                        'cover': cover_url(item.category_cover),
                    }
                    context.append(item_cat)
            else:
                if item.categories.parent_id != None and item.categories.parent_id == parent_category:
                # if item.categories.parent_id != None:
                    item_cat = {
                        'title': item.title,
                        'slug': 'products/' + item.slug,
                        'cover': cover_url(item.categories.category_cover),
                    }
                    context.append(item_cat)
                elif parent_category == None and item.categories.parent_id != None:
                    item_cat = {
                        'title': item.title,
                        'slug': 'products/' + item.slug,
                        'cover': cover_url(item.categories.category_cover),
                    }

                    context.append(item_cat)

        return context
