from django.shortcuts import render, HttpResponse
from django.views import View
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
import json
from django.utils import translation
from categories.models import Categories
from pages.models import Menus,CompanyInfos,MetaDescriptionTags
from products.models import Products
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from helpers.views import total_basket_price, basket_count,change_basket_for_language,notifications_count
from customers.models import Customers
from basket.models import Basket

@method_decorator(csrf_exempt, name='dispatch')
class category_index(View):
    template_name = "categories/index.html"

    def get(self, request):

        notification_count = 0
        menus = Menus.get_menus(translation.get_language(), "top")
        bottom_menu = Menus.get_menus(translation.get_language(), "bottom")
        categories = Categories.objects.categories_list(0, 16, "id", translation.get_language(), "html")
        product_counts = Products.objects.product_count()
        category_counts = Categories.objects.count()
        pocket_list = Categories.objects.get_parent_categories(translation.get_language(), None)

        # basket operations
        if "basket" in request.session:
            request.session["basket"] = change_basket_for_language(request, translation.get_language())
            basket_totalcount = basket_count(request.session["basket"])
            basket_totalprice = total_basket_price(request.session["basket"], translation.get_language())
        else:
            basket_totalprice = None
            basket_totalcount = 0

        # basket operations

        if translation.get_language() == "tr":
            category = "Parça Kataloğu"
            change_lang = "/en/categories"
        else:
            category = "Catalog"
            change_lang = "/tr/categories"


        if category_counts > len(categories):
            has_next = True
        else:
            has_next = False

        if "customer_id" in request.session and request.user.is_authenticated:
            order_count = Basket.get_order_count(Customers.get_customer_by_id(request.session["customer_id"]))
            notification_count = notifications_count(Customers.get_customer_by_id(request.session["customer_id"]))
        else:
            order_count = 0

        context = {
            'top_menu': menus,
            'bottom_menu': bottom_menu,
            'categories': categories,
            'category': category,
            "prd_count": product_counts,
            "category_counts": category_counts,
            'pocket_list': pocket_list,
            'has_next': has_next,
            'pager_type': 'all-categories',
            'basket_count': basket_totalcount,
            'basket_price': basket_totalprice,
            'order_count':order_count,
            'notification_count': notification_count,
            'footer_address': CompanyInfos.get_company_address(),
            'footer_email': CompanyInfos.get_company_email('email'),
            'change_language': change_lang,
        }
        return render(request, self.template_name, context)

    def post(self, request):
        page = request.POST.get("page")
        language = request.POST.get("language")
        categories = Categories.objects.categories_list(0, 0, "id", language, "json")
        paginator = Paginator(categories, 16)

        try:
            cat_list = paginator.page(page)
        except PageNotAnInteger:
            cat_list = paginator.page(1)
        except EmptyPage:
            cat_list = paginator.page(paginator.num_pages)

        pager = str(int(page) + 1)

        if cat_list.has_next() == False:
            if language == "tr":
                message = "Bulunduğunuz kategorideki tüm parçaları görüntülüyorsunuz."
            else:
                message = "You're viewing all spare parts in this category."
        else:
            message = ""

        context = {
            'page': pager,
            'categories': list(cat_list),
            'has_next': cat_list.has_next(),
            'message': message,
            'language': language,

        }
        return HttpResponse(json.dumps(context), content_type="application/json")


@method_decorator(csrf_exempt, name='dispatch')
class categories_list(View):
    template_name = "categories/list.html"

    def get(self, request, slug):
        category = Categories.objects.get_category(slug, translation.get_language())
        notification_count = 0
        selected_meta = MetaDescriptionTags.get_selected_meta(slug, translation.get_language())

        if translation.get_language() == "tr":
            categories = Categories.objects.categories_list_by_parent(0, 16, "id", translation.get_language(), category)
            categories_count = Categories.objects.category_count(slug, translation.get_language())
            # change_lang = "/en/categories/" + cate
            if category.categoriestranslates_set.exists():
                change_lang = "/en/categories/" + category.categoriestranslates_set.get().slug
            else:
                change_lang = "javascript:void(0);"
        else:

            if category is not None:
                change_lang = "/tr/categories/" + category.categories.slug
            else:
                change_lang = "javascript:void(0);"

            categories = Categories.objects.categories_list_by_parent(0, 16, "id", translation.get_language(),category.categories)
            categories_count = len(Categories.objects.categories_list_by_parent(0, 0, "id", translation.get_language(),category.categories))

        menus = Menus.get_menus(translation.get_language(), "top")
        bottom_menu = Menus.get_menus(translation.get_language(), "bottom")
        product_counts = Products.objects.product_count()



        if categories_count > len(categories):
            has_next = True
        else:
            has_next = False

        # basket operations
        if "basket" in request.session:
            request.session["basket"] = change_basket_for_language(request, translation.get_language())
            basket_totalcount = basket_count(request.session["basket"])
            basket_totalprice = total_basket_price(request.session["basket"], translation.get_language())
        else:
            basket_totalprice = None
            basket_totalcount = 0

        # basket operations

        if "customer_id" in request.session and request.user.is_authenticated:
            order_count = Basket.get_order_count(Customers.get_customer_by_id(request.session["customer_id"]))
            notification_count = notifications_count(Customers.get_customer_by_id(request.session["customer_id"]))
        else:
            order_count = 0

        # if translation.get_language() == "tr":
        #     selected_meta = MetaDescriptionTags.get_selected_meta("pamuk-makineleri", translation.get_language())
        # else:
        #     selected_meta = MetaDescriptionTags.get_selected_meta("cotton-machinery", translation.get_language())

        context = {
            'categories_count': categories_count,
            'categories': categories,
            'top_menu': menus,
            "category": category,
            "prd_count": product_counts,
            'has_next': has_next,
            'bottom_menu': bottom_menu,
            'pager_type': 'specific-categories',
            'basket_count': basket_totalcount,
            'basket_price': basket_totalprice,
            'order_count':order_count,
            'notification_count': notification_count,
            'footer_address': CompanyInfos.get_company_address(),
            'footer_email': CompanyInfos.get_company_email('email'),
            'meta_tags': selected_meta,
            'change_language': change_lang,
        }
        return render(request, self.template_name, context)

    def post(self, request):
        category_slug = request.POST.get("category")
        page = request.POST.get("page")
        language = request.POST.get("language")
        category = Categories.objects.get_category(category_slug, language)

        if language == "tr":

            categories = Categories.objects.categories_list_by_parent(0, 0, "id", language, category)
        else:
            categories = Categories.objects.categories_list_by_parent(0, 0, "id", language,category.categories)

        paginator = Paginator(categories, 16)

        try:
            cat_list = paginator.page(page)
        except PageNotAnInteger:
            cat_list = paginator.page(1)
        except EmptyPage:
            cat_list = paginator.page(paginator.num_pages)

        pager = str(int(page) + 1)

        if cat_list.has_next() == False:
            if language == "tr":
                message = "Bulunduğunuz kategorideki tüm parçaları görüntülüyorsunuz."
            else:
                message = "You're viewing all spare parts in this category."
        else:
            message = ""

        context = {
            'page': pager,
            'categories': list(cat_list),
            'has_next': cat_list.has_next(),
            'message': message,
            'language': language,
            # 'category_url': '/' + translation.get_language() + '/' + category_slug,

        }
        return HttpResponse(json.dumps(context), content_type="application/json")
