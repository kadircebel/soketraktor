from django.db import models
from django.utils.text import slugify
from django.utils.safestring import mark_safe
from ckeditor.fields import RichTextField
from helpers.views import custom_slugify
from .managers import CategoriesManager

def upload_cover(instance,filename):
    import os,random
    basefilename,file_extension = os.path.splitext(filename)
    basefilename = slugify(basefilename)
    chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890'
    randomstr = ''.join((random.choice(chars)) for x in range(10))
    return 'covers/cat_{basename}{randomstring}{ext}'.format(basename=basefilename,randomstring=randomstr, ext=file_extension)

class Categories(models.Model):
    id = models.BigAutoField(primary_key=True)
    parent_id = models.ForeignKey('self',on_delete=models.DO_NOTHING,null=True,blank=True,verbose_name="Ana Kategori",help_text="Ana kategori seçimi yapınız. Eğer yoksa boş bırakabilirsiniz.")
    title = models.CharField(max_length=600,verbose_name="Başlık TR")
    content_text = RichTextField(verbose_name="", blank=True, null=True)
    category_cover = models.ImageField(verbose_name="Kapak Resmi",upload_to=upload_cover,blank=True,null=True,help_text="Küçük Boyut - 195x195:::Büyük Boyut - 385x195")
    slug = models.SlugField(max_length=650,verbose_name="Kategori Url",blank=True,null=True)
    order_number = models.IntegerField(default=0,verbose_name="Sıra Numarası",help_text="Kategori Sırası")
    is_active=models.BooleanField(default=True)

    objects = CategoriesManager()

    class Meta:
        managed = True
        db_table = "categories"
        verbose_name = "Kategori"
        verbose_name_plural = "Kategoriler"
        ordering = ["order_number"]

    def __str__(self):
        return self.title

    def preview_cover(self):
        if self.category_cover is None:
            return '<img src="/static/images/empty.png" style="max-width:385px;max-height:195px;" width="" height="" />'
        else:
            return mark_safe('<img src="/media/{}" style="max-width:385px;max-height:195px;" width="" height="" />'.format(self.category_cover))

    preview_cover.short_description = 'Mevcut Kapak'
    preview_cover.allow_tags = True

    def save(self,*args,**kwargs):
        self.slug = slugify(custom_slugify(self.title))
        super(Categories,self).save(*args,**kwargs)




def upload_gallery_item(instance,filename):
    import os,random
    basefilename,file_extension = os.path.splitext(filename)
    basefilename = slugify(basefilename)
    chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890'
    randomstr = ''.join((random.choice(chars)) for x in range(10))
    return 'galleries/category/{name}/{basename}{randomstring}{ext}'.format(name=slugify(instance.categories.title), basename=basefilename,randomstring=randomstr, ext=file_extension)

class CategoriesGalleries(models.Model):
    categories = models.ForeignKey('categories.Categories',related_name="category_gallery",db_index=True,verbose_name="Kategori Adı",on_delete=models.DO_NOTHING)
    TYPE_OPTION = (None,'Seçiniz'),
    IMAGE_TYPES = [
        ('tech-image','Teknik Resim'),
        ('gallery-image', 'Galeri Resim'),
    ]
    image_type = models.CharField(max_length=15,choices=IMAGE_TYPES,default=TYPE_OPTION)
    gallery_image = models.ImageField(verbose_name="Kapak Resmi",upload_to=upload_gallery_item,blank=True,null=True)
    title = models.CharField(max_length=150,blank=True,null=True,verbose_name="Başlık EN",help_text="Resim başlığını ingilizce olarak giriniz.")
    is_active = models.BooleanField(default=True,verbose_name="Aktif Mi?")

    class Meta:
        managed = True
        db_table = "categories_galleries"
        verbose_name = "Kategori Galerisi"
        verbose_name_plural = "Kategori Galerileri"

    def __str__(self):
        return self.image_type

    def preview_gallery(self):
        if self.gallery_image is None:
            return '<img src="empty.jpeg" style="max-width:250px;margin-bottom:40px;" width="" height="" />'
        else:
            return mark_safe('<img src="/media/{}" style="max-width:250px;margin-bottom:40px;" width="" height="" />'.format(self.gallery_image))

    preview_gallery.short_description = 'Resim'
    preview_gallery.allow_tags = True