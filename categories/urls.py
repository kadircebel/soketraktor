from django.urls import path
from .views import *

app_name = "categories"

urlpatterns=[
    path('', category_index.as_view(), name='categories_index'),
    path('<slug:slug>/', categories_list.as_view(), name='cat_list'),
    # path('detail/<slug:slug>', product_detail.as_view(), name='detail'),
]
