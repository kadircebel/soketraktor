from django.contrib import admin
from .models import *

class CategoriesGalleriesInline(admin.StackedInline):
    min_num = 0
    extra = 1
    model = CategoriesGalleries
    readonly_fields = ["preview_gallery"]



class CategoriesAdmin(admin.ModelAdmin):
    list_display = ["parent_id","title","order_number","is_active"]
    list_per_page = 50
    list_editable = ["order_number","is_active"]
    search_fields = ["title","slug"]
    fields = ["parent_id","title","content_text","category_cover","preview_cover","slug","order_number","is_active"]
    readonly_fields = ["slug","preview_cover"]
    list_display_links = ["title","parent_id"]



    inlines = [
        CategoriesGalleriesInline,
    ]

    class Meta:
        model = Categories

    # def category_title(instance,obj):
    #     selected = Categories.objects.filter(id=obj.id).get()
    #     if selected.parent_id != None:
    #         return "***  " + selected.title + "  |  " + str(selected.parent_id)
    #     else:
    #         return selected.title



admin.site.register(Categories,CategoriesAdmin)